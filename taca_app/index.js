/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */


import {AppRegistry} from 'react-native';
// import App from './app/index';
import App from './app_redux/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
