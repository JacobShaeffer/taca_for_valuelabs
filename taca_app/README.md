
# TACA

## Travel Activity Collection Analysis ##

  

### Description ###

  
  

### Installation ###

First clone the repository.

```

git clone git clone https://JacobShaeffer@bitbucket.org/shuyaohong/taca_app.git

```

Then navigate to the root directory and install all the npm packages.

```

cd taca_app

npm install

```

This will take a few minutes.

  

Now that all of the npm packages are installed react-native-maps needs to be edited to work properly with the project.

Navigate to node_modules/react-native-maps/index.js and change the following
```
= export { default as Callout } from './lib/components/MapCallout.js';
= export { default as AnimatedRegion } from './lib/components/AnimatedRegion.js';
- export { Animated, ProviderPropType, MAP_TYPES } from './lib/components/MapView.js';
= export const PROVIDER_GOOGLE = MapView.PROVIDER_GOOGLE;
```
```
= export { default as Callout } from './lib/components/MapCallout.js';
= export { default as AnimatedRegion } from './lib/components/AnimatedRegion.js';
+ export const Animated = MapView.Animated;
+ export const ProviderPropType = MapView.ProviderPropType;
+ export const MAP_TYPES = MapView.MAP_TYPES;

= export const PROVIDER_GOOGLE = MapView.PROVIDER_GOOGLE;
```

The app should now build for Android.

To build for ios, navigate to the ios directory and unzip Pod.zip.

After doing that the app should build for iOS.

### Running the app

### common problems
If you get an error which states
```
error: bundling failed: Error: Unable to resolve module `../components/Verification` from ...
```
then check to make sure the file app/components/Verification.js has a capital V on verification and not a lower case v.