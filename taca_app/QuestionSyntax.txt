TEXT
*qid: string,
*qtext: string,
*qtype: Enum<string>['text', 'slider', 'checkbox', 'radio']
required: boolean,
validation: func(value: string) -> number,
errorMessages: Array<string>,
placeholder: string,

SLIDER
*qid: string,
*qtext: string,
*qtype: Enum<string>['text', 'slider', 'checkbox', 'radio']
required: boolean, (ignored if value is set)
min: number,
max: number,
step: number,
value: number,

CHECKBOX:
*qid: string,
*qtext: string,
*qtype: Enum<string>['text', 'slider', 'checkbox', 'radio']
required: boolean, (ignored if labels contains a true value)
*labels: Object<string, boolean>{
	*$label: boolean,
	$label: boolean,
	...
}

RADIO:
*qid: string,
*qtext: string,
*qtype: Enum<string>['text', 'slider', 'checkbox', 'radio']
required: boolean, (ignored if defaultValue is set)
*labels: Array<string>,
defaultValue: string,


PARENT QUESTION:
children: Array<string>

CHILD QUESTION:
parent: string,
shouldShow: func($parentValue) -> boolean,


example Questions:
80650f0uEZbUDfZtCb5Y: {
	labels: {
		"Coke": false,
		"Dr. Pepper": false,
		"Pepsi": false,
		"Sprite": false,
	},
	qid: "",
	qtext: "Select a couple!",
	qtype: "checkbox",
	required: true,
}
MrkKJsQ1QsPk7L4kbfym: {
	children: [
		"i5ttATik5Iklr7AhGkYG",
	],
	max: 100,
	min: 0,
	qid: "MrkKJsQ1QsPk7L4kbfym",
	qtext: "Pick a good number.",
	qtype: "slider",
	step: 1,
}
Z3XqGt3Fwhno2ykusFc4: {
	errorMessages: [
		"Sorry, no numbers allowed.",
	],
	placeholder: "Type here",
	qid: "Z3XqGt3Fwhno2ykusFc4",
	qtext: "Your Name Please.",
	qtype: "text",
	validation: "if(String(value).match(/[0-9]/g) === null){return true;}else{return 0}"
},
i5ttATik5Iklr7AhGkYG: {
	defaultValue: "Green",
	labels: [
		"Green",
		"Blue",
		"Red",
		"Yellow"
	],
	parent: "MrkKJsQ1QsPk7L4kbfym",
	qid: "i5ttATik5Iklr7AhGkYG",
	qtext: "What is your favorite color?",
	qtype: "radio",
	required: true,
	shouldShow: "if(value > 50)return true; else return false;"
}

