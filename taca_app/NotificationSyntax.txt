*notifid: string,
*title: string,
*body: string,
*type: Enum<string>['text', 'slider', 'checkbox', 'radio', 'none']
*price: number
notificationTarget: function - see notes below

NONE
buttonText: string

TEXT
validation: func(value: string) -> number,
errorMessages: Array<string>,
placeholder: string,

SLIDER
min: number,
max: number,
step: number,
value: number,

CHECKBOX:
*labels: Object<string, boolean>{
	*$label: boolean,
	$label: boolean,
	...
}

RADIO:
*labels: Array<string>,
defaultValue: string,


PARENT:
children: Array<string>

CHILD:
parent: string,
isChild: boolean,
shouldShow: func($parentValue) -> boolean,


SERIES:
series: boolean
seriesID: string
seriesValue: number

// ----------------------------------------------------------------------------------------------------------------------

notification tagetting:
	- The notificationTarget key is used to define a function that will be run on the user's end on whether or not the user will be shown that notification.
	- Note that this is not a secure method to deliver private notifications, all users will still download the notification to their device before the device decides to show it.
	- The specific data this function will recieve is defined below.
	()

checkbox example: {
	notifID: "ATik5IslsadfgaionDFgGkYG",
	title: "Soda Flavors",
	body: "Please select the soda flavors you like from the list below",
	type: "checkbox",
	price: 1,
	labels: {
		"Coke": false,
		"Dr. Pepper": false,
		"Pepsi": false,
		"Sprite": false,
	},
}
slider with child: {
	notifID: "MrkKJsQ1QsPk7L4kbfym",
	title: "Slider",
	body: "Slide the slider to reveal the child question",
	type: "slider",
	price: 2
	children: [
		"i5ttATik5Iklr7AhGkYG",
	],
	max: 100,
	min: 0,
	step: 1,
}
text with validation: {
	notifID: "KJQ1Qsfr43Pk7L4kb",
	title: "Your name",
	body: "Please enter your first and last name.",
	type: "text",
	price: 1
	errorMessages: [
		"Sorry, no numbers allowed.",
	],
	placeholder: "Type here",
	validation: "if(String(value).match(/[0-9]/g) === null){return true;}else{return 0}"
},
radio button with parent: {
	notifID: "i5ttATik5Iklr7AhGkYG",
	title: "Favorite color",
	body: "Please select your favorite color from the list provided.",
	type: "radio",
	price: 1
	defaultValue: "Green",
	labels: [
		"Green",
		"Blue",
		"Red",
		"Yellow"
	],
	parent: "MrkKJsQ1QsPk7L4kbfym",
	shouldShow: "if(value > 50)return true; else return false;"
}

