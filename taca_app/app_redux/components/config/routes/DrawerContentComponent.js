/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import { 
	StyleSheet,
	Image,
	ImageBackground,
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Text, View, Icon } from 'native-base';
import { connect } from 'react-redux';
import { invalidateUser } from '../../../redux/modules/auth';
import { routeNames } from '../../../config/routes';
import { auth } from '../../../lib/server';
import { 
	getGeolocationState, 
	startGeolocationTracking, 
	stopGeolocationTracking,
	backgroundGeolocationReady,
	configureBackgroundGeolocation,
} from '../../../lib/backgroundGeolocation/geolocationUtils';
import TouchableItem from './TouchableItem';

class CustomDrawerContentComponent extends Component {
	constructor(props){
		super(props);

		this.state = {
			geolocationState: false,
			drawerItems: [],
		}
	}

	componentDidMount(){
		this.setState({drawerItems: this.getDrawerItems(this.props)});

		backgroundGeolocationReady()
			.then(async (state) => {
				if(!state.enabled){
					console.log('geolocation is not runnnig. Starting...');
					state = await startGeolocationTracking();
				}
				else{
					console.log('geolocation is already running');
				}

				this.setState({geolocationState: state.enabled});

				let url = state.url;
				let uid = `${this.props.user.uid}`;

				console.log('geolocationstate: ', state);

				if(!(url && url.endsWith(uid))){
					configureBackgroundGeolocation({
						url: 'https://us-central1-taca-dev-2.cloudfunctions.net/locationRestEndpoint?uid=' + uid,
    					autoSync: true,        
					});
				}
			})
			.catch((error) => {
				// this catch should never be reached
				this.setState({geolocationState: false});
				console.error('The catch that should never be reached was? ', error);
			});
	}

	shouldComponentUpdate(nextProps, nextState){
		// console.group('should Componenet Update');
		// 	console.group('current');
		// 		console.log('state: ', this.state);
		// 		console.log('props: ', this.props);
		// 	console.groupEnd();
		// 	console.group('next');
		// 		console.log('state: ', nextState);
		// 		console.log('props: ', nextProps);
		// 	console.groupEnd();
		// console.groupEnd();
		// return true;
		const prevState = this.state;
		const prevProps = this.props;
		let who = null;
		
		for(key in nextProps){
			if(nextProps[key] !== prevProps[key]){ 
				who = key;
				break;
			}
		}
		for(key in nextState){
			if(nextState[key] !== prevState[key]){
				who = key;
				break;
			}
		}
		if(who && who !== 'navigation'){
			return true;
		}
		else{
			return false;
		}
	}

	getDrawerItems = (props) => {
		let drawerItems = [];
		const { descriptors } = props;

		let keys = Object.keys(descriptors);
		for(let i=0; i<keys.length; i++){
			if(descriptors.hasOwnProperty(keys[i])){
				let route = descriptors[keys[i]].state.routeName;
				let title = descriptors[keys[i]].options.title || keys[i];
				drawerItems.push({key: keys[i], route, title});
			}
		}

		return drawerItems;
	}

    navigate = (routeName) => {
		const { navigation } = this.props;
		navigation.closeDrawer();
		navigation.navigate({routeName});
		return;
	}

	toggleGeolocationState = async () => {
		this.setState({geolocationState: !this.state.geolocationState});
		let state = await getGeolocationState();
		if(state.enabled){
			stopGeolocationTracking().then((state) => {
				this.setState({geolocationState: state.enabled});
			})
			.catch((error) => {
				this.setState({geolocationState: false});
				console.log('geolocation error: ', error);
			});
		}
		else{
			startGeolocationTracking().then((state) => {
				this.setState({geolocationState: state.enabled});
			})
			.catch((error) => {
				this.setState({geolocationState: false});
				console.log('geolocation error: ', error);
			});
		}
	}
	
	getIconForTouchable = (value) => {
		let icon;
		switch(value){
			case routeNames.NotificationStack:
				icon = 'notifications';
				break;
			case routeNames.RewardStack:
				icon = 'logo-usd';
				break;
			case routeNames.DiaryStack:
				icon = 'map';
				break;
			case 'marker':
				icon = 'pin';
				break;
			case 'log-out': 
				icon = 'log-out';
				break;
			default:
				icon = 'bug';
		}
		return (
			<View style={styles.drawerIconContainer}>
				<Icon style={styles.drawerIcon} name={icon}/>
			</View>
		);
	}

	/**
	 * Stop background-geolocation and
	 * Sign the user out from Firebase
	 * Then navigate to login
	 */
	_signOut = () => {
        //localStore.removeAll('@database');
		this._setTracking(false);
		auth.doSignOut()
		.catch((error) => {
			console.log('!!- An error occured during the sign out process: ' + error);
		})
		.finally(this._navReset);
	}

    /**
     * handle user logout
     * invalidate user data in reduxStore
     * set root switch navigator to Auth path
     */
    onLogout = () => {
		console.log("logout pressed");
        auth.doSignOut().then(() => {
			this.props.invalidateUser();
        }).catch((error) => {
            console.log('an error occured signing out the user: ', error);
        }).finally(() => {
            this.navigate(routeNames.Auth);
        });
	}

	render(){
		// console.log('DrawerContentComponent render');
		return(
			<SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
				<View style={styles.header}>
					<ImageBackground source={require('../../../resources/background.png')} style={styles.background}>
						<TouchableItem style={styles.backButton} onPress={this.props.navigation.closeDrawer}>
							<Icon style={styles.backButtonIcon} name='format-horizontal-align-left' type='MaterialCommunityIcons'/>
						</TouchableItem>
						<Image
							style={styles.icon}	
							source={require('../../../resources/icon.png')}
						/>
					</ImageBackground>
				</View>
				<View>
					<View style={styles.navigation}>
						{this.state.drawerItems.map(({key, route, title}) => 
							<TouchableItem key={key} onPress={() => this.navigate(route)}>
								<View style={styles.drawerTouchable}>
									{this.getIconForTouchable(route)}
									<Text>{title}</Text>
									{route == routeNames.NotificationStack && this.props.notificationCount ?
										<View style={styles.badge}><Text style={styles.badgeText}>{this.props.notificationCount}</Text></View>
									:
										null
									}
								</View>
							</TouchableItem>
						)}
					</View>
					<View style={styles.break}></View>
					<View>
						<TouchableItem onPress={this.toggleGeolocationState}>
							<View style={styles.drawerTouchable}>
								{this.getIconForTouchable('marker')}
								<Text>GPS tracking: </Text>
								<Text style={this.state.geolocationState ? {color: 'green'} : {color: 'red'}}>{this.state.geolocationState ? 'enabled' : 'disabled'}</Text>
							</View>
						</TouchableItem>
						<TouchableItem onPress={this.onLogout}>
							<View style={styles.drawerTouchable}>
								{this.getIconForTouchable('log-out')}
								<Text>Sign Out</Text>
							</View>
						</TouchableItem>
					</View>
				</View> 
			</SafeAreaView>
		);
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	header: {
		height: 182,
	},
	navigation: {
	},
	dropDown: {
		backgroundColor: 'red',
	},
	break: {
		marginLeft: 20,
		borderBottomColor: '#00000045',
		borderBottomWidth: 1,
	},
	drawerIconContainer: {
		marginHorizontal: 13,
		width: 22,
		alignItems: 'center',
	},
	drawerTouchable: {
		justifyContent: 'flex-start',
		alignItems: 'center',
		flexDirection: 'row',
		marginVertical: 5,
		marginHorizontal: 2,
		paddingVertical: 10,
	},
	drawerButton: {
		justifyContent: 'flex-start',
		marginVertical: 5,
		marginHorizontal: 2,
	},
	icon: {
		borderRadius: 50,
		height: 100,
		width: 100,
	},
	background: {
		height: '100%',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	badgeText: {
		fontSize: 10,
		textAlign: 'center',
		color: 'white',
		fontWeight: 'bold',
	},
	badge: {
		position: 'absolute',
		backgroundColor: 'red',
		borderRadius: 10,
		left: 23,
		top: 5,
		justifyContent: 'center',
		width: 20,
		height: 20,
	},
	backButton: {
		position: 'absolute',
		right: 0,
		top: 0,
		// backgroundColor: 'red',
		paddingVertical: 10,
		paddingHorizontal: 10,
	},
	backButtonIcon: {
		color: 'white',
		fontSize: 28,
	},
});

const mapStoreToProps = (store) => ({
	user: store.auth.user,
	notificationCount: store.notifications.notificationCount,
});

const mapDispatchToProps = {
	invalidateUser,
};

export default connect(mapStoreToProps, mapDispatchToProps)(CustomDrawerContentComponent);