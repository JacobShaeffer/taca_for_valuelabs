/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

// this contains three screens
// the About, Terms and Conditions, and Privacy Policy screens
import React from 'react';
import {
    StyleSheet,
} from 'react-native';
import {
    Text,
    View,
    H3,
    Container,
    Header,
    Left,
    Button,
    Body,
    Title,
    Content,
    Tabs,
    Tab,
    H1,
    TabHeading,
    Icon,
} from 'native-base';
import commonColors from '../../../native-base-theme/variables/commonColor';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const About = () => {  
    return (
        <View style={styles.narrow}>
            <H1 style={styles.heading}>
                About
            </H1>

            <Text style={styles.text}>
                TACA is an app designed for use with the National Household Travel Survey (NHTS). 
            </Text>
            <Text style={styles.text}>
                Its primary purpose is to collect GPS data and allow the user (you) to validate the data collected.
            </Text>
            <Text style={styles.text}>
                Thank you for helping to improve your community.
            </Text>
            <H3 style={styles.header}>
                Questions? Concerns? 
            </H3>
            <Text style={styles.text}>
                If you have any questions or comments about anything related to this app please feel free to contact us at (INSERT MAG CONTACT INFO HERE).
            </Text>
        </View>
    );
}

const Terms = () => { 
    return (
        <View style={styles.narrow}>
            <H3 style={styles.header}>
                AGREEMENT BETWEEN USER AND AZMAG
            </H3>
            <Text style={styles.text}>
                TACA (this app) is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of TACA constitutes your agreement to all such terms, conditions, and notices.
            </Text>

            <H3 style={styles.header}>
                MODIFICATION OF THESE TERMS OF USE
            </H3>
            <Text style={styles.text}>
                AZMAG reserves the right to change the terms, conditions, and notices under which TACA is offered, including but not limited to the charges associated with the use of TACA.
            </Text>

            <H3 style={styles.header}>
                LINKS TO THIRD PARTY SITES
            </H3>
            <Text style={styles.text}>
                TACA may contain links to other Web Sites ("Linked Sites"). The Linked Sites are not under the control of AZMAG and AZMAG is not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. AZMAG is not responsible for webcasting or any other form of transmission received from any Linked Site. AZMAG is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by AZMAG of the site or any association with its operators.
            </Text>

            <H3 style={styles.header}>
                NO UNLAWFUL OR PROHIBITED USE
            </H3>
            <Text style={styles.text}>
                As a condition of your use of TACA, you warrant to AZMAG that you will not use TACA for any purpose that is unlawful or prohibited by these terms, conditions, and notices. You may not use TACA in any manner which could damage, disable, overburden, or impair TACA or interfere with any other party's use and enjoyment of TACA. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for through TACAs.
            </Text>

            <H3 style={styles.header}>
                USE OF COMMUNICATION SERVICES
            </H3>
            <Text style={styles.text}>
                TACA may contain bulletin board services, chat areas, news groups, forums, communities, personal web pages, calendars, and/or other message or communication facilities designed to enable you to communicate with the public at large or with a group (collectively, "Communication Services"), you agree to use the Communication Services only to post, send and receive messages and material that are proper and related to the particular Communication Service. By way of example, and not as a limitation, you agree that when using a Communication Service, you will not:
            </Text>
            <Text style={styles.text}>
                Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others.
                Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful topic, name, material or information.
                Upload files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless you own or control the rights thereto or have received all necessary consents.
                Upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another's computer.
                Advertise or offer to sell or buy any goods or services for any business purpose, unless such Communication Service specifically allows such messages.
                Conduct or forward surveys, contests, pyramid schemes or chain letters.
                Download any file posted by another user of a Communication Service that you know, or reasonably should know, cannot be legally distributed in such manner.
                Falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded.
                Restrict or inhibit any other user from using and enjoying the Communication Services.
                Violate any code of conduct or other guidelines which may be applicable for any particular Communication Service.
                Harvest or otherwise collect information about others, including e-mail addresses, without their consent.
                Violate any applicable laws or regulations.
                AZMAG has no obligation to monitor the Communication Services. However, AZMAG reserves the right to review materials posted to a Communication Service and to remove any materials in its sole discretion. AZMAG reserves the right to terminate your access to any or all of the Communication Services at any time without notice for any reason whatsoever.
            </Text>
            <Text style={styles.text}>
                AZMAG reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in AZMAG's sole discretion.
            </Text>
            <Text style={styles.text}>
                Always use caution when giving out any personally identifying information about yourself or your children in any Communication Service. AZMAG does not control or endorse the content, messages or information found in any Communication Service and, therefore, AZMAG specifically disclaims any liability with regard to the Communication Services and any actions resulting from your participation in any Communication Service. Managers and hosts are not authorized AZMAG spokespersons, and their views do not necessarily reflect those of AZMAG.
            </Text>
            <Text style={styles.text}>
                Materials uploaded to a Communication Service may be subject to posted limitations on usage, reproduction and/or dissemination. You are responsible for adhering to such limitations if you download the materials.
            </Text>

            <H3 style={styles.header}>
                MATERIALS PROVIDED TO AZMAG OR POSTED AT ANY AZMAG WEB SITE
            </H3>
            <Text style={styles.text}>
                AZMAG does not claim ownership of the materials you provide to AZMAG (including feedback and suggestions) or post, upload, input or submit to any AZMAG Web Site or its associated services (collectively "Submissions"). However, by posting, uploading, inputting, providing or submitting your Submission you are granting AZMAG, its affiliated companies and necessary sublicensees permission to use your Submission in connection with the operation of their Internet businesses including, without limitation, the rights to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat your Submission; and to publish your name in connection with your Submission.
            </Text>
            <Text style={styles.text}>
                No compensation will be paid with respect to the use of your Submission, as provided herein. AZMAG is under no obligation to post or use any Submission you may provide and may remove any Submission at any time in AZMAG's sole discretion.
            </Text>
            <Text style={styles.text}>
                By posting, uploading, inputting, providing or submitting your Submission you warrant and represent that you own or otherwise control all of the rights to your Submission as described in this section including, without limitation, all the rights necessary for you to provide, post, upload, input or submit the Submissions.
            </Text>

            <H3 style={styles.header}>
                LIABILITY DISCLAIMER
            </H3>
            <Text style={styles.text}>
                THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH TACA MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. AZMAG AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN TACA AT ANY TIME. ADVICE RECEIVED VIA TACA SHOULD NOT BE RELIED UPON FOR PERSONAL, MEDICAL, LEGAL OR FINANCIAL DECISIONS AND YOU SHOULD CONSULT AN APPROPRIATE PROFESSIONAL FOR SPECIFIC ADVICE TAILORED TO YOUR SITUATION.
            </Text>
            <Text style={styles.text}>
                AZMAG AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON TACA FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED "AS IS" WITHOUT WARRANTY OR CONDITION OF ANY KIND. AZMAG AND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.
            </Text>
            <Text style={styles.text}>
                TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL AZMAG AND/OR ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF TACA, WITH THE DELAY OR INABILITY TO USE TACA OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH TACA, OR OTHERWISE ARISING OUT OF THE USE OF TACA, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF AZMAG OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF TACA, OR WITH ANY OF THESE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING TACA.
            </Text>
            <Text style={styles.text}>
                SERVICE CONTACT : no-reply@azmag.gov
            </Text>

            <H3 style={styles.header}>
                TERMINATION/ACCESS RESTRICTION
            </H3>
            <Text style={styles.text}>
                AZMAG reserves the right, in its sole discretion, to terminate your access to TACA and the related services or any portion thereof at any time, without notice. GENERAL To the maximum extent permitted by law, this agreement is governed by the laws of the State of Washington, U.S.A. and you hereby consent to the exclusive jurisdiction and venue of courts in San Mateo County, California, U.S.A. in all disputes arising out of or relating to the use of TACA. Use of TACA is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph. You agree that no joint venture, partnership, employment, or agency relationship exists between you and AZMAG as a result of this agreement or use of TACA. AZMAG's performance of this agreement is subject to existing laws and legal process, and nothing contained in this agreement is in derogation of AZMAG's right to comply with governmental, court and law enforcement requests or requirements relating to your use of TACA or information provided to or gathered by AZMAG with respect to such use. If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in effect. Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and AZMAG with respect to TACA and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and AZMAG with respect to TACA. A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent an d subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express wish to the parties that this agreement and all related documents be drawn up in English.
            </Text>

            <H3 style={styles.header}>
                COPYRIGHT AND TRADEMARK NOTICES:
            </H3>
            <Text style={styles.text}>
                All contents of TACA are: Copyright 2019 by Maricopa Association of Governments and/or its suppliers. All rights reserved.
            </Text>

            <H3 style={styles.header}>
                TRADEMARKS
            </H3>
            <Text style={styles.text}>
                The names of actual companies and products mentioned herein may be the trademarks of their respective owners.
            </Text>
            <Text style={styles.text}>
                The example companies, organizations, products, people and events depicted herein are fictitious. No association with any real company, organization, product, person, or event is intended or should be inferred.
            </Text>
            <Text style={styles.text}>
                Any rights not expressly granted herein are reserved.
            </Text>

            <H3 style={styles.header}>
                NOTICES AND PROCEDURE FOR MAKING CLAIMS OF COPYRIGHT INFRINGEMENT
            </H3>
            <Text style={styles.text}>
                Pursuant to Title 17, United States Code, Section 512(c)(2), notifications of claimed copyright infringement under United States copyright law should be sent to Service Provider's Designated Agent. ALL INQUIRIES NOT RELEVANT TO THE FOLLOWING PROCEDURE WILL RECEIVE NO RESPONSE. See Notice and Procedure for Making Claims of Copyright Infringement.
            </Text>
        </View>
    );
}

const Privacy = () => { 
    return (
        <View style={styles.narrow}>
            <H1 style={styles.heading}>Privacy Policy</H1>

            <Text style={styles.text}>
                AZMAG is committed to protecting your privacy and developing technology. By using the TACA app, you consent to the data practices described in this statement.
            </Text>

            <H3 style={styles.header}>
                Collection of your Personal Information
            </H3>
            <Text style={styles.text}>
                AZMAG collects personally identifiable information, such as your email address, name, home or work address or telephone number. AZMAG also collects anonymous demographic information, which is not unique to you, such as your ZIP code, age, gender, preferences, interests and favorites.
            </Text>

            <H3 style={styles.header}>
                Use of your Personal Information
            </H3>
            <Text style={styles.text}>
                AZMAG collects and uses your personal information to operate the TACA app and deliver the services found within. AZMAG may also contact you via surveys to conduct research about your opinion of current services or of potential new services that may be offered.
            </Text>
            <Text style={styles.text}>
                AZMAG does not sell, rent or lease its customer lists to third parties. AZMAG may share data with trusted partners to help us perform statistical analysis, send you email or postal mail, provide customer support, or arrange for deliveries. All such third parties are prohibited from using your personal information except to provide these services to AZMAG, and they are required to maintain the confidentiality of your information.
            </Text>
            <Text style={styles.text}>
                AZMAG does not use or disclose sensitive personal information, such as race, religion, or political affiliations, without your explicit consent.
            </Text>
            <Text style={styles.text}>
                AZMAG will disclose your personal information, without notice, only if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on AZMAG or the site; (b) protect and defend the rights or property of AZMAG; and, (c) act under exigent circumstances to protect the personal safety of users of AZMAG, or the public.
            </Text>
            
            <H3 style={styles.header}>
                Security of your Personal Information
            </H3>
            <Text style={styles.text}>
                AZMAG secures your personal information from unauthorized access, use or disclosure. AZMAG secures the personally identifiable information you provide on computer servers in a controlled, secure environment, protected from unauthorized access, use or disclosure. When personal information (such as a credit card number) is transmitted to other Web sites, it is protected through the use of encryption, such as the Secure Socket Layer (SSL) protocol.
            </Text>

            <H3 style={styles.header}>
                Children's Privacy
            </H3>
            <Text style={styles.text}>
                These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions 
            </Text>

            <H3 style={styles.header}>
                Changes to this Statement
            </H3>
            <Text style={styles.text}>
                AZMAG will occasionally update this Statement of Privacy to reflect company and customer feedback. AZMAG encourages you to periodically review this Statement to be informed of how AZMAG is protecting your information.
            </Text>

            <H3 style={styles.header}>
                Contact Information
            </H3>
            <Text style={styles.text}>
                AZMAG welcomes your comments regarding this Statement of Privacy. If you believe that AZMAG has not adhered to this Statement, please contact AZMAG at no-reply@azmag.gov. We will use commercially reasonable efforts to promptly determine and remedy the problem.
            </Text>
        </View>
    );
}

export default class Information extends React.PureComponent {
    constructor(props){
        super(props);
    }

    _goBack = () => {
        this.props.navigation.goBack();
    }

    render(){
        return (
            <Container>
                <Header hasTabs>
                    <Left>
                        <Button transparent onPress={() => this._goBack()}>
                            <Icon name='backburger' type='MaterialCommunityIcons'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>
                            Terms & Conditions
                        </Title>
                    </Body>
                </Header>
                <Tabs>
                    <Tab heading={<TabHeading><Icon name='file-document-outline' type='MaterialCommunityIcons'/></TabHeading>}>
                        <Content>
                            <Terms/>
                        </Content>
                    </Tab>
                    <Tab heading={<TabHeading><Icon name='lock-outline' type='MaterialCommunityIcons'/></TabHeading>}>
                        <Content>
                            <Privacy/>
                        </Content>
                    </Tab>
                    <Tab heading={<TabHeading><Icon name='information-outline' type='MaterialCommunityIcons'/></TabHeading>}>
                        <Content>
                            <About/>
                        </Content>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
	narrow: {
		marginRight: 15,
        marginLeft: 15,
    },
    header: {
        marginTop: 25,
    },
    heading: {
        marginVertical: 35,
    },
    text: {
        marginTop: 5,
        textAlign: 'justify',
    }
});