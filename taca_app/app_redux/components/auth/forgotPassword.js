/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, {PureComponent} from 'react';
import { Keyboard } from 'react-native';
import {
    Button,
    Text,
    Content,
    Label,
    Form,
    Item,
    Input,
    Container,
    Header,
    Left,
    Body,
    Icon,
    Title,
} from 'native-base';
import variables from '../../../native-base-theme/variables/platform';
import { auth } from '../../lib/server';
import Toast from 'react-native-easy-toast';

export default class ForgotPassword extends PureComponent {
    constructor(props){
        super(props);
        this.state = {
            isEmailError: false,
            emailErrorMessage: '',
            email: '',
        }
		this.toastRef = React.createRef();
    }

    _toast = () => {
	    this.toastRef.show('Email has been sent. \nIf you do not get the email in 5 minutes: \ntry again, or check your spam folder.', 20000);
    }

    /**
     * 
     */
    _resetPasswordSuccess = () => {
        Keyboard.dismiss();
        this._toast();
    }

    /**
     * 
     */
    _resetPasswordFailure = (error) => {
        switch(error.code){
            case 'auth/invalid-email':
            case 'auth/user-not-found':
                Keyboard.dismiss();
                this._toast();
                break;
            default:
                this.setState({isEmailError: true, emailErrorMessage: 'a server error occured, try again later'})
                console.error('- ', error);
        }
    }

    /**
     * 
     */
    _tryPasswordReset = () => {
        const {email} = this.state;
        auth.doResetPassword(email)
        .then(this._resetPasswordSuccess)
        .catch(this._resetPasswordFailure);
    }

	/**
	 * simple regex test to verify this.state.email has 
	 * .+ 	one or more of any character
	 * @ 	an at symbol
	 * .+ 	one or more of any character
	 * \.	a period
	 * .+	one or more of any character
	 * in that order
	 */
	_verifyEmail = () => {
        const {email} = this.state;
		if((/.+@.+\..+/i).test(email.toLowerCase()) == false && email.length > 0){
            this.setState({isEmailError: true, emailErrorMessage: 'Improperly formatted email, try again.'});
            return false;
        }
        return true;
    }

    /**
     * 
     */
    _verifyInput = () => {
        var hasProblemOccured = false;

        hasProblemOccured = !this._verifyEmail();
        if(this.state.isEmailError || this.state.isPasswordError){
            hasProblemOccured = true;
        }
		if(!this.state.email || this.state.email.length == 0){
            hasProblemOccured = true;
			this.setState({emailErrorMessage: 'This field cannot be left blank.', isEmailError: true, isLoginModalVisible: false});
        }

        if(hasProblemOccured){
            return;
        }
        else{
            this._tryPasswordReset();
        }
    }
    
    /**
     * 
     */
    _goBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <Container>
                <Content>
                    <Header>
                        <Left>
                            <Button transparent onPress={() => this._goBack()}>
                                <Icon name='backburger' type='MaterialCommunityIcons'/>
                            </Button>
                        </Left>
                        <Body>
                            <Title>
                                Reset Password
                            </Title>
                        </Body>
                    </Header>
                    <Form>
                        <Item stackedLabel error={this.state.isEmailError ? true : false}>
                            {this.state.isEmailError ? <Label style={{color: variables.brandDanger}}>{this.state.emailErrorMessage}</Label> : null}
                            <Icon name='email' type='MaterialCommunityIcons'/>
                            <Input 
                                placeholder="Email"
                                onChangeText={(text) => this.setState({email: String(text).toLowerCase(), isEmailError: false})}
                                onEndEditing={this._verifyEmail}
                                maxLength={128}
                            />
                        </Item>
                    </Form>
                    <Button 
                        block 
                        style={{margin: 15, marginTop: 50}}
                        onPress={this._verifyInput}
                    >
                        <Text>Send Reset Email</Text>
                    </Button>
                </Content>
			    <Toast ref={ref => this.toastRef = ref}/>
            </Container>
        );
    }
}