/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import {Text, View, Col, Card} from 'native-base';
import moment from 'moment';

const TimelineCard = ({data, currentDate, index, last}) => {
    return (
		<Col size={20}>
			{
				index !== last ?
					<View style={{
							flex: 1, 
							paddingTop: 8,
							paddingBottom: 8,
							paddingRight: 3,
							paddingLeft: 3,
							margin: 2,
							justifyContent: 'flex-start'
					}}>
						<Card>
							<Text style={{textAlign: 'center', fontSize: 12, color: 'black'}}>
							{
								moment(data.begin).isBefore(moment(currentDate), 'day') ?
									moment(data.begin).format('M/D h:mm A')
								:
									moment(data.begin).format('h:mm A')
							}
							</Text>
						</Card>
					</View>
				:
					<View style={{
							flex: 1, 
							paddingTop: 8,
							paddingBottom: 8,
							paddingRight: 3,
							paddingLeft: 3,
							margin: 2,
							justifyContent: 'space-between'
					}}>
						<Card>
							<Text style={{textAlign: 'center', fontSize: 12, color: 'black'}}>
								{
									moment(data.begin).isBefore(moment(currentDate), 'day') ?
										moment(data.begin).format('M/D h:mm A')
									:
										moment(data.begin).format('h:mm A')
								}
							</Text>
						</Card>
						<Card>
							<Text style={{textAlign: 'center', fontSize: 12, color: 'black'}}>
								{
									moment(data.end).isAfter(moment(currentDate), 'day') ?
										moment(data.end).format('M/D h:mm A')
									:
										moment(data.end).format('h:mm A')
								}
							</Text>
						</Card>
					</View>
			}
		</Col>
	)
}

export default TimelineCard;