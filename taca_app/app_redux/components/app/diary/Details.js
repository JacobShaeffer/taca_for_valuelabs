/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { 
	Container, 
	View,
} from 'native-base';
import { AndroidBackHandler } from 'react-navigation-backhandler';
import NavHeader from '../../general/navHeader';
import DetailsMap from './details/DetailsMap';
import Content from './details/Content';
import { isNumeric } from '../../../lib/helpers';
import ConfirmDeleteModal from './details/ConfirmDeleteModal';
import { setDetailsData, hideDeleteModal, invalidateDetailsStore } from '../../../redux/modules/details';

class Details extends React.Component {

	constructor(props){
		super(props);
	}

	componentDidUpdate(prevProps){
		if(JSON.stringify(this.props.mapData) !== JSON.stringify(prevProps.mapData)){
			const { mapData, selectedIndex } = this.props;

			if(mapData && isNumeric(selectedIndex) && mapData[selectedIndex]){
				this.props.setDetailsData(mapData[selectedIndex]);
			}
		}
	}

	_goBack = () => {
		this.props.invalidateDetailsStore();
		this.props.navigation.goBack(null);
	}

	onBackButtonPress = () => {
		if(this.props.isDeleteModalVisible){
			this.props.hideDeleteModal();
			return true;
		}
		else{
			return false;
		}
	}

	render(){
		return (
			<AndroidBackHandler onBackPress={this.onBackButtonPress}>
				<Container>
					<NavHeader 
						stackHeader
						customBackAction={this.onBackButtonPress}
					>
						Verify Travel
					</NavHeader>
					<View style={styles.content}>
						<DetailsMap/>
						<View style={styles.bottom}>
							<Content navigation={this.props.navigation} goBack={this._goBack}/>
						</View>
						<ConfirmDeleteModal goBack={this._goBack}/>
					</View>
				</Container>
			</AndroidBackHandler>
		);
	}
}

const styles = StyleSheet.create({
	content: {
		flex: 1,
	},
	bottom: {
		flex: 1,
	},
});

const mapStoreToProps = (store) => ({
	currentDate: 			store.diary.currentDate,
    mapData: 				store.diary.mapData,
	selectedIndex: 			store.diary.selectedIndex,
	isDeleteModalVisible: 	store.details.isDeleteModalVisible,
});

const mapDispatchToProps = {
	hideDeleteModal,
	invalidateDetailsStore,
	setDetailsData,
}

export default connect(mapStoreToProps, mapDispatchToProps)(Details);