/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { PureComponent } from 'react';
import { 
    Image,
    StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import variables from '../../../../native-base-theme/variables/platform';

const imageWidth = 300;
const imageHeight = Math.floor(9 * imageWidth / 16);

class StaticMap extends PureComponent {
    constructor(props) {
        super(props);
    }

    getMapUri(props) {
        let { polyline, stop, inProgress, } = props;
        let uri;
        if (inProgress) {
            polyline = [];
            for (let i = 0; i < inProgress.data.length; i++) {
                polyline.push({ latitude: inProgress.data[i].coords.latitude, longitude: inProgress.data[i].coords.longitude });
            }
        }
        if (polyline) {
            const enc = this.encodePolyLine(polyline);
            uri = this.polyLineUri(enc);
        }
        else if (stop) {
            uri = this.stopUri(stop);
        }
        else {
            uri = '&center=Phoenix,AZ&zoom=9';
		}
        uri = this.getUri(imageWidth, imageHeight, uri);
        return { mapUri: uri, isUriReady: true};
    }

    getUri(width, height, content){
        const domain = 'https://maps.googleapis.com/maps/api/staticmap?';
        const size = `size=${width}x${height}&scale=2`;
        const mapType = '&maptype=roadmap';
        const key = '&key=AIzaSyC3W35u8ArCD0IL6-osYHKKmQCvW_jDfSE';//TODO: put api key here
        let uri = domain + size + mapType + content + key;
        return uri;
    }

    polyLineUri(encoding){
        const pipe = '|';
        let uri = `&path=weight:3${pipe}color:0x${variables.brandPrimary.substring(1)}FF${pipe}enc:${encoding}`;
        return uri;
    }

    stopUri(stop){
        const pipe = '|';
        let uri = `&markers=size:mid${pipe}color:0x${variables.brandPrimary.substring(1)}${pipe}${stop.latitude},${stop.longitude}`;
        return uri;
    }

    /**
     * encodes an array of geopoints according to Google's encoding format
     * see https://developers.google.com/maps/documentation/utilities/polylinealgorithm
     * @param {{latitude: number, longitude: number}[]} polyline 
     */
    encodePolyLine(polyline){
        let gps = this.props.gps;
        let start, finish;
        if(gps){
            start = gps.start;
            finish = gps.finish;
        }
        var enc = '';
        var previous = {latitude: 0, longitude: 0};
        
        if(start){
            enc += this.encodeSinglePoint(start, previous);
            previous = start;
        }
        for(var i=0; i<polyline.length; i++){
            let current = polyline[i];
            enc += this.encodeSinglePoint(current, previous);
            previous = current;
        }
        if(finish){
            enc += this.encodeSinglePoint(finish, previous);
        }
        return enc;
    }

    /**
     * encodes a single geopoint 
     * see encodePolyLine()
     * @param {{latitude: number, longitude: number}} point 
     * @param {{latitude: number, longitude: number}} previous the geopoint before 'point' in the polyline, used to calculate offset 
     */
    encodeSinglePoint(point, previous){
        let offset = [point.latitude - previous.latitude, point.longitude - previous.longitude];
        let enc = '';
        enc += this.encodeSingleValue(offset[0]) + this.encodeSingleValue(offset[1]);
        return enc;
    }

    /**
     * encodes single value
     * see encodeSinglePoint or encodePolyLine 
     * @param {number} value 
     */
    encodeSingleValue(value) {
        let isNegative = value < 0 ? true : false;

        //step 2
        value *= 100000;
        value = Math.round(value);
        //step 4
        value = value << 1;
        //step 5
        if(isNegative) value = ~value;
        //step 3
        value = value.toString(2);
        var v = [];
        for(var i=0; i<value.length; i++){
            v.push(value.charAt(i));
        }
        //the previous steps are out of order but this is more efficient in javascript to perform them in this order

        //step 6
        value = [];
        var temp = '';
        for(i=v.length-1; i>=0; i--){
            temp = v[i] + temp;
            if(temp.length === 5){
                value.push(temp);
                temp = '';
            }
        }
        if(temp.length > 0){
            temp = new Array(5 - temp.length + 1).join('0') + temp;
            value.push(temp);
        }

        //steps 7, 8, 9, 10, 11
        for(var i=0; i<value.length-1; i++){
            let parseSafe = value[i].replace(/[^01]/gi, '');
            value[i] = String.fromCharCode(parseInt(parseSafe, 2) + 32 + 63);
        }
        let parseSafe = value[value.length-1].replace(/[^01]/gi, '');
        value[value.length-1] = String.fromCharCode(parseInt(parseSafe, 2) + 63);

        return value.join('');
    }

    render() {
        const {
            mapUri,
            isUriReady,
		} = this.getMapUri(this.props);
		
        return (
            isUriReady ? 
                <Image 
                    style={styles.image} 
                    source={{uri: mapUri}}
                    resizeMode='contain'
                />
            : 
                null
        );
    }
}

const styles = StyleSheet.create({
    image: {
		// borderColor: 'red',
		// borderWidth: 1,
        // flex: 1,
        // height: undefined,
		// width: 300,
		...StyleSheet.absoluteFill,
    }
})

StaticMap.propTypes = {
    polyline: PropTypes.array,
    stop: PropTypes.object,
    style: PropTypes.object,
}

export default StaticMap;