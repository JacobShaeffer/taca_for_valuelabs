/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
	Content, Left,
	ListItem, Text,
	Right, Body, Icon, 
	Button, List,
	H1, View, Grid, Col, Picker, Row,
} from 'native-base';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import variables from '../../../../../native-base-theme/variables/platform';
import { showDeleteModal, updateChanges } from '../../../../redux/modules/details';
import { isNumeric } from '../../../../lib/helpers';
import { validateExistingTrip } from '../../../../redux/modules/diary';
import { setNewStopCoords } from '../../../../redux/modules/newStop';
import { getModeIcon, getActivityIcon, getModeName, getActivityName, activities, modes } from '../../../../config/constants';
import { throttle } from '../../../../lib/helpers';

class DetailContent extends React.PureComponent { 

	constructor(props){
		super(props);

		this.state = {
			isBeginModalVisible: false,
			isEndModalVisible: false,
			isShowMore: false,
		}
	}

	_onTimeCancel = () => {
		this.setState({isBeginModalVisible: false, isEndModalVisible: false});
	}

	_timeConfirmed = (date, who) => {
		let { updateChanges } = this.props;
		date = moment(date).toDate();

		if(who === 'begin'){
			updateChanges({begin: date});
		}
		else{
			updateChanges({end: date});
		}
		this._onTimeCancel();
	}
	
	_updateActivityMode = (newActivityMode) => {
		let { updateChanges, data } = this.props;

		if(data.type === 'travel'){
			updateChanges({mode: newActivityMode});
		}
		else{
			updateChanges({activity: newActivityMode});
		}
	}

	_saveUpdatedData = () => {
		const { 
			validateExistingTrip,
			selectedIndex, 
			updateKeys, user,
			changes, goBack,
			data,
		} = this.props;

		if(selectedIndex && isNumeric(selectedIndex) && updateKeys && updateKeys.length > selectedIndex){
			const uKey = updateKeys[selectedIndex];
			validateExistingTrip({
				uid: user.uid, 
				tid: uKey, 
				selectedIndex, 
				updates: changes, 
				date: data.begin,
			}, data.userCreated);
			goBack();
		}
		else{ 
			// A serious problem occured for this to be the case
			console.error(
				`An error occured attempting to verify a travel 
				selectedIndex: ${selectedIndex} 
				updateKeys.length: ${updateKeys.length}`
			);
		}
	}

    _create = throttle(() => {
		const { navigation, setNewStopCoords, data } = this.props;
		setNewStopCoords(data);
		navigation.navigate({routeName: 'Create'});
    }, 1500);

	_onDeleteCreatePress = () => {
		const { data, showDeleteModal } = this.props;
		if(data.type === 'travel'){
			this._create();
		}
		else{
			showDeleteModal()
		}
	}

	render() {
		const {currentDate, changes} = this.props;
		let data = {...this.props.data, ...changes};

		if(!data){
			// an error occured somewhere
			if(data === null){
				return null;
			}
			else{
				return (
					<Content>
						<View style={styles.errorContainer}>
							<H1 style={styles.error}>An error occured</H1>
							{/* TODO: make this look better */}
						</View>
					</Content>
				)
			}
		}

		let labels;
		if(data.type === 'travel'){
			labels = {
				timeStartLabel: 'Departure',
				timeStartIcon: 'log-out',
				timeEndLabel: 'Arrival',
				timeEndIcon: 'log-in',
				activityModeName: getModeName(data.mode),
				activityModeValue: data.mode,
				activityModeLabel: 'Mode',
				activityModeIcon: getModeIcon(data.mode), 
				activityModeConstants: modes,
				deleteCreateType: 'travel',
				deleteCreatePhrase: 'Add missing Stop:',
				deleteCreateIcon: 'map-marker-plus',
			}
		}
		else{
			labels = {
				timeStartLabel: 'Arrival',
				timeStartIcon: 'log-in',
				timeEndLabel: 'Departure',
				timeEndIcon: 'log-out',
				activityModeName: getActivityName(data.activity),
				activityModeValue: data.activity,
				activityModeLabel: 'Activity',
				activityModeIcon: getActivityIcon(data.activity), 
				activityModeConstants: activities,
				deleteCreateType: 'stop',
				deleteCreatePhrase: 'Delete Stop:',
				deleteCreateIcon: 'delete',
			}
		}

		return (
			<Content>
				<List>
					<ListItem>
						<Grid>
							<Row>
								<Body style={[styles.textVerticalCenter]}>
									<Text style={styles.centerText}>
										{moment(currentDate).format('dddd, MMMM Do YYYY')}
									</Text>
								</Body>
								<Right>
									<Button transparent small dark icon onPress={() => this.setState((state) => {return {isShowMore: !state.isShowMore}})}>
										<Icon name='dots-horizontal' type='MaterialCommunityIcons'/>
									</Button>
								</Right>
							</Row>
							<Row style={[{display: this.state.isShowMore ? 'flex' : 'none'}, styles.dropDown]}>
								<Body style={[styles.textVerticalCenter]}>
									<Text style={styles.centerText}>
										{labels.deleteCreatePhrase}
									</Text>
								</Body>
								<Right>
									<Button success={labels.deleteCreateType === 'travel'} danger={labels.deleteCreateType === 'stop'} icon onPress={this._onDeleteCreatePress}>
										<Icon name={labels.deleteCreateIcon} type='MaterialCommunityIcons'/>
									</Button>
								</Right>
							</Row>
						</Grid>
					</ListItem>
					<ListItem icon>
						<Left>
							<Icon name={labels.timeStartIcon}/>
						</Left>
						<Body>
							<Grid>
								<Col style={styles.labelContainer}>
									<Text>
										{labels.timeStartLabel}
									</Text>
								</Col>
								<Col style={styles.interactionContainer}>
									<Button style={styles.centerButton} bordered small onPress={() => this.setState({isBeginModalVisible: true})}>
										<Text style={styles.buttonText}>
											{moment(data.begin).format('h:mm a')}
										</Text>
									</Button>
								</Col>
							</Grid>
						</Body>
					</ListItem>
					<ListItem icon>
						<Left>
							<Icon style={styles.materialIcon} type='MaterialCommunityIcons' name={labels.activityModeIcon}/>
						</Left>
						<Body>
							<Grid>
								<Col style={styles.labelContainer}>
									<Text>
										{labels.activityModeLabel}
									</Text>
								</Col>
								<Col style={styles.interactionContainer}>
								{/* TODO: update this to use a custom picker because this one sucks */}
									<Picker 
										style={styles.travelPicker} 
										selectedValue={labels.activityModeValue}
										onValueChange={this._updateActivityMode}
									>
										{labels.activityModeConstants.map(({name, icon, value}) => 
											<Picker.Item label={name} value={value} key={value}/>
										)}
									</Picker>
								</Col>
							</Grid>
						</Body>
					</ListItem>
					<ListItem icon>
						<Left>
							<Icon name={labels.timeEndIcon} />
						</Left>
						<Body>
							<Grid>
								<Col style={styles.labelContainer}>
									<Text>
										{labels.timeEndLabel}
									</Text>
								</Col>
								<Col style={styles.interactionContainer}>
									<Button style={styles.centerButton} bordered small onPress={() => this.setState({isEndModalVisible: true})}>
										<Text style={styles.buttonText}>
											{moment(data.end).format('h:mm a')}
										</Text>
									</Button>
								</Col>
							</Grid>
						</Body>
					</ListItem>
					<ListItem>
						<Body>
							<Button block onPress={this._saveUpdatedData}>
								<Text>verify</Text>
							</Button>
						</Body>
					</ListItem>
				</List>
				<DateTimePicker
					date={moment(data.begin).toDate()}
					isVisible={this.state.isBeginModalVisible}
					onConfirm={(date) => this._timeConfirmed(date, 'begin')}
					onCancel={this._onTimeCancel}
					is24Hour={false}
					mode='time'
				/>
				<DateTimePicker
					date={moment(data.end).toDate()}
					isVisible={this.state.isEndModalVisible}
					onConfirm={(date) => this._timeConfirmed(date, 'end')}
					onCancel={this._onTimeCancel}
					is24Hour={false}
					mode='time'
				/>
			</Content> 
		);
	}
}

const styles = StyleSheet.create({
	error: {
		color: 'red',
		textAlign: 'center',
	},
	dropDown: {
		marginLeft: 18,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: 'grey',
		paddingTop: 15,
		marginTop: 10,
	},
	text: {
		color: variables.brandPrimary,
		paddingRight: 5,
	},
	textVerticalCenter: {
		justifyContent: 'center',
	},
	centerText: {
		textAlign: 'center'
	},
	errorContainer: {
	},
	materialIcon: {
		fontSize: 23,
	},
	right: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	labelContainer: {
		justifyContent: 'center',
	},
	interactionContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	travelPicker: {
		width: 150,
	},
	centerButton: {
		alignSelf: 'center',
	},
});

const mapStoreToProps = (store) => ({
    user: 			store.auth.user,
    currentDate:	store.diary.currentDate,
	updateKeys: 	store.diary.updateKeys,
    mapData: 		store.diary.mapData,
	selectedIndex: 	store.diary.selectedIndex,
	data: 			store.details.detailsData,
	changes: 		store.details.changes,
});

const mapDispatchToProps = {
	validateExistingTrip,
	updateChanges,
	showDeleteModal,
	setNewStopCoords,
}

export default connect(mapStoreToProps, mapDispatchToProps)(DetailContent);