/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React,{Component} from 'react'
import {PanResponder,View} from 'react-native'
import Svg,{Path,Circle,G,Text} from 'react-native-svg'

class CircularSlider extends Component {
	constructor(props){
		super(props)

		this.handlePanResponderMoveBegin = this.handlePanResponderMoveBegin.bind(this);
		this.handlePanResponderMoveEnd = this.handlePanResponderMoveEnd.bind(this);
		this.cartesianToPolar = this.cartesianToPolar.bind(this);
		this.polarToCartesian = this.polarToCartesian.bind(this);

		const {width,height} = props
		const smallestSide = (Math.min(width,height))

		this.state = {
			cx: width/2,
			cy: height/2,
			r: (smallestSide/2)*0.85,
		}
	}

	componentWillMount = () => {
		this._panResponderBegin = PanResponder.create({
			onStartShouldSetPanResponder: () => true,
			onMoveShouldSetPanResponder: () => true,
			onPanResponderMove: this.handlePanResponderMoveBegin
		});
		this._panResponderEnd = PanResponder.create({
			onStartShouldSetPanResponder: () => true,
			onMoveShouldSetPanResponder: () => true,
			onPanResponderMove: this.handlePanResponderMoveEnd
		});
	}

	polarToCartesian(angle){
		const {cx,cy,r} = this.state;
		const a = (angle-270) * Math.PI / 180.0;
		const x = cx + (r * Math.cos(a));
		const y = cy + (r * Math.sin(a));
		return {x,y}
	}

	cartesianToPolar(x,y){
		const {cx,cy} = this.state;
		return Math.round((Math.atan((y-cy)/(x-cx)))/(Math.PI/180)+((x>cx) ? 270 : 90));
	}

	handlePanResponderMoveBegin({nativeEvent:{locationX,locationY}}){
		let polarValue = this.cartesianToPolar(locationX,locationY);
		this.props.onValueBeginChange(polarValue);
	}

	handlePanResponderMoveEnd({nativeEvent:{locationX,locationY}}){
		let polarValue = this.cartesianToPolar(locationX,locationY);
		this.props.onValueEndChange(polarValue);
	}

	render(){
		const {width,height,valueBegin,valueEnd,meterColor,textColor} = this.props;
		const {cx,cy,r} = this.state;
		const startCoord = this.polarToCartesian(valueBegin);
		const endCoord = this.polarToCartesian(valueEnd);
		const largeArc = 0;
		return (
			<Svg onLayout={this.onLayout} width={width} height={height}>
				<Circle cx={cx} cy={cy} r={r} stroke='#eee' strokeWidth={2} fill='none'/>
				<Path 
					stroke={meterColor} 
					strokeWidth={10} 
					fill='none'
					//${valueEnd - valueBegin>180?1:0} 1 
					d={`M${startCoord.x} ${startCoord.y} A ${r} ${r} 0 ${largeArc} 1 ${endCoord.x} ${endCoord.y}`}
				/>
				<G x={endCoord.x-7.5} y={endCoord.y-7.5}>
					<Circle cx={7.5} cy={7.5} r={10} fill={meterColor} {...this._panResponderEnd.panHandlers}/>
					<Text key={valueEnd+''} x={7.5} y={12.5} fontSize={10} fill={textColor} textAnchor="middle">
						{valueEnd+''}
					</Text>
				</G>
				<G x={startCoord.x-7.5} y={startCoord.y-7.5}>
					<Circle cx={7.5} cy={7.5} r={10} fill={meterColor} {...this._panResponderBegin.panHandlers}/>
					<Text key={valueBegin+''} x={7.5} y={1} fontSize={10} fill={textColor} textAnchor="middle">
						{valueBegin+''}
					</Text>
				</G>
			</Svg>
		)
	}
}

export default CircularSlider
// import CircularClider from '../../components/app/diary/details/create/CircularSlider';
							{/* <CircularClider 
								width={150} 
								height={150} 
								meterColor='#f0a' 
								textColor='#000'
								valueBegin={this.state.sliderBegin} 
								valueEnd={this.state.sliderEnd}
								onValueBeginChange={(value)=>this.setState({sliderBegin: value})}
								onValueEndChange={(value)=>this.setState({sliderEnd: value})}
							/> */}