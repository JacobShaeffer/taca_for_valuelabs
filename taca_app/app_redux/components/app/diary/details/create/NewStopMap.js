/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import { Button, Text, View, Icon } from 'native-base';
import MapView, { PROVIDER_GOOGLE, Callout } from 'react-native-maps';
import { connect } from 'react-redux';
import { updateDetailsData } from '../../../../../redux/modules/details';
import variables from '../../../../../../native-base-theme/variables/platform';

// start by asking for when the stop occurred
// then with the time data we can deduce the location on the map for the new stop
// assume the travel is all at the same speed and interpolate the appropriate point to put the stop at

class Map extends PureComponent {

	constructor(props){
		super(props);
		this.mapRef = null;
		this.markerRef = null;
	}

	renderTravel = (data) => {
		let coords = [];
		if(data.gpsStart){
			coords.push(data.gpsStart);
		}
		data.coords.forEach((element) => {
			coords.push(element);
		});
		if(data.gpsFinish){
			coords.push(data.gpsFinish);
		}
		//TODO: pick better colors
		return [
			<MapView.Marker
				key={'marker1'}
				coordinate={coords[0]}
				pinColor={variables.brandSuccess}
			>
			</MapView.Marker>,
			<MapView.Polyline
				key={'polyline'}
				coordinates={coords}
				strokeWidth={2}
			/>,
			<MapView.Marker
				key={'marker2'}
				coordinate={coords[coords.length-1]} 
				pinColor={variables.brandDanger}
			>
			</MapView.Marker>,
		]
	}

	renderNewStop = (data) => {
		if(data && data.coords){
			return (
				<MapView.Marker
					key={'newStop'}	
					coordinate={data.coords}
					pinColor={variables.brandPrimary}
					draggable
					onDragStart={() => {
						this.markerRef.hideCallout();
					}}
					onDragEnd={this.props.onMarkerDragged}
					ref={(ref) => {
						this.markerRef = ref; 
						this.props.markerRef(ref);
					}}
				>
					<Callout><Text>Drag me!</Text></Callout>
				</MapView.Marker>
			)
		}
		else{
			return null;
		}
	}


    fitMapToCoordinates = () => {
		const { data } = this.props;
        if(data.type === 'travel'){
			let edgePadding = {
				top: 40,
				left: 25,
				bottom: 15,
				right: 25,
			};
            this.mapRef.fitToCoordinates(data.coords, {edgePadding, animated: false});
        }
	}
	
	render(){
		// const { data, stop, onMarkerDragged } = this.props;
		const { data, stop } = this.props;
		return (
			<View style={styles.container}>
				<MapView
					style={styles.map}	
					provider={PROVIDER_GOOGLE}
					initialRegion={data.region}
					ref={(ref) => this.mapRef = ref}
					onLayout={this.fitMapToCoordinates}
				>
					{this.renderTravel(data)}
					{this.renderNewStop(stop)}
				</MapView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	map: {
		...StyleSheet.absoluteFillObject,
	},
	button: {
		position: 'relative',
	},
	container: {
		position: 'relative',
		flex: 1,
	},
});

const mapStoreToProps = (store) => ({
	data: store.details.detailsData,
	stop: store.newStop.stop,
});

const mapDispatchToProps = {
	updateDetailsData,
}

export default connect(mapStoreToProps, mapDispatchToProps)(Map);