/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Button, Text, View } from 'native-base';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { connect } from 'react-redux';
import { updateDetailsData } from '../../../../redux/modules/details';
import variables from '../../../../../native-base-theme/variables/platform';

class DetailsMap extends Component {

	constructor(props){
		super(props);

		this.mapRef = null;
	}

	renderMapData = (data) => {
		if(!data){
			// No data, render nothing on th map
			return null;
		}

		if(data.type === 'stop'){
			return this.renderStop(data.coords);
		}
		else if(data.type === 'travel'){
			return this.renderTravel(data);
		}
	}

	handleStopMoved = (event) => {
		console.log('pin moved event: ', event.nativeEvent);	
	}

	renderStop = (coords) => (
		<MapView.Marker
			coordinate={coords}
			draggable={true}
			onDragEnd={(event) => {this.handleStopMoved(event)}}		
			pinColor={'pink'}//TODO:
		/>
	)

	renderTravel = (data) => {
		let coords = [];
		if(data.gpsStart){
			coords.push(data.gpsStart);
		}
		data.coords.forEach((element) => {
			coords.push(element);
		});
		if(data.gpsFinish){
			coords.push(data.gpsFinish);
		}
		//TODO: pick better colors
		return [
			<MapView.Marker
				key={'marker1'}
				coordinate={coords[0]}
				pinColor={variables.brandSuccess}
			>
			</MapView.Marker>,
			<MapView.Polyline
				key={'polyline'}
				coordinates={coords}
				strokeWidth={2}
			/>,
			<MapView.Marker
				key={'marker2'}
				coordinate={coords[coords.length-1]} 
				pinColor={variables.brandDanger}
			>
			</MapView.Marker>
		]
	}

    fitMapToCoordinates = () => {
		const { data } = this.props;
        if(data.type === 'travel'){
			let edgePadding = {
				top: 40,
				left: 25,
				bottom: 15,
				right: 25,
			};
			//NOTE: if you want to make this animated then try changing the mapView prop from onLayout to onMapReady
            this.mapRef.fitToCoordinates(data.coords, {edgePadding, animated: false});
        }
    }

	render(){
		const { data } = this.props;
		if(data){
			return (
				<View style={styles.container}>
					<MapView
						style={styles.map}	
						provider={PROVIDER_GOOGLE}
						initialRegion={data.region}
						ref={(ref) => this.mapRef = ref}
						onLayout={this.fitMapToCoordinates}
					>
						{this.renderMapData(data)}	
					</MapView>
				</View>
			);
		}
		else return null
	}
}

const styles = StyleSheet.create({
	map: {
		...StyleSheet.absoluteFillObject,
	},
	button: {
		position: 'relative',
	},
	container: {
		position: 'relative',
		flex: 1,
	},
});

const mapStoreToProps = (store) => ({
    data: store.details.detailsData,
});

const mapDispatchToProps = {
	updateDetailsData,
}

export default connect(mapStoreToProps, mapDispatchToProps)(DetailsMap);





	// this.state = {
	// 	mapHeight: new Animated.Value(1),
	// }
	// let { mapHeight } = this.state;
	// <Animated.View style={[{flex: mapHeight}]}>
	// </Animated.View>

	// _grow = () => {
	// 	Animated.timing(
	// 		this.state.mapHeight,
	// 		{
	// 			toValue: 2,
	// 			duration: 200,
	// 		}
	// 	).start();
	// }

	// _shrink = () => {
	// 	Animated.timing(
	// 		this.state.mapHeight,
	// 		{
	// 			toValue: 1,
	// 			duration: 200,
	// 		}
	// 	).start();
	// }