/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import { View, Text, Button } from 'native-base';
import { deleteExistingData } from '../../../../redux/modules/diary';
import { hideDeleteModal } from '../../../../redux/modules/details';
import variables from '../../../../../native-base-theme/variables/platform';


const ConfirmDeleteModal = (props) => {

	_onDeleteCancel = () => {
		props.hideDeleteModal();
	}

	_onDeleteConfirm = () => {
		const {
			deleteExistingData,
			user, updateKeys,
			selectedIndex,
			goBack,
		} = props;
		let uKey = updateKeys[selectedIndex];
		deleteExistingData({
			uid: user.uid, 
			tid: uKey, 
			selectedIndex,
		});
		goBack();
	}

	let dictionary = {
		deleteConfirmModal: {
			prompt1: 'Are you sure you want to ',
			prompt2: 'premanently delete',
			prompt3: ' this stop?',
			confirm: 'Yes Delete',
			reject1: 'No ',
			reject2: 'DO NOT',
			reject3: ' Delete',
		},
	}

	return (
		<View style={props.isDeleteModalVisible ? styles.container : styles.hidden}>
			<View style={styles.modal}>
				<Text style={{paddingBottom: 10}}>{dictionary.deleteConfirmModal.prompt1}
					<Text style={{color: variables.brandDanger}}>{dictionary.deleteConfirmModal.prompt2}</Text>
					{dictionary.deleteConfirmModal.prompt3}
				</Text>
				<Button danger block onPress={_onDeleteConfirm} style={{marginBottom: 10}}>
					<Text>{dictionary.deleteConfirmModal.confirm}</Text>
				</Button>
				<Button warning block onPress={_onDeleteCancel}>
					<Text>{dictionary.deleteConfirmModal.reject1}
						<Text style={{fontWeight: 'bold', color: variables.inverseTextColor}}>{dictionary.deleteConfirmModal.reject2}</Text>
						{dictionary.deleteConfirmModal.reject3}
					</Text>
				</Button>
			</View>
		</View>
	)
}

const styles = StyleSheet.create({
	hidden: {
		display: 'none',
	},
	container: {
		...StyleSheet.absoluteFill,
		backgroundColor: '#55555575',
		position: 'absolute',
		justifyContent: 'center',
		alignItems: 'center',
	},
	modal: {
		margin: 15,
		backgroundColor: 'white',
		borderRadius: 5,
		padding: 10,
	},
})

const mapStoreToProps = (store) => ({
    user: 					store.auth.user,
	updateKeys: 			store.diary.updateKeys,
	selectedIndex: 			store.diary.selectedIndex,
	isDeleteModalVisible: 	store.details.isDeleteModalVisible,
});

const mapDispatchToProps = {
	deleteExistingData,
	hideDeleteModal,
}

export default connect(mapStoreToProps, mapDispatchToProps)(ConfirmDeleteModal);