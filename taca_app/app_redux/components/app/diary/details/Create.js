/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { PureComponent } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { View, Text, Container, Button, H1, Picker, List, ListItem, Card, CardItem } from "native-base";
import { connect } 				from 'react-redux';
import { Slider } 				from 'react-native-gesture-handler';
import { AndroidBackHandler } 	from 'react-navigation-backhandler';
import { addNewStop } 			from '../../../../redux/modules/diary';
import { findNearestSegment } 	from '../../../../lib/helpers';
import NavHeader 				from '../../../general/navHeader';
import NewStopMap 				from './create/NewStopMap';
import variables 				from '../../../../../native-base-theme/variables/platform';
import moment 					from 'moment';
import { activities, getActivityName } from '../../../../config/constants';
import { nextScreenState, previousScreenState, updateStop, updateTravelAfter, updateTravelBefore } from '../../../../redux/modules/newStop';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

const { height, width } = Dimensions.get('window');

class Create extends PureComponent {

	constructor(props){
		super(props);
		this.state = {
			startSliderValue: 50,
			startTimeValue: null,
			endSliderValue: 50,
			endTimeValue: null,
			sliderMax: 100,
		};
		this.NewStopMap_MarkerRef = null;
	}

	componentDidMount(){
		const begin = moment(this.props.mapData.begin);
		const end = moment(this.props.mapData.end);
		const sliderMax = end.diff(begin, 'minutes');

		let startTimeValue = moment(begin);
		startTimeValue.add(sliderMax/2 - 2.5, 'minutes');

		let endTimeValue = moment(begin);
		endTimeValue.add(sliderMax/2 + 2.5, 'minutes');
	
		const startSliderValue = this.getSliderValueFromTime(startTimeValue);
		const endSliderValue = this.getSliderValueFromTime(endTimeValue);


		this.setState({
			startTimeValue,
			endTimeValue,
			startSliderValue,
			endSliderValue,
			sliderMax,
			activityValue: this.props.mapData.activity,
		}, () => console.log(this.state));
	}

	onBackButtonPress = () => {
		if(this.props.screenState > 0){
			this.props.previousScreenState();
			return true;
		}
		return false;
	}

	// ScreenState 0 functions
		getSliderValueFromTime = (time) => {
			time = moment(time);

			let begin = moment(this.props.mapData.begin);
			let value = time.diff(begin, 'minute');

			return value;
		}

		getTimeFromSliderValue = (value) => {
			let time = moment(this.props.mapData.begin);
			time.add(value, 'minutes');

			return moment(time);
		}

		onSlidersChange = (values) => {
			let startTimeValue = this.getTimeFromSliderValue(values[0]);
			let endTimeValue = this.getTimeFromSliderValue(values[1]);
			
			this.setState({
				startTimeValue,
				endTimeValue,
				startSliderValue: values[0],
				endSliderValue: values[1],
			});
		}

		onActivitySelect = (value) => {
			this.setState({
				activityValue: value,
			});
		}

		finishScreenState0 = () => {
			let { activityValue, endTimeValue, startTimeValue } = this.state;
			const { updateStop, updateTravelAfter, updateTravelBefore } = this.props;
			// TODO: update newStop store with state data above

			startTimeValue = moment(startTimeValue).toDate();
			endTimeValue = moment(endTimeValue).toDate()

			updateStop({
				activity: activityValue, //TODO: activityValue is null sometimes (all the time?)
				begin: startTimeValue, 
				end: endTimeValue,
			});
			updateTravelBefore({end: startTimeValue});
			updateTravelAfter({begin: endTimeValue});
			this.NewStopMap_MarkerRef.showCallout();
			
			this.props.nextScreenState();
		}
	// End of ScreenState 0 functions

	// ScreenState 1 functions
		onMarkerDragged = (event) => {
			const coords = {...event.nativeEvent.coordinate};
			this.props.updateStop({
				coords,
				locations: [
					{...coords}
				],
			});
		}

		finishScreenState1 = () => {
			const { 
				travelAfter, 
				travelBefore,
				selectedIndex,
				mapData,
				addNewStop,
				navigation,
				user, 
				updateKeys,
			} = this.props;
			let { stop } = this.props;

			stop = {
				...stop,
				userGenerated: true,
				type: 'stop',
			};
			const beforeCount = findNearestSegment(mapData.coords, stop.coords);
			let beforeLocations = [], afterLocations = [];
			let counter = 0;
			mapData.coords.forEach(element => {
				if(counter < beforeCount){
					beforeLocations.push({...element});
					counter++;
				}
				else{
					afterLocations.push({...element});
				}
			});
			const before = {
				...travelBefore,
				count: beforeCount,
				coords: beforeLocations,
			};
			const after = {
				...mapData,
				...travelAfter,
				count: mapData.coords.length - beforeCount,
				coords: afterLocations,
				userGenerated: true,
			};

			addNewStop({
				uid: user.uid, 
				tid: updateKeys[selectedIndex], 
				before, 
				stop, 
				after, 
				index: selectedIndex
			});
			navigation.dispatch(StackActions.popToTop());
		}
	// End of ScreenState 1 functions

	renderScreenState = () => {
		switch(this.props.screenState){
			case 0: 
				return <>
					<View style={[styles.modal, styles.panel]}>
						<View>
							<H1>Step 1:</H1>
							<Text>What and When</Text>
						</View>
						{/* <View style={styles.centerContent}> */}
						<View style={styles.content}>
							<View style={styles.selectorAreaPadding}></View>
							<Card>
								<CardItem>
									<View style={styles.selectorContainer}>
										<Text>Activity</Text>
										<Picker 
											style={styles.picker} 
											selectedValue={this.state.activityValue}
											onValueChange={this.onActivitySelect}
										>
											{activities.map(({name, icon, value}) => 
												<Picker.Item label={name} value={value} key={value}/>
											)}
										</Picker>
									</View>
								</CardItem>
							</Card>
							<Card>
								<CardItem>
									<View style={styles.selectorContainer}>
										<View style={styles.displayDate}>
											<View> 
												<Text>Start</Text>
											</View>
											<View> 
												<Text>End</Text>
											</View>
										</View>
										<MultiSlider 
											values={[this.state.startSliderValue, this.state.endSliderValue]}
											max={this.state.sliderMax}
											step={1}
											snapped
											allowOverlap={false}
											onValuesChange={this.onSlidersChange}
											selectedStyle={{backgroundColor: variables.brandPrimary}}
											unselectedStyle={{backgroundColor: variables.brandLight}}
											customMarker={
												(props) => 
													props.pressed ? 
														<View style={{height: 20, width: 20, borderRadius: 10, backgroundColor: variables.brandDark}}></View>
													: 
														<View style={{height: 12, width: 12, borderRadius: 10, backgroundColor: variables.brandDark}}></View>
											}
										></MultiSlider>	
										<View style={styles.displayDate}>
											<View style={styles.timeLabelLarge}> 
												<Text style={styles.centerText}>{moment(this.state.startTimeValue).format('H:mm A')}</Text>
											</View>
											<View style={styles.timeLabelLarge}> 
												<Text style={styles.centerText}>{moment(this.state.endTimeValue).format('H:mm A')}</Text>
											</View>
										</View>
									</View>
								</CardItem>
							</Card>
								<View style={styles.selectorAreaPadding}></View>
							</View>
							<View>
								<Button style={styles.flexEnd} onPress={this.finishScreenState0}>
									<Text>
										Next
									</Text>
								</Button>
							</View>
						</View>
					</>
				case 1: 
					return <>
						<View style={[styles.modal, styles.prompt]}>
							<H1>Step 2:</H1>
							<Text>Where</Text>
						</View>
						<View style={styles.button}>
							<Button block style={styles.flexEnd} onPress={this.finishScreenState1}>
								<Text>Finish</Text>
							</Button>
						</View>
					</>
				default: 
					return null;
			}
		}

		render(){
			return (
				<AndroidBackHandler onBackPress={this.onBackButtonPress}>
				<Container>
					<NavHeader 
						stackHeader
						customBackAction={this.onBackButtonPress}
					>
						Add New Stop
					</NavHeader>
					<View style={{flex: 1}}>    
						<View style={{ flex: 1 }}>
							<NewStopMap
								onMarkerDragged = {this.onMarkerDragged}	
								markerRef = {(ref) => this.NewStopMap_MarkerRef = ref}
							/>
					</View>
					{this.renderScreenState()}
				</View>
			</Container>
			</AndroidBackHandler>
		);
	}

}

const dev = {
	borderColor: 'red',
	borderWidth: 1,
}
const styles = StyleSheet.create({
	centerContent: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	content: {
		flex: 1,
	},
	centerText: {
		textAlign: 'center',
	},
	prompt: {
		top: 0, 
		borderColor: 'black', 
		borderStyle: 'dashed', 
		borderWidth: StyleSheet.hairlineWidth,
	},
	panel: {
		top: 0, bottom: 0,
		borderColor: 'black', 
		borderWidth: StyleSheet.hairlineWidth,
	},
	modal: {
		backgroundColor: 'white', 
		position: 'absolute',
		left: 0, right: 0,
		margin: 18,
		paddingVertical: 10,
		paddingHorizontal: 10,
		borderRadius: 4,
	},
	button: {
		position: 'absolute', 
		right: 0, bottom: 0, 
		margin: 25, 
	},
	flexEnd: {
		alignSelf: 'flex-end'
	},
	selectorAreaPadding: {
		flex: 1
	},
	selectorContainer: {
		flex: 3,
	},
	picker: {
		width: width * 0.7,
	},
	slider: {
		width: width * 0.7,
	},
	sliderReversed: {
		transform: [{rotate: '180deg'}]
	},
	displayDate: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 10,
	},
	timeLabelSmall: {
		position: 'absolute',
	},
	timeLabelLarge: {
		flex: 1,
	},
});

mapStoreToProps = (store) => ({
    user: 			store.auth.user,
	updateKeys: 	store.diary.updateKeys,
	selectedIndex: 	store.diary.selectedIndex,
	mapData: 		store.details.detailsData,
	screenState: 	store.newStop.screenState,
	stop: 			store.newStop.stop,
	travelBefore:	store.newStop.travelBefore,
	travelAfter:	store.newStop.travelAfter,
});

mapDispatchToProps = {
	addNewStop,	
	nextScreenState,
	previousScreenState,
	updateStop,
	updateTravelAfter,
	updateTravelBefore
};

export default connect(mapStoreToProps, mapDispatchToProps)(Create);
							
							/* <View style={styles.selectorContainer}>
								<Text>Activity</Text>
								<Picker 
									style={styles.picker} 
									selectedValue={this.state.activityValue}
									onValueChange={this.onActivitySelect}
								>
									{activities.map(({name, icon, value}) => 
										<Picker.Item label={name} value={value} key={value}/>
									)}
								</Picker>
							</View>
							<View style={styles.selectorContainer}>
								<View style={styles.displayDate}>
									<View style={styles.timeLabelSmall}> 
										<Text>Start</Text>
									</View>
									<View style={styles.timeLabelLarge}> 
										<Text style={styles.centerText}>{moment(this.state.startTimeValue).format('H:mm A')}</Text>
									</View>
								</View>
								<Slider 
									style={styles.slider}
									maximumValue={this.state.sliderMax}
									value={this.state.startSliderValue}
									step={1}
									onValueChange={this.onStartSliderChange}	
									maximumTrackTintColor={variables.brandPrimary}
									minimumTrackTintColor={variables.brandPrimary}
									thumbTintColor={variables.brandDark}
								></Slider>
							</View>
							<View style={styles.selectorContainer}>
								<View style={styles.displayDate}>
									<View style={styles.timeLabelSmall}> 
										<Text>End</Text>
									</View>
									<View style={styles.timeLabelLarge}> 
										<Text style={styles.centerText}>{moment(this.state.endTimeValue).format('H:mm A')}</Text>
									</View>
								</View>
								<Slider 
									style={[styles.slider, styles.sliderReversed]}
									maximumValue={this.state.sliderMax}
									value={this.state.endSliderValue}
									step={1}
									onValueChange={this.onEndSliderChange}	
									maximumTrackTintColor={variables.brandPrimary}
									minimumTrackTintColor={variables.brandPrimary}
									thumbTintColor={variables.brandDark}
									inverted={true}
								></Slider>
							</View> */