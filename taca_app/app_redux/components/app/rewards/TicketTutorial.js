/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
import { Container, Content, View, Text, Button } from 'native-base';
import NavHeader from '../../general/navHeader';
import variables from '../../../../native-base-theme/variables/platform';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

class TicketTutorial extends React.Component {
	constructor(props){
		super(props);
	}

	componentDidMount(){
	}

	_goBack = () => {
		// this.props.invalidateDetailsStore();
		this.props.navigation.goBack(null);
	}

	render(){
		return (
			<Container>
				<NavHeader 
					stackHeader
					customBackAction={this.onBackButtonPress}
				>
					Learn
				</NavHeader>
				<Content>
					<View>
						<Text>Test</Text>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
});

export default TicketTutorial;