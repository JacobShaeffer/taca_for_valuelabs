/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View, Text, Button, Icon } from 'native-base';
import NavHeader from '../../general/navHeader';
import { database, https } from '../../../lib/server';
import variables from '../../../../native-base-theme/variables/platform';
import SpinnerModal from '../../general/modal_busySpinner';
import Modal from '../../general/Modal';
import { routeNames } from '../../../config/routes';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const statusEnum = {
	working: 'working',
	tickets: 'not enough tickets',
	success: 'tickets successfully spent',
	pending: 'pending',
	exists: (val) => {
		switch(val){
			case statusEnum.working: 
			case statusEnum.pending: 
			case statusEnum.tickets: 
			case statusEnum.success: 
				return true;
			default: 
				return false;
		}
	}
}

class RaffleDetails extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			data: {
				cost: null,
				description: null,
			},
			status: statusEnum.pending,
		};
	}

	componentDidMount(){
		database.doGetItemDetails(this.props.selectedRaffle).then((data) => {
			this.setState({
				data: data,
			});
		});
	}

	_goBack = () => {
		this.props.navigation.goBack(null);
	}

	_openTicketTutorial = () => {
		this._navigate(routeNames.TicketTutorial);
	}

	_navigate = (route) => {
		const pushAction = StackActions.push({
			routeName: route,
		});
		this.props.navigation.dispatch(pushAction);
	}

	_useTickets = () => {
		this.setState({status: statusEnum.working}, () => {
			if(this.props.tickets < this.state.data.cost){
				this.setState({status: statusEnum.tickets});
			}
			else{
				https.onTicketsUsed({itemID: this.props.selectedRaffle}).then((val) => {
					let status;
					if(val.data.status === 'success'){
						status = statusEnum.success;
					}
					else{
						status = val.data.message;
					}
					this.setState({status});
				})
				.catch((error) => {
					this.setState({status: error});
				});
			}
		});
	}

	_renderPopup = () => {
		let { status } = this.state;
		let renderable = null;

		if(status === statusEnum.working){
			renderable = (
				<SpinnerModal isVisible={true}>Working on it</SpinnerModal>
			);
		}
		else if(status === statusEnum.tickets){
			renderable = (
				<Modal isVisible={true} containerStyle={{width: '80%', marginLeft: '10%', paddingVertical: 35}} onBackgroundPressed={() => {this.setState({status: statusEnum.pending})}}>
					<View style={styles.centeredView}>
						<Icon style={styles.warningIcon} name='alert-outline' type='MaterialCommunityIcons'/>
						<Text>Not enough tickets!</Text>
					</View>
					<View style={styles.centeredView}>
						<Text style={styles.centerText}>You unfortunatly do not have enough tickets to enter this raffle.</Text>
						<Button style={styles.centerButton} onPress={this._openTicketTutorial} transparent block>
							<Text>Learn how to earn more tickets!</Text>
						</Button>
						<Button style={styles.centerButton} onPress={() => {this.setState({status: statusEnum.pending})}}>
							<Text>Continue</Text>
						</Button>
					</View>
				</Modal>
			);
		}
		else if(status === statusEnum.success){
			renderable = (
				<Modal isVisible={true} containerStyle={{width: '60%', marginLeft: '20%', paddingTop: 30, paddingBottom: 35}} onBackgroundPressed={() => {this.setState({status: statusEnum.pending})}}>
					<View style={[styles.centeredView, styles.successView]}>
						<Icon style={styles.successIcon} name='check-all' type='MaterialCommunityIcons'/>
						<Text>Success!</Text>
					</View>
					<View style={styles.centeredView}>
						<Text style={styles.centerText}>You have been successfully entered into this raffle!</Text>
						<Button style={[styles.centerButton, styles.marginTop]} onPress={() => {this.setState({status: statusEnum.pending})}}>
							<Text>Continue</Text>
						</Button>
					</View>
				</Modal>
			)
		}
		else{
			renderable = (
				<Modal isVisible={true} containerStyle={{width: '66%', marginLeft: '17%', paddingVertical: 35}} onBackgroundPressed={() => {this.setState({status: statusEnum.pending})}}>
					<View style={styles.centeredView}>
						<Icon style={styles.errorIcon} name='alert' type='MaterialCommunityIcons'/>
						<Text>An unknown error occured.</Text>
					</View>
					<View style={styles.centeredView}>
						<Text style={styles.centerText}>Please, try again later.</Text>
						<Button style={[styles.centerButton, styles.marginTop]} onPress={() => {this.setState({status: statusEnum.pending})}}>
							<Text>Continue</Text>
						</Button>
					</View>
				</Modal>
			);
		}

		return renderable;
	}

	render(){
		const { raffleData, selectedRaffle } = this.props;
		const data = {
			...this.state.data,
			...raffleData[selectedRaffle]
		}

		return (
			<>
				<NavHeader 
					stackHeader
					customBackAction={this.onBackButtonPress}
				>
					Raffle
				</NavHeader>
					<Container>
						<Content>
							<View style={styles.imageContainer}>
								<Image 
									style={styles.image}
									source={{uri: data.uri}}
									resizeMode='contain'
								/>	
							</View>
							<View style={styles.descriptionBox}>
								<Text>{data.description}</Text>
								<Text>Retail Value: ${data.worth}</Text>
							</View>
							<View>
								<Text>Tickets to enter: {data.cost}</Text>
							</View>
						</Content>
						<View style={styles.bottomButton}>
							<Button block onPress={this._useTickets} disabled={!data.cost}>
								<Text uppercase={false}>Enter Raffle</Text>
							</Button>
						</View>
						{this.state.status === statusEnum.pending ? null : this._renderPopup()}
					</Container>
			</>
		);
	}
}

const styles = StyleSheet.create({
	centeredView: {
		justifyContent: 'center',
		alignContent: 'center',
		alignItems: 'center',
	},
	centerButton: {
		alignSelf: 'center',
	},
	centerText: {
		textAlign: 'center',
	},
	successIcon: {
		fontSize: 45,
		color: variables.brandSuccess
	},
	warningIcon: {
		fontSize: 45,
		color: variables.brandWarning,
	},
	errorIcon: {
		fontSize: 45,
		color: variables.brandDanger,
	},
	successView:{
		marginBottom: 5,
	},
	imageContainer: {
		height: DEVICE_HEIGHT/3,
		width: DEVICE_WIDTH - 10,
		margin: 5,
	},
	image: {
		height: undefined,
		width: undefined,
		flex: 1,
	},
	bottomButton: {
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
	},
	descriptionBox: {
		backgroundColor: variables.brandLight,
	},
	marginTop: {
		marginTop: 10,
	}
});

const mapStoreToProps = (store) => ({
	selectedRaffle: store.rewards.selectedRaffle,
	tickets: store.rewards.tickets,
	progress: store.rewards.progress,
	raffleData: store.rewards.raffleData,
});

const mapDispatchToProps = {

};

export default connect(mapStoreToProps, mapDispatchToProps)(RaffleDetails);