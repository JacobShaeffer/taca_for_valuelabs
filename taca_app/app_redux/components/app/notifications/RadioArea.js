/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import {
    StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import {
    View,
    ListItem,
    Text,
    Radio,
    Right,
    Left,
} from 'native-base';
import InputAreaContainer from './InputAreaContainer';

class RadioArea  extends Component{
    constructor(props){
		super(props);

        this.state = {
			value: props.body.questionBody.initialValue,
		};
    }

    _radioPress = (item) => {
		this.setState({value: item});
		this.props.update(item);
    }

    render(){
        const { 
			value,
		} = this.state;
		const {
			labels,
		} = this.props.body.questionBody;

        return(
			<InputAreaContainer body={this.props.body.questionText}>
				{labels.map((item, index) => (
						<View key={index+""}>
							<ListItem
								selected = {item === value}
								onPress = {() => this._radioPress(item)}
							>
								<Left>
									<Text>
										{item}
									</Text>
								</Left>
								<Right>    
									<Radio
										selected={item === value}
										onPress={() => this._radioPress(item)}
									/>
								</Right>
							</ListItem>   
						</View>   
					)
				)}    
			</InputAreaContainer>
        );
    }  
};

const mapStoreToProps = (store) => ({
	selectedNotification: store.notifications.selectedNotification,
	notificationData: store.notifications.notificationData,
});

const mapDispatchToProps = {
};

export default connect(mapStoreToProps, mapDispatchToProps)(RadioArea);

const styles = StyleSheet.create({
});