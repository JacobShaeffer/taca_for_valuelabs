/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import { StyleSheet, Button, ScrollView } from 'react-native';
import { https, firestore } from '../../../lib/server';
import { connect } from 'react-redux';
import { Container, View, Text, H1 } from 'native-base';
import NavHeader from '../../general/navHeader';
import SliderArea from './SliderArea';
import CheckArea from './CheckArea';
import TextArea from './TextArea';
import RadioArea from './RadioArea';
import { questionSubmit, setLoading } from '../../../redux/modules/notifications';
import SpinnerModal from '../../general/modal_busySpinner';
import variables from '../../../../native-base-theme/variables/platform';


class SpecificNotification extends Component{
	constructor(props){
		super(props);

		const {
			notificationData,
			selectedNotification,
		} = this.props;

		const nData = notificationData[selectedNotification];
		if(nData.type === 'question'){
			let arr = [];
			nData.body.forEach((qbody) => {
				arr.push(qbody.questionDefaultAnswer || null);	
			});

			this.state = {
				answers: arr,
				isLoading: true,
				isQuestion: true,
			}
		}
		else{
			this.state = {
				isQuestion: false,
			};
		}

		this._questionUpdate = this._questionUpdate.bind(this);
		this._getAppropriateInputArea = this._getAppropriateInputArea.bind(this);
	}

	_submit = async () => {
		const { isQuestion, answers } = this.state;

		if(isQuestion){
			this.props.setLoading(true);
			const {
				notificationData,
				selectedNotification,
				user,
			} = this.props;
			const nData = notificationData[selectedNotification];
			const nid = nData.notifID;
			const uid = user.uid;

			try {
				// let result = await https.onQuestionAnswered({nid, answers});
				// TODO: maybe: set a timeout before the firestore call and have the timeout be canceled after the await finishes
				// this timeout will tell the user that there may be network problems and they could try again later.
				await firestore.doCreateAnswerDocument(uid, nid, answers);
			}
			catch(error){
				console.log('there was an error answering a question: ', error);
			}
			finally{
				this.props.setLoading(false);
				this.props.questionSubmit(nid);
				this.props.navigation.goBack();
			}
		}
		else{
			this.props.setLoading(true);
			// TODO:
			// remove the nid from redux store,
			// make call to server (do not await the server call);
			this.props.setLoading(false);
			this.props.navigation.goBack();
		}
	}

	_questionUpdate = (value, id) => {
		let answers = this.state.answers;
		answers[id] = value;
		this.setState({answers}, () => {console.log('state set: ', this.state.answers)});
	}

	_getAppropriateInputArea = (question, idx) => {
		if(question.parent !== undefined){
			let shouldShow = eval(`(function (value) { ${question.shouldShow}})(${this.state.answers[question.parent]})`);
			if(!shouldShow){
				return null;
			}
		}
		let localUpdate = (value) => {
			this._questionUpdate(value, idx);
		};
		switch(question.questionType){
			case 'text': 
				return <TextArea body={question} update={localUpdate} key={idx}/>
			case 'slider':
				return <SliderArea body={question} update={localUpdate} key={idx}/>
			case 'checkbox':
				return <CheckArea body={question} update={localUpdate} key={idx}/>
			case 'radio':
				return <RadioArea body={question} update={localUpdate} key={idx}/>
			default:
				// TODO: this in an error if this happens
				// deal with it
				// TODO: should probably do data validity checks here so we don't have to do them in every InputArea
				return null;
		}
	}

	render(){
		const {
			notificationData,
			selectedNotification,
		} = this.props;

		const nData = notificationData[selectedNotification];

		if(!nData) return null;

		return (
			<Container>
				{/* TODO: make use of this.props.loading for when the question is being submitted */}
				<NavHeader stackHeader>
					Notification
				</NavHeader>
				<View style={styles.MessageView}>
					<H1>{nData.title}</H1>
					{nData.type === 'question' ?  
						<ScrollView>
							{nData.body.map((question, idx) => 
								<View key={'holder-'+idx}>
									{this._getAppropriateInputArea(question, idx)}
									{idx <= nData.body.length ? (
											<View key={'spacer-'+idx} style={styles.spacer}></View>
										) : (
											null
										)
									}
								</View>
							)}
						</ScrollView>
					: 
						<Text>{nData.message}</Text>
					}
				</View>
				<View style={styles.SubmitButtonView}>
					{/* TODO: make this button act differently on messages versus questions */}
					{nData.type === 'question' ?
						<Button 
							title={'Submit'} 
							onPress={this._submit} 
							disabled={this.props.isLoading}
							color={variables.brandPrimary}		
						/>
					:
						<>
							<Button 
								title={'Close'} 
								onPress={() => {}} 
								color={variables.brandPrimary}		
							/>
							<View style={styles.ButtonPadding}></View>
							<Button 
								title={'Dismiss'} 
								onPress={() => {}} 
								color={variables.brandWarning}		
							/>
						</>
					}
				</View>
				<SpinnerModal isVisible={this.props.isLoading}></SpinnerModal>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	ButtonPadding: {
		margin: 3,
	},
	SubmitButtonView: {
		margin: 10,
		justifyContent: 'space-between',
	},
	MessageView: {
		flex: 1,
		margin: 10,
	},
	spacer: {
		height: 10,
	}
});

const mapStoreToProps = (store) => ({
    user: 					store.auth.user,
	selectedNotification: 	store.notifications.selectedNotification,
	notificationData: 		store.notifications.notificationData,
	isLoading: 				store.notifications.loading
});

const mapDispatchToProps = {
	questionSubmit,
	setLoading,
};

export default connect(mapStoreToProps, mapDispatchToProps)(SpecificNotification);