/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import {
    StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import {
    View,
    ListItem,
    Text,
    CheckBox,
    Body,
} from 'native-base';
import variables from '../../../../native-base-theme/variables/platform';
import InputAreaContainer from './InputAreaContainer';

export class CheckArea extends Component {
    constructor(props){
		super(props);

        this.state = {
			values: this.props.body.questionBody.labels,
		};
    }

    _toggleCheckBox = (idx) => {
        let values = this.state.values;
        values[idx].value = !values[idx].value;
		this.setState({values});
		this.props.update({values});
    }

    render(){
		const {
			values,
		} = this.state;

        return(
			<InputAreaContainer body={this.props.body.questionText}>
				{values.map((e, idx) => 
					<View key={idx}>
						<ListItem onPress = {() => this._toggleCheckBox(idx)}>
							<CheckBox
								checked = {e.value}
								onPress = {() => this._toggleCheckBox(idx)}
								color = {variables.brandPrimary}
							/>
							<Body>
								<Text>
									{e.key}
								</Text>
							</Body>
						</ListItem>
					</View>
				)}
			</InputAreaContainer>
        );
    }
};

const mapStoreToProps = (store) => ({
	selectedNotification: store.notifications.selectedNotification,
	notificationData: store.notifications.notificationData,
});

const mapDispatchToProps = {
};

export default connect(mapStoreToProps, mapDispatchToProps)(CheckArea);

const styles = StyleSheet.create({
});