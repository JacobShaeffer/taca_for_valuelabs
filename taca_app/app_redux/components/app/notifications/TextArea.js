/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import {
	StyleSheet,
	TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import {
    Item,
	Label,
	Input,
} from 'native-base';
import variables from '../../../../native-base-theme/variables/platform';
import InputAreaContainer from './InputAreaContainer';

class TextArea extends Component {
    constructor(props){
		super(props);

        this.state = {
			isError: false,
			error: 0,
			value: '',
		};
    }

    validate = (text) => {
        let func;
        try{
			if(this.props.body.questionBody){
            	func = new Function('value', this.props.body.questionBody.validation)(text);
			}
			else{
				func = () => true;
			}
        }
        catch(error){
            console.error('- an error occured when trying to create validation function: ', error);
            func = undefined;
        }
        return func;
    }

    onTextFinish = (event) => {
        let text = event.nativeEvent.text;
        let err;
        try{
            err = this.validate(text);
        }
        catch(error){
            console.error('- an error occured when trying to validate user input: ', error);
        }
        if(typeof err === 'number'){
            this.setState({isError: true, error: err, value: text});
        }
        else{
			this.setState({value: text});
			this.props.update(text);
        }
    }

    render(){
		let questionBody = this.props.body.questionBody || {};
		const { placeholder, errorMessages } = questionBody;
		const { isError, error, value } = this.state;
        return (
			<InputAreaContainer body={this.props.body.questionText}>
				<Item style={styles.Item} last floatingLabel={isError} error={isError}>
					{isError ? <Label style={{color: variables.brandDanger}}>{(errorMessages && errorMessages[error]) || 'error'}</Label> : null}
					<Input 
						value={value}
						placeholder={placeholder || 'Answer'}
						onChangeText={(text) => {
							this.setState({isError: false, value: text}); 
							this.props.update(text);
						}}
						onEndEditing={this.onTextFinish}
						// TODO: verify the text before allowing submission
					/> 
				</Item>
			</InputAreaContainer>
        );
    }
};

const mapStoreToProps = (store) => ({
	selectedNotification: store.notifications.selectedNotification,
	notificationData: store.notifications.notificationData,
});

const mapDispatchToProps = {
};

export default connect(mapStoreToProps, mapDispatchToProps)(TextArea);

const styles = StyleSheet.create({
	Item: {
		marginTop: 5,
	}
});