/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
} from 'react-native';
import { connect } from 'react-redux';
import Slider from 'react-native-slider';
import {
    View,
} from 'native-base';
import variables from '../../../../native-base-theme/variables/platform';
import InputAreaContainer from './InputAreaContainer';

class SliderArea extends Component{
    constructor(props){
		super(props);

        const {
			value,
			step,
			min,
			max,
        } = props.body.questionBody;

		const defaultStep = step || 1;
		const defaultMin = min || 0;
		const defaultMax = max || 100;
        const defaultValue = value !== undefined ? value : defaultStep % 1 === 0 ? Math.ceil((defaultMax - defaultMin)/2) : ((defaultMax - defaultMin)/2);

        this.state = {
			value: defaultValue,
		};
    }

    render(){

		const {
			max,
			min,
			step
		} = this.props.body.questionBody;
		const {
			value,
		} = this.state;

		return (
			<InputAreaContainer body={this.props.body.questionText}>
				<View>
					<View>
						<Text style={styles.SliderValue}>{value || '???'}</Text>
					</View>
					<View>
						<Slider
							maximumValue={max || 100}
							minimumValue={min || 0}
							value={value}
							step={step | 1}
							minimumTrackTintColor={variables.brandPrimary}
							maximumTrackTintColor={variables.brandLight}
							// trackStyle={styles.SliderTrack}
							thumbStyle={styles.SliderThumb}
							onValueChange={(value) => {this.setState({value: value})}}
							onSlidingComplete={(value) => {this.setState({value: value}); this.props.update(value)}}
						/>
					</View>
				</View>
			</InputAreaContainer>
		);
	}
}

const styles = StyleSheet.create({
    SliderThumb: {
        backgroundColor: variables.brandPrimary,
        borderColor: variables.brandPrimary,
    },
    SliderValue: {
		textAlign: 'center',
	},
});

const mapStoreToProps = (store) => ({
	selectedNotification: store.notifications.selectedNotification,
	notificationData: store.notifications.notificationData,
});

const mapDispatchToProps = {
};

export default connect(mapStoreToProps, mapDispatchToProps)(SliderArea);