/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import {
	StyleSheet,
} from 'react-native';
import {
    View,
    Text,
} from 'native-base';

const InputAreaContainer = ({children, body}) => {
	return (
		<View style={styles.Container}>
			<Text>{body}</Text>
			<View>
				{children}
			</View>
		</View>
	);
}

export default InputAreaContainer;

const styles = StyleSheet.create({
    Container: {
		marginVertical: 5,
		marginRight: 10,
		marginLeft: 10,
		paddingBottom: 5,
	},
});