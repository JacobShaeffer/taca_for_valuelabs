/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {
    Header,
    Body,
    Icon,
    Title,
    Left,
	Button,
	View,
	Text,
} from 'native-base';
import { withNavigation } from 'react-navigation';

const NavHeader = (props) => {

	const onButtonPress = () => {
		if(typeof props.customBackAction === 'function'){
			if(props.customBackAction()){
				// if customBAckAction returns true then the back action was taken care of
				// so we do nothing
				return;
			}
		}

		if(props.stackHeader){
			props.navigation.goBack(null);
		}
		else{
			props.navigation.openDrawer();
		}
	}

	return (
		<Header>
			<Left>
				<Button transparent onPress={onButtonPress} badge vertical>
					{
						props.stackHeader ?
							<Icon name='backburger' type='MaterialCommunityIcons'/>
						:
							<>
								<Icon name='menu' type='MaterialCommunityIcons'/>
								{/* TODO: notification counter */}
								{props.notificationCount ? 
									<View style={styles.badge}><Text style={styles.badgeText}>{props.notificationCount}</Text></View>
								: 
									null
								}
							</>
					}
				</Button>
			</Left>
			<Body>
				<Title>
					{props.children}
				</Title>
			</Body>
		</Header>
	);
}

const styles = StyleSheet.create({
	badgeText: {
		fontSize: 10,
		textAlign: 'center',
		color: 'white',
		fontWeight: 'bold',
	},
	badge: {
		position: 'absolute',
		backgroundColor: 'red',
		borderRadius: 10,
		left: 20,
		justifyContent: 'center',
		width: 20,
		height: 20,
	}
});

// export default withNavigation(NavHeader);

const mapStoreToProps = (store) => ({
	notificationCount: store.notifications.notificationCount,
});

export default connect(mapStoreToProps)(withNavigation(NavHeader));