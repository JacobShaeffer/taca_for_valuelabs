/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import {
	StyleSheet,
	TouchableWithoutFeedback,
} from 'react-native';
import { View } from 'native-base';

const modal = ({children, isVisible, containerStyle, onBackgroundPressed}) => {
	return (
		isVisible ?
			<TouchableWithoutFeedback onPress={onBackgroundPressed}>
				<View style={styles.background}>
					<View style={[styles.container, containerStyle]}>
						{children}
					</View>
				</View>
			</TouchableWithoutFeedback>
		: null
	)
}

const styles = StyleSheet.create({
	background: {
		...StyleSheet.absoluteFill,
		backgroundColor: '#00000088',
        margin: 0,
        justifyContent: 'center',
        alignContent: 'center',
	},
    container: {
        backgroundColor: 'white', 
        borderRadius: 7, 
        opacity: 1, 
        paddingLeft: 5, 
        paddingRight: 5,
        alignContent: 'center', 
        justifyContent: 'center'
    },
});

export default modal;