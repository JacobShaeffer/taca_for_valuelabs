/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import { InteractionManager } from 'react-native';
import Modal from 'react-native-modal';
import {
    View, Text, H1, Content,
} from 'native-base';
import InputListItem from './InputListItem';
import PropTypes from 'prop-types';

class SurveyModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            finished: false,
            updated: false,
            answers: {},
        }
    }

    componentDidMount(){
        InteractionManager.runAfterInteractions(() => {
            this.setState({
                finished: true,
            });
        });
    }

    _updateAnswer = (key, value) => {
        let updatedAnswers = this.state.answers;
        updatedAnswers[key] = value;
        this.setState({ answers: updatedAnswers });
    }

    _closeModal = () => {
        this.props.onBack(Object.assign({}, this.state.answers));
        this.setState({answers: {}});
    }

    render() {
        const {
            isVisible,
            questionData,
            triggerTitle,
        } = this.props;
        if(this.state.finished){
            return (
                <Modal 
                    isVisible={isVisible} 
                    onBackButtonPress={this._closeModal} 
                    onBackdropPress={this._closeModal}
                    hideOnBack={false}
                >
                    <View style={{backgroundColor: 'white', borderRadius: 7, flex: 1, paddingVertical: 10, paddingHorizontal: 15}}>
                        <Content>
                            <View style={{justifyContent: 'center'}}><H1 style={{textAlign: 'center'}}>{triggerTitle}</H1></View>
                            <InputListItem questions={questionData} onBack={this._closeModal} updateAnswers={this._updateAnswer}/>
                        </Content>
                    </View>
                </Modal>
            )
        }
        else{
            return null;
        }
    }
}

SurveyModal.propTypes = { 
    isVisible:      PropTypes.bool.isRequired, 
    questionData:   PropTypes.array, 
    triggerTitle:   PropTypes.string,
    onBack:         PropTypes.func.isRequired,
}

export default SurveyModal;