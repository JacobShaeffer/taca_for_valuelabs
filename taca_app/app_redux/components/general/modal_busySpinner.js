/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import {
    StyleSheet,
} from 'react-native';
import { View, Text, Spinner } from 'native-base';
import Modal from './Modal';

const modal = ({children, isVisible, onBackgroundPressed}) => {
	return (
		<Modal isVisible={isVisible} containerStyle={{width: '66%', marginLeft: '17%', flexDirection: 'row'}} onBackgroundPressed={onBackgroundPressed}>
			<View style={styles.spinner}>
				<Spinner/>
			</View>
			<View style={styles.text_container}> 
				<Text style={styles.text}>{children}</Text>
			</View> 
		</Modal>
	)
}

const styles = StyleSheet.create({
    spinner: {
        marginLeft: 10, 
        justifyContent: 'center', 
        alignContent: 'center',
    },
    text: {
        textAlign: 'left',
        fontWeight: 'bold', 
        fontSize: 24, 
    },
    text_container: {
        marginLeft: 10, 
        justifyContent: 'center', 
    }
});

export default modal;