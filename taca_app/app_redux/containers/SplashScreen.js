/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, {Component} from 'react';
import {
    View,
    Text,
} from 'react-native';
import SS from 'react-native-splash-screen';
import { connect } from 'react-redux';
import { setUser } from '../redux/modules/auth';
import { auth } from '../lib/server';

class SplashScreen extends Component {
	constructor(props){
        super(props);
	}

	componentDidMount(){
        this.unsubscribeFromAuthStateListener = auth.onAuthStateChanged((user) => {
            let navigationTarget;

			if(user){
                this.props.setUser(user);
                navigationTarget = 'App';
            }
            else{
                navigationTarget = 'Auth';
            }

            return this.props.navigation.navigate(navigationTarget);
		})
    }

    componentWillUnmount(){
        this.unsubscribeFromAuthStateListener();
        SS.hide();
    }
    
	render() {
		return (
            <View>
                <Text>Loading...</Text>
            </View>
		);
	}
}

const mapDispatchToProps = (dispatch) => ({
    setUser: (user) => dispatch(setUser(user)),
});

export default connect(null, mapDispatchToProps)(SplashScreen);