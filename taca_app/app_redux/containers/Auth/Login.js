/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

// This is the login screen
// it allows the user to input their email address and passwrd to loginto the app as well as offers a "forgot password" option to let users reset their password
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { auth } from '../../lib/server';
import { setUser } from '../../redux/modules/auth';
import {
	StyleSheet,
	ImageBackground,
} from 'react-native';
import {
    Container,
    Content,
    Text,
    Body,
	Button,
	Form,
	Item,
	Input,
	Label,
	View,
	Header,
	Icon,
} from 'native-base';
import variables from '../../../native-base-theme/variables/platform';
import SpinnerModal from '../../components/general/modal_busySpinner';
import { NavigationActions, StackActions } from 'react-navigation';
import { routeNames } from '../../config/routes';
import { authStyles } from '../../config/styles';

class Login extends PureComponent {
	constructor(){
		super();

		this.state = {
			isLoginModalVisible: false,

			email: '',
			password: '',
			isEmailError: false,
			isPasswordError: false,
			isGeneralError: false,
			emailErrorMessage: '',
			passwordErrorMessage: '',
			generalErrorMessage: '',
		}

	}
	
    _attemptLogin = (email, password) => {
        //DEBUG:
        email = 'ta-mar1819@test.com';
        password = '123456';

        this.setState({isLoggingIn: true}, () => {
            auth.doLoginWithEmailAndPassword(email, password).then((userCreds) => {
            }).catch((error) => {
                console.error('There was an error loggin in the user: ', error);
            });
        });
    }

	/**
	 * simple regex test to verify this.state.email has 
	 * .+ 	one or more of any character
	 * @ 	an at symbol
	 * .+ 	one or more of any character
	 * \.	a period
	 * .+	one or more of any character
	 * in that order
	 */
	_isEmailInvalid = () => {
		return (/.+@.+\..+/i).test(this.state.email.toLowerCase()) === false;
	}

	_verifyEmail = () => {
		if(this._isEmailInvalid()){
			this.setState({isEmailError: true, emailErrorMessage: 'invalid email adress'});
		}
    }
    
    /**
     * verifies data of login input fields
     */
    _verifySubmittedData = () => {
		var hasProblemOccured = false;
        
		if(this._isEmailInvalid() || this.state.password.length == 0){
			hasProblemOccured = true;
			this._verifyEmail();
			this.setState({passwordErrorMessage: 'This field cannot be left blank.', isPasswordError: true, isLoginModalVisible: false});
		}
		if(!this.state.email || this.state.email.length == 0){
			hasProblemOccured = true;
			this.setState({emailErrorMessage: 'This field cannot be left blank.', isEmailError: true, isLoginModalVisible: false});
        }
       	if(hasProblemOccured || !this._shouldAllowLogin()){
			return;
		}
		else{
			this._passwordLogin();
		}
    }

    /**
     * runs on user login failure
     */
    _loginFailure = (error) => {
        let errorCode = error.code;
        let errorMessage = error.message;
        switch(errorCode){
            case 'auth/invalid-email':
            case 'auth/wrong-password':
            case 'auth/user-not-found':
                this.setState({generalErrorMessage: 'Login failed; Invalid email or password', isGeneralError: true});
                break;
            case 'auth/user-disabled':
                this.setState({generalErrorMessage: 'This account has been disabled.', isGeneralError: true});
                break;
            case 'auth/unknown':
                if(errorMessage.startsWith('A network error')){
                    this.setState({generalErrorMessage: 'A network error has occured. \nMake sure you are connected to the internet or Try again later.', isGeneralError: true});
                    break;
                }
            default:
                this.setState({generalErrorMessage: 'An unknown error has occured.', isGeneralError: true});
        }
        this.setState({isLoginModalVisible: false});
    }

    /**
     * runs on successful user login
     */
    _loginSuccess = (userCreds) => {
        this.props.setUser(userCreds.user);
        return this.props.navigation.navigate(routeNames.App);
    }

    /**
     * Only alow the user to log in if there are no errors
     */
    _shouldAllowLogin = () => {
        const {
			isEmailError,
			isPasswordError,
        } = this.state;
        let shouldAllow = !(isEmailError || isPasswordError);
		return shouldAllow;
    }

	/**
	 * attempt to login to he firebase backend with the given credentials this.state.email, and this.state.password
	 */
	_passwordLogin = () => {
		this.setState({isLoginModalVisible: true}, () => {
			auth.doLoginWithEmailAndPassword(this.state.email, this.state.password)
			.then(this._loginSuccess)
			.catch(this._loginFailure);
        });
	}

	/**
	 * navigate page to signup
	 */
	_goToSignup = () => {
		const navigate = NavigationActions.navigate({
			routeName: routeNames.SignUp,
		});
		this.props.navigation.dispatch(navigate);
	}

	/**
	 * 
	 */
	_pushForgotPassword = () => {
		const push = StackActions.push({
			routeName: routeNames.ForgotPassword,
		});
		this.props.navigation.dispatch(push);
	}
	
	/**
	 * react-native function
	*/
	render(){
		const loginDisabled = !this._shouldAllowLogin();

		const {
			generalErrorMessage,
			isGeneralError,
			emailErrorMessage,
			isEmailError,
			passwordErrorMessage,
			isPasswordError,
			email,
			isLoginModalVisible,
		} = this.state;

		return(
			<Container>
				<Header style={{display: 'none'}}/>
				<ImageBackground source={require('../../resources/background.png')} style={styles.image}>
					<Content>
						<View style={styles.topPadding}/>
						<Body>
							<Text style={styles.warning_general}>{isGeneralError ? generalErrorMessage : ''}</Text>
						</Body>
						<View style={styles.form_background}>
							<Form>
								{isEmailError ? <Label style={styles.warning_specific}>{emailErrorMessage}</Label> : null}
								<Item error={isEmailError || isGeneralError ? true : false}>
									<Icon color={styles.icon.color} name='email' type='MaterialCommunityIcons'/>
									<Input 
										light
										placeholder='Email'
										onChangeText={(text) => this.setState({email: String(text), isEmailError: false, isGeneralError: false})}
										autoCapitalize='none'
										autoCompleteType='email'
										onEndEditing={this._verifyEmail}
										value={email}
										maxLength={128}
										textContentType="emailAddress"
									/>
								</Item>
								{isPasswordError ? <Label style={styles.warning_specific}>{passwordErrorMessage}</Label> : null}
								<Item error={isPasswordError || isGeneralError ? true : false}>
									<Icon color={styles.icon.color} name='key' type='MaterialCommunityIcons'/>
									<Input 
										style={styles.password}
										placeholder='Password' 
										autoCompleteType='password'
										autoCapitalize='none'
										secureTextEntry={true}
										onChangeText={(text) => this.setState({password: String(text), isPasswordError: false, isGeneralError: false})}
										maxLength={128}
										textContentType="password"
									/>
								</Item>	
								<View>
									<Button
										style={styles.forgotPassword_button}
										transparent
										dark
										onPress={this._pushForgotPassword}
									>
										<Text style={[styles.small_text, styles.link_text]} uppercase={false}>Forgot Password?</Text>	
									</Button>
								</View>
							</Form>
						</View>
						<Button 
							block 
							disabled={loginDisabled}
							style={styles.button_submit}
							onPress={this._verifySubmittedData}
						>
							<Text>Log In</Text>
						</Button>
						<View style={styles.nav_container}>
							<Text style={[styles.small_text, styles.nav_text]}>
								Don't have an account yet?
							</Text>
							<Button
								transparent
								small
								onPress={this._goToSignup}
							>
								<Text style={styles.nav_button_text} uppercase={false}>Sign Up</Text>
							</Button>
						</View>
					</Content>
					<SpinnerModal isVisible={isLoginModalVisible}>Loggin in...</SpinnerModal>
				</ImageBackground>
			</Container>
		);
	}
};

const styles = StyleSheet.create({
	...authStyles,
	forgotPassword_button: {
		alignSelf: 'flex-end',
	},
	icon: {
		color: variables.brandLight,
	}
});

const mapDispatchToProps = (dispatch) => ({
    setUser: (user) => dispatch(setUser(user)),
});

export default connect(null, mapDispatchToProps)(Login);