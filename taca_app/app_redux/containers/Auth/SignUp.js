/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

// this is the signup screen
// this allows new users to create accounts by providing an email and password 
// as well as answer a set of initial screening questions
// this process also requires the user to agree to the privacy and terms and conditions policies before creating their account
// a user may not use the app without an account
import React from 'react';
import {
    ImageBackground,
    StyleSheet,
} from 'react-native';
import {
	CheckBox,
    Container,
    Content,
    Text,
    Body,
	Button,
	Form,
	Input,
	Item,
	Label,
	View,
	Header,
	Icon,
} from 'native-base';
import { SafeAreaView, NavigationActions, StackActions } from 'react-navigation';
import { auth } from '../../lib/server';
import { connect } from 'react-redux';
import SpinnerModal from '../../components/general/modal_busySpinner';
import { routeNames } from '../../config/routes';
import { authStyles } from '../../config/styles';
import variables from '../../../native-base-theme/variables/platform';
import { setUser } from '../../redux/modules/auth';

const dictionary = { 
    signup: {
        title: 'Sign Up',
        Email: 'Email',
        Pass: 'Password',
        Vpass: 'Confirm Password',
        Login: 'Already have an account?',
        Submit: 'Sign Up',
        Loading: 'Loading...',
    }
};


class SignUp extends React.PureComponent {
	constructor(props){
		super(props);
		this.state = {
			isModalVisible: false,

			questionsLoaded: false,


			isEmailError: false,
			isPasswordError: false,
			isGeneralError: false,
			isvPassError: false,
			isSurveyError: false,
			isTermsError: false,
			
			termsAccepted: false,

			password: '',
			email: '',
			vPass: '',
		}

		this.contentRef = React.createRef();
	}
		
	_verifyEmail = () => {
		if(this._isEmailInvalid()){
			this.setState({isEmailError: true, emailErrorMessage: 'invalid email adress'});
			this.contentRef._root.scrollToPosition(0,0);
		}
	}

	_verifyPassword = () => {
		if(this._isPasswordInvalid()){
			this.setState({isPasswordError: true, passwordErrorMessage: 'password to short'});
			this.contentRef._root.scrollToPosition(0,0);
		}
		if(this.state.vPass.length != 0){
			this._verifyvPass();
		}
	}

	_verifyvPass = () => {
		if(this._isvPassInvalid()){
			this.setState({isvPassError: true, vPassErrorMessage: 'passwords do not match'});
			this.contentRef._root.scrollToPosition(0,0);
		}
	}

	/**
	 * simple regex test to verify this.state.email has 
	 * .+ 	one or more of any character
	 * @ 	an at symbol
	 * .+ 	one or more of any character
	 * \.	a period
	 * .+	one or more of any character
	 * in that order
	 */
	_isEmailInvalid = () => {
		return (/.+@.+\..+/i).test(this.state.email.toLowerCase()) === false;
	}

	/**
	 * verifies this.state.password has atleast 6 characters and if this.state.vPass is typed, they are identical
	 */
	_isPasswordInvalid = () => {
		return this.state.password.length < 6;
	}

	/**
	 * verifies this.state.password and this.state.vPass are identical 
	 */
	_isvPassInvalid = () => {
		return this.state.password !== this.state.vPass;
	}

	/**
	 * 
	 */
	_verifySubmittedData = () => {
		var hasProblemOccured = false;

		if(this._isEmailInvalid() || this._isPasswordInvalid() || this._isvPassInvalid()){
			this._verifyEmail();
			this._verifyPassword();
			this._verifyvPass();
			hasProblemOccured = true;
		}
		if(this.state.password.length === 0 || this.state.email.length === 0 || this.state.vPass.length === 0){
			this.setState({isGeneralError: true, generalErrorMessage: 'All fields must be filled in.'});
			hasProblemOccured = true;
		}
		if(!this.state.termsAccepted){
			this.setState({isTermsError: true, termsErrorMessage: 'You must accept the Terms & Conditions.'});
			hasProblemOccured = true;
		}
		if(hasProblemOccured){
			return;
		}
		else{
			this._attemptSignUp();
		}
	}

	/**
	 * performs one last round of form validation then attemps to sign the user up with the given information
	 */
	_attemptSignUp = () => {
		this.setState({isModalVisible: true, isGeneralError: false}, () => {
			auth.doCreateUserWithEmailAndPassword(this.state.email, this.state.password)
			.then(this._signupSuccess)
			.catch(this._signupFailure);
		});
	}

    /**
     * runs on user signup failure
     */
    _signupFailure = (error) => {
		console.log('!!- firebase create user error', error);
		switch(error.code){
			case 'auth/email-already-in-use':
				this.setState({generalErrorMessage: 'There is already an account under that email address.', isGeneralError: true});
				break;
			case 'auth/invalid-email':
				this.setState({generalErrorMessage: 'The email address given is invalid. \n Check for typos or Pick a different email address.', isGeneralError: true});
				break;
			case 'weak-password':
				this.setState({generalErrorMessage: 'Chosen password is too weak. Passwords need to be at least 6 characters.', isGeneralError: true});
				break;
			default:
				this.setState({generalErrorMessage: 'An unknown error has occured.', isGeneralError: true});
				break;
		}
		this.setState({isModalVisible: false});
    }

    /**
     * runs on successful user signup 
     */
    _signupSuccess = (userCreds) => {
        this.props.setUser(userCreds.user);
        return this.props.navigation.navigate(routeNames.App);
    }

	/**
	 * navigate page to login
	 */
	_goToLogin = () => {
		const navigate = NavigationActions.navigate({
			routeName: routeNames.Login,
		});
		this.props.navigation.dispatch(navigate);
	}

	/**
	 * push Information screen onto top of navigation stack
	 */
	_navigateToInformation = () => {
		const push = StackActions.push({
			routeName: routeNames.Information,
		})
		this.props.navigation.dispatch(push);
	}
	
	/**
	 * react-native function
	*/
	render(){
		return(
			<Container>
				<Header style={{display: 'none'}}/>
				<ImageBackground source={require('../../resources/background.png')} style={styles.image}>
				{/* TODO: Need to update this so Content will fill at least the height of the screen, the loading screen currently does not cover the whole screen*/}
				{/* FIXME: hitting signup without checking the box will sign the user up without logging them in, preventing them from signing up with that email*/}
					<Content ref={(ref) => this.contentRef = ref}>
						<View style={styles.topPadding}/>
						<Body>
							{this.state.isGeneralError ? 
								<Text style={styles.warning_general}>{this.state.generalErrorMessage}</Text>
							:
								null
							}
						</Body>
						<View style={styles.form_background}>
						<Form>
							{this.state.isEmailError ? <Label style={styles.warning_specific}>{this.state.emailErrorMessage}</Label> : null}
							<Item error={this.state.isEmailError || this.state.isGeneralError ? true : false}>
								<Icon color={styles.icon.color} name='email' type='MaterialCommunityIcons'/>
								<Input 
									placeholder={dictionary.signup.Email}
									autoCapitalize='none'
									autoCompleteType='email'
									onChangeText={(text) => this.setState({email: String(text), isEmailError: false})}
									onEndEditing={() => {this._verifyEmail()}}
									maxLength={128}
								/>
							</Item>
							{this.state.isvPassError || this.state.isPasswordError ?
								<Label style={styles.warning_specific}>
									{this.state.isvPassError ? this.state.vPassErrorMessage : this.state.passwordErrorMessage}
								</Label> 
							: null}
							<Item error={this.state.isPasswordError || this.state.isGeneralError ? true : false}>
								<Icon color={styles.icon.color} name='key' type='MaterialCommunityIcons'/>
								<Input 
									placeholder={dictionary.signup.Pass}
									autoCapitalize='none'
									style={styles.password}
									secureTextEntry
									onChangeText={(text) => this.setState({password: String(text), isPasswordError: false, isvPassError: false})}
									onEndEditing={() => {this._verifyPassword()}}
									maxLength={128}
								/>
							</Item>	
							{this.state.isvPassError ? <Label style={styles.warning_specific}>{this.state.vPassErrorMessage}</Label> : null}
							<Item error={this.state.isPasswordError || this.state.isGeneralError ? true : false}>
								<Icon color={styles.icon.color} name='shield' type='MaterialCommunityIcons'/>
								<Input 
									placeholder={dictionary.signup.Vpass}
									autoCapitalize='none'
									style={styles.password}
									secureTextEntry
									onChangeText={(text) => this.setState({vPass: String(text), isvPassError: false})}
									onEndEditing={() => {this._verifyvPass()}}
									maxLength={128}
								/>
							</Item>	
							<View style={styles.terms_container}>
								{this.state.isTermsError ? 
									<View style={[styles.centered, styles.terms_error_padding]}>
										<Text style={styles.warning_specific}>{this.state.termsErrorMessage}</Text>
									</View>
								:
									null
								}
								<View style={styles.centered}>
									<View style={styles.terms_radio}>
										<CheckBox
											onPress={() => {
												this.setState((previousState) => {
													if(previousState.termsAccepted){
														return {termsAccepted: false};
													}
													else{
														return {termsAccepted: true, isTermsError: false};
													}
												});
											}}
											checked = {this.state.termsAccepted}
											color = {styles.terms_color.color}
										/>
									</View>
									<Text style={styles.terms_text}>
										I have read and agree to the&nbsp; 
										<Text 
											onPress={this._navigateToInformation} 
											style={[styles.terms_color, styles.link_text]}
										>
											terms and conditions
										</Text>
										.
									</Text>
								</View>
							</View>
						</Form>
						</View>
						<Button 
							block 
							style={styles.button_submit} 
							onPress={this._verifySubmittedData}
						>
							<Text>{dictionary.signup.Submit}</Text>
						</Button>
						<View style={styles.nav_container}>
							<Text style={[styles.small_text, styles.nav_text]}>
								Already have an account? 
							</Text>
							<Button
								transparent
								small
								onPress={this._goToLogin}
							>
								<Text style={styles.nav_button_text} uppercase={false}>Login</Text>
							</Button>
						</View>
						<SpinnerModal isVisible={this.state.isModalVisible}>Signing You Up!</SpinnerModal>
					</Content>
				</ImageBackground>
			</Container>
		);
	}
};

const styles = StyleSheet.create({
	centered: {
		flexDirection: 'row', 
		alignSelf: 'center',
	},
	icon: {
		color: variables.brandLight,
	},
	terms_color: {
		color: variables.brandLight,
	},
	terms_text: {
		fontSize: 13,
	},
	terms_radio: {
		marginRight: 15
	},
	terms_container: {
		marginVertical: 15,
	},
	terms_error_padding: {
		paddingBottom: 3,
	},
	safeAreaView: {
		flex: 1,
	},
	...authStyles,
});

const mapDispatchToProps = {
    setUser,
};

export default connect(null, mapDispatchToProps)(SignUp);