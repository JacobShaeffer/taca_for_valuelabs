/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import {
	StyleSheet,
	Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { View, Text, Content, Container, Card, CardItem, Icon, Spinner, Button } from 'native-base';
import NavHeader from '../../components/general/navHeader';
import variables from '../../../native-base-theme/variables/platform';
import { 
	loadNotificationData,  
	setSelectedNotification,
} from '../../redux/modules/notifications';
import { routeNames } from '../../config/routes';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

class Notifications extends Component {
    constructor(props){
		super(props);
	}

	componentDidMount(){
		this.props.loadNotificationData(this.props.user.uid);
	}

    /**
     * opens the navigation drawer 
     */
    _openNavigation = () => {
        this.props.navigation.openDrawer();
	}

	_openNotification = (index) => {
		this.props.setSelectedNotification(index);
		this.props.navigation.navigate(routeNames.SpecificNotification);
	}
	
	/**
	 * map icon names to each type of notification
	 */
	getIconForType = (type) => {
		switch(type){
			case 'question': 
				return 'clipboard';
			case 'reminder':
				return 'alert';
			case 'message': 
				return 'mail-open';
			default:
				return 'alert';
		}
	}

	/**
	 * returns an array of jsx items to show the proper number of dollar signs on a notification
	 */
	getDollars = (price) => {
		let dollars = [];
		for(let i=0; i<price; i++){
			dollars.push(
				<Icon style={[styles.iconBright, styles.dollarSign]} key={i} name='logo-usd'/>
			);
		}
		return dollars;
	}
	
    render(){
		const { notificationData, isLoading } = this.props;
		const shouldShowNotifications = notificationData && notificationData.length > 0;

		// TODO: add a refesh button to the null screen
		// TODO: add pull to refresh to notification list

        return (
			<Container>
				<NavHeader>
					Notification Center	
				</NavHeader>
				<Content>
					{isLoading ? 
						<View style={[styles.halfWindow, styles.centerContent]}>
							<Spinner size='large'></Spinner>
							<Text style={styles.allDoneText}>Loading...</Text>
						</View>
					: 
						shouldShowNotifications ? (
							<View>
								{notificationData.map((notification, index) =>
									<Card key={index}>
										<CardItem button onPress={() => this._openNotification(index)}>
											<Icon style={styles.icon} name={this.getIconForType(notification.type)}/>
											<Text>{notification.title}</Text>
											<View style={[styles.dollars]}>
												{notification.price > 0 ? 
													this.getDollars(notification.price)
												: null}
											</View>
										</CardItem>
									</Card>
								)}
							</View>	
						) : (
							<View style={[styles.fillWindow, styles.centerContent]}>
								<Text style={styles.allDoneText}>All done!</Text>
								<Text style={styles.allDoneText}>Have a nice Day.</Text>
								<View>
									<Button onPress={() => {this.props.loadNotificationData(this.props.user.uid); console.log('Reloading notifications')}}><Text>Reload</Text></Button>
								</View>
							</View>
						)
					}
				</Content>
			</Container>
        );
    }
}

const test = {
	borderColor: 'red',
	borderWidth: 1,
}

const segment = DEVICE_HEIGHT/12;
const styles = StyleSheet.create({
	fillWindow: {
		height: segment * 10.5,
	},
	halfWindow: {
		height: segment * 5.5,
	},
	centerContent: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	allDoneText: {
		fontSize: 30,
		color: variables.textColor + '66',
	},
	rightText: {
		right: 20,
		textAlign: 'right',
	},
	icon: {
		color: variables.brandPrimary,
	},
	iconBright: {
		color: variables.brandWarning,
	},
	dollarSign: {
		fontSize: 20,
		marginRight: 1,
	},
	dollars: {
		position: 'absolute',
		right: 20,
		flexDirection: 'row',
	}
});

const mapStoreToProps = (store) => ({
    user: 				store.auth.user,
	notificationData:	store.notifications.notificationData,
	isLoading: 			store.notifications.loading,
});

const mapDispatchToProps = {
	loadNotificationData,
	setSelectedNotification,
};

export default connect(mapStoreToProps, mapDispatchToProps)(Notifications);