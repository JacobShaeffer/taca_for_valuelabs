/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

// This is the main screen of the app and were most of the important things take place
// this screen shows the list of Stops and Travels and allows navigation to the Details screen of each
// We also setup the Background geolocation here as well as handle some of the SurveyQuestion triggers
// This document does a lot of the heavy lifiting in this app
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Toast, { DURATION } from 'react-native-easy-toast';
import { SafeAreaView, StackActions } from 'react-navigation';
import moment from 'moment';
import {
    ScrollView,
    RefreshControl,
    Dimensions,
    Platform,
	StyleSheet,
	Image,
} from 'react-native';
import {
    Container, Content,
    Text, Card,
    CardItem,
    Grid, Col,
    Row, Left,
    Right, Button,
    H2, View,
    Body, Icon, Spinner, 
} from 'native-base';
import DateTimePicker 	from 'react-native-modal-datetime-picker';
import NavHeader 		from '../../components/general/navHeader';
import SpinnerModal 	from '../../components/general/modal_busySpinner';
import TimelineCard 	from '../../components/app/diary/TimelineCard';
import StaticMap 		from '../../components/app/diary/StaticMap';
import { throttle } 	from '../../lib/helpers';
import { routeNames } 	from '../../config/routes';
import { setDetailsData } 		from '../../redux/modules/details';
import variables, { isIphoneX }	from '../../../native-base-theme/variables/platform';
import { getModeIcon, getActivityIcon } from '../../config/constants';
import { setCurrentDate, syncDataFromServer, setSelectedIndex, setCurrentDayIndex, setSimpleLoadingIndicator, validateExistingTrip } from '../../redux/modules/diary';

const dim = Dimensions.get('window');
const deviceWidth = dim.width;
const deviceHeight = dim.height;

class Diary extends Component{
    constructor(props){
        super(props);

        this.state = {
            isRefreshing: false,
			datePickerVisible: false,
			dateHeaderTop: 0,
			scrollViewContentSizeChange: 0,
			previousContentHeight: 0,
        }

        this.scrollViewRef = React.createRef();
    }

	/**
	 * react-native function
	 * runs before the first render of this component
     * configure background geolocation with AuthUser
     * get mapdata from firestore
	*/
	componentDidMount(){
		const { user, syncDataFromServer } = this.props;
		syncDataFromServer({uid: user.uid, date: moment()});
	}
	
	/**
	 * 
	 */
	_pushRoute = (route, index) => {
		this.props.setSelectedIndex(index);
		this.props.setDetailsData(this.props.mapData[index]);
		const pushAction = StackActions.push({
			routeName: route,
		});
		this.props.navigation.dispatch(pushAction);
	}
    
    /**
     * sets the currently shown day to the day previous
     */
    _previousDay = throttle(() => {
		// const beginIndex = this.props.currentDayBeginIndex;
		// const distance = beginIndex * 285;
		this.props.setSimpleLoadingIndicator(true);
		// this.props.setCurrentDayIndex({currentDayBeginIndex: 0, currentDayEndIndex: this.props.currentDayEndIndex});
		// this.scrollViewRef.scrollTo({x: 0, y: distance, animated: false});
		// this.scrollViewRef.scrollTo({x: 0, y: 0, animated: true});
        let d = moment(this.props.currentDate);
        d.subtract(1, 'day');
		this._setDate(d, false);
    }, 900);

    /**
     * sets the currently shown day to the day after
     */
    _nextDay = throttle(() => {
		// const newIndex = this.props.mapData.length - 1 ;
		this.props.setSimpleLoadingIndicator(true);
		// this.props.setCurrentDayIndex({currentDayBeginIndex: this.props.currentDayBeginIndex, currentDayEndIndex: newIndex});
        let d = moment(this.props.currentDate);
		d.add(1, 'day');
		this._setDate(d, false);
    }, 900);

    /**
     * sets the current date to the date given
     */
    _setDate = (date, loading = true) => {
		date = moment(date);
		this.props.syncDataFromServer({uid: this.props.user.uid, date, loading}).then(() => {
			this.scrollViewRef.scrollTo({x:0, y:0, animated: true});
		});
	}
	
	/**
	 * verifies the stop or travel without altering any of the other data
	 * @param {number} selectedIndex the index of the selected card
	 */
	_quickVerify = (selectedIndex) => {
		console.log('quickVerify: ', selectedIndex);
		const { user, mapData, updateKeys, validateExistingTrip } = this.props;
		let data = mapData[selectedIndex];
		return validateExistingTrip({
			uid: user.uid,
			tid: updateKeys[selectedIndex],
			selectedIndex,
			updates: {},
			date: data.begin,
		}, data.userCreated);
	}

	/**
	 * 
	 * @param {trip Object} data
	 * @param {number} index data index in parent array
	 * @returns {jsx} renderable map component
	 */
	_renderMapElement = (data, index, inProgress) => {
        let renderable = null;
        if(inProgress){
            renderable = <StaticMap inProgress={data ? data : null} key={index}/>;
        }
		else if(data.type === 'stop'){
			renderable = <StaticMap stop={data.coords} key={index}/>;
		}
		else if(data.type === 'travel'){
            renderable = <StaticMap polyline={data.coords} gps={{start: data.gpsStart, finish: data.gpsFinish}} key={index}/>; 
		}
		else{
			console.error('An error occured parsing the map data into map elements: ', data);
		}
		return renderable;
	}

	_dateHeaderOnLayout = (event) => {
		this.setState({
			headerPadding: Platform.OS === 'ios' ? event.nativeEvent.layout.height - 18 : event.nativeEvent.layout.height
		});
	}

	_handleScrollEvent = (event) => {
		// console.log(event.nativeEvent.contentOffset.y);
	}

    _showDatePicker      = () => this.setState({ datePickerVisible: true });
    _hideDatePicker      = () => this.setState({ datePickerVisible: false });
    _handleDateCancelled = () => this._hideDatePicker();
    _handleDatePicked = (date) => {
        this._setDate(date);
        this._hideDatePicker();
    }

    _onRefresh = () => {
		const { user, currentDate } = this.props;
		this.props.syncDataFromServer({uid: user.uid, date: currentDate});
    }

    render(){
        const {
            inProgress,
        } = this.state;

        const {
            currentDate,
            mapData,
            updateKeys,
			isLoading,
			simpleLoadingIndicator,
			currentDayBeginIndex,
			currentDayEndIndex,
		} = this.props;


		let renderMapData = (data, index) => {

			const currentDayIndexesExist = currentDayBeginIndex === null || currentDayEndIndex === null;
			const currentIndexIsInRange = index > currentDayBeginIndex && index <= currentDayEndIndex && currentDayBeginIndex <= currentDayEndIndex;

			if(currentDayIndexesExist || !currentIndexIsInRange){
				return null;
			}

			return (
				<Row key={updateKeys[index]}>
					<TimelineCard data={data} currentDate={currentDate} index={index} last={currentDayEndIndex}></TimelineCard>
					<Col size={80}>
						<Content>
							<Card>
								<CardItem style={styles.cardPadding}>
									<Body style={styles.cardBodyHeight}>
										{this._renderMapElement(data, index)}
									</Body>
								</CardItem>
								<CardItem>
									<Left>
										<View>
											{
												data.type === 'travel' ?
													<Icon type='MaterialCommunityIcons' style={styles.centerText} name={getModeIcon(data.mode)} size={20}/>
												:
													<Icon type='MaterialCommunityIcons' style={styles.centerText} name={getActivityIcon(data.activity)} size={20}/>
											}
											<Text style={styles.humanizedText}>{moment.duration(moment(data.end).diff(moment(data.begin))).humanize()}</Text>
										</View>
									</Left>
									<Body style={styles.contentCenter}>
										{
											data.verified ? 
												<Icon 
													type='MaterialCommunityIcons' 
													style={styles.centerText} 
													name={'checkbox-marked-outline'} 
													size={30} 
													color={variables.brandSuccess}
												/>
											:  
												<Button small success onPress={() => {this._quickVerify(index)}}>
													<Text>Verify</Text>
												</Button>
										}
									</Body>
									<Right>
										<Button light small onPress={() => {this._pushRoute(routeNames.Details, index)}}>
											<Text>Edit</Text>
											{/* <Icon name='arrow-right' type='MaterialCommunityIcons' size={24}/> */}
										</Button>
									</Right>
								</CardItem>
							</Card>
						</Content>
					</Col>
				</Row>
			);
		}


        return (
            <Container>
                <NavHeader>
                    Travel Diary
                </NavHeader>
                <Grid>
                    <ScrollView 
						ref={(component) => this.scrollViewRef = component} 
						style={styles.scrollView}
						onScroll={this._handleScrollEvent}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isRefreshing} 
                                onRefresh={this._onRefresh}
                            />
                        }
                    >
                        <View style={styles.timeline}/>
                        <View style={[{marginBottom: this.state.headerPadding}]}/>
						{mapData.map(renderMapData)}
                        {moment(currentDate).isSame(moment(), 'day') ? 
                            <Row>
                                <Col size={20}>
									<View style={styles.inProgressTimelineCardContainer}>
                                        <Card>
                                            <Text style={styles.inProgressTimelineCardText}>Now</Text>
                                        </Card>
                                    </View>
                                </Col>
                                <Col size={80}>
                                    <Card>
                                        <CardItem>
                                            <Body>
                                                <Text>In Progress</Text>
                                                {this._renderMapElement(inProgress, 0, true)}
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </Col>
                            </Row>
                        :
                            null
                        }
                        <Row>
                            <Col>
                                <Card style={styles.footerCard}>
                                    <CardItem>
                                        <Left>
                                            <Button style={{backgroundColor: variables.brandInfo}} onPress={this._previousDay}>
												<Text>
													Previous Day
												</Text>
											</Button>
                                        </Left>
                                        <Right>
                                            <Button style={{backgroundColor: variables.brandInfo}} onPress={this._nextDay} disabled={moment(currentDate).isAfter(moment().startOf('day'))}>
												<Text>
													Next Day
												</Text>
											</Button>
                                        </Right>
                                    </CardItem>
                                </Card>
                            </Col>
                        </Row>
                    </ScrollView>
                    <Row style={[styles.absolute, {top: this.state.dateHeaderTop}]} onLayout={this._dateHeaderOnLayout}>
                        <Col>
                            <Card style={styles.dateHeaderCard}>
                                <CardItem>
                                    <Left>
                                        <Icon
                                            name={'chevron-left'} 
                                            size={32} 
                                            onPress={this._previousDay}
                                            type='MaterialCommunityIcons'
                                        />
                                        <Button 
                                            transparent 
                                            style={styles.datePickerButton} 
                                            onPress={ this._showDatePicker }
                                        >
                                            <H2>{moment(currentDate).format('MMM D YYYY')}</H2>
                                        </Button>
                                        <Icon 
                                            name={'chevron-right'} 
                                            size={32} 
                                            onPress={this._nextDay}
                                            type='MaterialCommunityIcons'
                                        />
                                    </Left>
                                    <Right style={styles.alignCenter}>
                                        <View>
                                            { 
											simpleLoadingIndicator ? 
												<Spinner style={{height: 40}}/>
											:
												mapData && mapData.length && mapData.length - this.state.totalVerified ? 
													<View style={styles.totalVerifiedContainer}>
														<Text style={styles.totalVerified}>
															{mapData.length - this.state.totalVerified}
														</Text>
													</View>
												:
													mapData.length > 0 ?//TODO: make this reflect if all trips in a day are verified
														<Icon type='MaterialCommunityIcons' name={'checkbox-marked-outline'} size={30} color={variables.brandSuccess}/>
													:
														null
                                            }
                                        </View>
                                    </Right>
                                </CardItem>
                            </Card>
                        </Col>
                    </Row>
                    <DateTimePicker
                        mode={'date'}
                        isVisible={this.state.datePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._handleDateCancelled}
                        date={moment(currentDate).toDate()}
                    />
                    {/* {this._renderModal()} */}
					<SpinnerModal isVisible={isLoading}>Loading...</SpinnerModal>
                </Grid>
				{
					this.props.popup ?
						<View style={styles.popup}>
							<View style={styles.popupBody}>
								<Image style={{width: 150, height: 150, backgroundColor: 'blue'}} source={{uri: 'http://crcexchange.com/GIF/coin.gif'}}/>
							</View>
						</View>
					:
						null
				}
				<Toast ref={ref => this.toastRef = ref}/>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    testing: {
        borderColor: 'red',
        borderWidth: 1,
	},
	popup: {
		backgroundColor: '#000000' + '77',
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
	},
	popupBody: {
		backgroundColor: 'white',
		justifyContent: 'center',
		position: 'relative',
		flex: 1,
		borderRadius: 10,
		margin: 10,
	},
	contentCenter: {
		justifyContent: 'center',
	},
	humanizedText: {
		textAlign: 'center', 
		fontSize: 11
	},
	footerCard: {
		marginBottom: Platform.OS === 'ios' ? isIphoneX ? 50 : 18 : 0
	},
	absolute: {
		position: 'absolute',
	},
	alignCenter: {
		alignItems: 'center',
	},
	dateHeaderCard: {
		marginTop: 2,
	},
	datePickerButton: {
		borderRadius: 5, 
		paddingRight: 5,
		paddingLeft: 5,
	},
	totalVerifiedContainer:{
		borderRadius: 18, 
		height: 32,
		width: 32, 
		backgroundColor: 'red',
		justifyContent: 'center',
	},
	totalVerified: {
		color: 'white', 
		textAlign: 'center',
		fontWeight: '700',
		fontSize: 18,
	},
	inProgressTimelineCardContainer: {
		flex: 1, 
		paddingTop: 8,
		paddingBottom: 8,
		paddingRight: 3,
		paddingLeft: 3,
		margin: 2,
		justifyContent: 'flex-end'
	},
	inProgressTimelineCardText: {
		textAlign: 'center', 
		fontSize: 12, 
		color: 'black',
	},
	centerText: {
		textAlign: 'center'
	},
    timeline: {
        position: 'absolute',
        borderLeftWidth: 1,
        left: deviceWidth/10,
        top: 0,
        bottom: 5,
        borderColor: variables.brandPrimary,
        borderWidth: 1,
        marginTop: Platform.OS === 'ios' ? 18 : undefined,
        marginBottom: Platform.OS === 'ios' ? isIphoneX ? 50 : 18 : undefined,
    },
    scrollView: {
        top: Platform.OS === 'ios' ? 18 : 5,
    },
	safeAreaView: {
		flex: 1,
	},
	cardPadding: {
		paddingBottom: 0,
	},
	cardBodyHeight: {
		height: deviceHeight * 0.25,
		// TODO: maps get stretched because they are not quite the right size
	},
});

const mapStoreToProps = (store) => ({
    user: 					store.auth.user,
    currentDate: 			store.diary.currentDate,
    mapData: 				store.diary.mapData,
	updateKeys: 			store.diary.updateKeys,
	isLoading: 				store.diary.isLoading,
	simpleLoadingIndicator: store.diary.simpleLoadingIndicator,
	currentDayEndIndex: 	store.diary.currentDayEndIndex,
	currentDayBeginIndex: 	store.diary.currentDayBeginIndex,
});

const mapDispatchToProps = {
	setCurrentDate,
	setSelectedIndex,
	syncDataFromServer,
	setDetailsData,
	setCurrentDayIndex,
	setSimpleLoadingIndicator,
	validateExistingTrip,
};

export default connect(mapStoreToProps, mapDispatchToProps)(Diary);