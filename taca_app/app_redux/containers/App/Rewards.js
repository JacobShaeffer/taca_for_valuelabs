/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React, { Component } from 'react';
import {
	StyleSheet,
	Dimensions,
	Image,
} from 'react-native';
import { StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { database } from '../../lib/server';
import { View, Text, Content, Container, Card, CardItem, Badge, Spinner } from 'native-base';
import NavHeader from '../../components/general/navHeader';
import variables from '../../../native-base-theme/variables/platform';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { setRaffleData, setTickets, setSelectedRaffle } from '../../redux/modules/rewards';
import { routeNames } from '../../config/routes';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

class Rewards extends Component {
    constructor(props){
		super(props);
	}
	
	componentDidMount(){
		// ticket count needs to be updated whenever this screen loads (unblurs?)
		// database.doGetIncentives(this.props.user.uid).then((data) => {
		// 	if(data)
		// 		this.props.setTickets(data.tickets || null);
		// 	else 
		// 		this.props.setTickets(null);
		// });
		database.onGetIncentives(this.props.user.uid, (data) => {
			if(data)
				this.props.setTickets(data.tickets || null);
			else 
				this.props.setTickets(null);
		});
		database.doGetItems().then((data) => {
			this.props.setRaffleData(data);
		});
	}

	componentWillUnmount(){
		database.offGetIncentives(this.props.user.uid);
	}

    /**
     * opens the navigation drawer 
     */
    _openNavigation = () => {
        this.props.navigation.openDrawer();
	}

	_openRaffle = (key) => {
		console.log('open Raffle: ', key);
		this.props.setSelectedRaffle(key);
		this._navigate(routeNames.RaffleDetails);
	}

	_openTicketTutorial = () => {
		this._navigate(routeNames.TicketTutorial);
	}

	_navigate = (route) => {
		const pushAction = StackActions.push({
			routeName: route,
		});
		this.props.navigation.dispatch(pushAction);
	}

    render(){
		const { tickets, raffleData, ticketsHaveLoaded, raffleDataLoaded } = this.props;

        return (
			<Container>
				<NavHeader>
					Rewards
				</NavHeader>
				<Content>
					<Card style={styles.ticketsContainer}>
						<CardItem style={[styles.fillHeight]}>
							<View style={[styles.ticketsTitle]}>
								<Text style={styles.ticketsText}>
									Tickets	
								</Text>
							</View>
							<View style={[styles.ticketsCount]}>
								{ticketsHaveLoaded ? 
									<Text style={styles.ticketsCountText}>
										{tickets ? 
											tickets
										:
											0
										}	
									</Text>
								:
									<Spinner></Spinner>
								}
							</View>
						</CardItem>
					</Card>
					<Card>
						<CardItem style={styles.selectableBackground} button onPress={this._openTicketTutorial}>
							<View>
								<Text>Click here to learn how to earn tickets.</Text>
							</View>
						</CardItem>
					</Card>
					<Card>
						<CardItem style={[styles.centerContent, styles.raffleBackground]}>
							<View style={styles.raffleContainer}>
								<View style={styles.raffleTitle}>
									<Text style={[styles.centerText, styles.raffleTitleText]}>
										Available Raffles!
									</Text>
								</View>
								{
									raffleDataLoaded ?
										Object.keys(raffleData).map((element) => 
											<TouchableOpacity onPress={() => this._openRaffle(element)} key={element}>
												<View style={styles.raffleBox}>
													<Image 
														style={styles.raffleImage}
														source={{uri: raffleData[element].uri}}/>	
													<Badge style={styles.raffleValue}><Text>${raffleData[element].worth}</Text></Badge>
												</View>
											</TouchableOpacity>
										)
									:
										<Spinner></Spinner>
								}
							</View>
						</CardItem>
					</Card>
				</Content>
			</Container>
        );
    }
}

const test = {
	red: {
		borderColor: 'red',
		borderWidth: 1,
	},
	blue: {
		borderColor: 'blue',
		borderWidth: 1,
	},
	green: {
		borderColor: 'green',
		borderWidth: 1,
	}
};

const segment = DEVICE_HEIGHT/12;
const styles = StyleSheet.create({
	centerContent: {
		justifyContent: 'center',
		alignContent: 'center',
		alignItems: 'center',
	},
	centerContentVertical: {
		alignItems: 'center',
	},
	fillHeight: {
		height: '100%',
	},
	centerText: {
		textAlign: 'center',
	},

	selectableBackground: {
		backgroundColor: variables.brandLight,
	},

	ticketsContainer: {
		height: segment*2,
	},
	ticketsTitle: {
		flex: 1,
		alignItems: 'center',
	},
	ticketsCount: {
		flex: 1,
		alignItems: 'center',
	},
	ticketsText: {
		fontSize: 40,
	},
	ticketsCountText: {
		fontSize: 60,
	},

	raffleContainer: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
	},
	raffleBackground: {
		backgroundColor: 'white',
	},
	raffleTitle: {
		width: '100%', 
		justifyContent: 'center',
	},
	raffleTitleText: {
		fontSize: 20,
		color: variables.brandWarning,
	},
	raffleBox: {
		width: DEVICE_WIDTH / 3,
		height: DEVICE_WIDTH / 3,
		position: 'relative',
		backgroundColor: 'white',
		borderColor: 'black',
		borderWidth: 1,
		borderRadius: 5,
		margin: 10,
	},
	raffleImage: {
		...StyleSheet.absoluteFill,
		margin: 5,
	},
	raffleValue: {
		top: 5,
		left: 5,
	}
});

const mapStoreToProps = (store) => ({
	user: store.auth.user,
	tickets: store.rewards.tickets,
	progress: store.rewards.progress,
	raffleData: store.rewards.raffleData,
	ticketsHaveLoaded: store.rewards.ticketsHaveLoaded,
	raffleDataLoaded: store.rewards.raffleDataLoaded,
});

const mapDispatchToProps = {
	setTickets,
	setRaffleData,
	setSelectedRaffle,
};

export default connect(mapStoreToProps, mapDispatchToProps)(Rewards);