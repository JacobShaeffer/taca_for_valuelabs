/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import { createStackNavigator, createAppContainer, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import { Animated, Easing } from 'react-native';

import SignUp from '../containers/Auth/SignUp';
import Login from '../containers/Auth/Login';
import SplashScreen from '../containers/SplashScreen';
import Information from '../components/auth/Information'; 
import ForgotPassword from '../components/auth/forgotPassword';
import Diary from '../containers/App/Diary';
import Details from '../components/app/diary/Details';
import Rewards from '../containers/App/Rewards';
import Notifications from '../containers/App/Notifications';
import SpecificNotification from '../components/app/notifications/SpecificNotification';
import Create from '../components/app/diary/details/Create';
import DrawerContentComponent from '../components/config/routes/DrawerContentComponent';
import RaffleDetails from '../components/app/rewards/RaffleDetails';
import TicketTutorial from '../components/app/rewards/TicketTutorial';

const SignUpStack = createStackNavigator({
    SignUp: {
        screen: SignUp,
        navigationOptions: {
            gestuesEnabled: false,
        }
    },
    Information: {
        screen: Information,
        navigationOptions: {
            gestuesEnabled: true,
        }
    }
}, {
    headerMode: 'none',
});

const LoginStack = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            gestuesEnabled: false,
        }
    },
    ForgotPassword: {
        screen: ForgotPassword,
        navigationOptions: {
            gestuesEnabled: true,
        }
    }
}, {
    headerMode: 'none',
})

const AuthenticationStack = createStackNavigator({
    Login: {
        screen: LoginStack,
        navigationOptions: {
            gestuesEnabled: false,
        }
    },
    SignUp: {
        screen: SignUpStack,
        navigationOptions: {
            gestuesEnabled: false,
        }
    },
}, {
    headerMode: 'none',
    transitionConfig: () => ({
        // This provides animationless transition from login to signup screens
        transitionSpec: {
            duration: 0,
            timing: Animated.timing,
            easing: Easing.step0,
        },
    }),
})

const DiaryStack = createStackNavigator({
	Diary: {
		screen: Diary,
	},
	Details: {
		screen: Details,
	},
	Create: {
		screen: Create,
	},
}, {
	headerMode: 'none',
});

DiaryStack.navigationOptions = {
    title: 'Travel Diary',
};

const RewardStack = createStackNavigator({
	Rewards: {
		screen: Rewards,
	},
	RaffleDetails: {
		screen: RaffleDetails,
	},
	TicketTutorial: {
		screen: TicketTutorial,
	},
}, {
	headerMode: 'none',
});

RewardStack.navigationOptions = {
	title: 'Rewards'
};

const NotificationStack = createStackNavigator({
	Notifications: {
		screen: Notifications,
	},
	SpecificNotification: {
		screen: SpecificNotification,
	}
}, {
	headerMode: 'none',
});

NotificationStack.navigationOptions = {
	title: 'Notifications'
};

const AppDrawer = createDrawerNavigator({
    NotificationStack: {
        screen: NotificationStack,
    },
    RewardStack: {
        screen: RewardStack,
    },
    DiaryStack: {
		screen: DiaryStack,
    },
},{
    contentComponent: DrawerContentComponent,
})

const CreateAppNavigator = 
    createAppContainer(
        createSwitchNavigator({
            SplashScreen: {
                screen: SplashScreen,
            },
            Auth: AuthenticationStack,
			App: AppDrawer,
        })
    );

export default CreateAppNavigator;

export const routeNames = {
    Login: 'Login',
    SignUp: 'SignUp',
    Information: 'Information',
    Auth: 'Auth',
    App: 'App',
	ForgotPassword: 'ForgotPassword',
	Diary: 'Diary',
	DiaryStack: 'DiaryStack',
	Details: 'Details',
	Create: 'Create',
	Rewards: 'Rewards',
	RewardStack: 'RewardStack',
	RaffleDetails: 'RaffleDetails',
	TicketTutorial: 'TicketTutorial',
	NotificationStack: 'NotificationStack',
	Notifications: 'Notifications',
	SpecificNotification: 'SpecificNotification',
}