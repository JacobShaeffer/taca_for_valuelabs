/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import React from 'react';
import {
    Dimensions,
} from 'react-native';
import variables from '../../native-base-theme/variables/platform';

const dim = Dimensions.get('window');
const deviceHeight = dim.height;

export const authStyles = {
	form_background: {
		// borderRadius: 2,
        // backgroundColor: localColors.Light + 'af',	
		// marginHorizontal: 5,
		// shadowColor: '#000',
		// shadowOffset: { width: 0, height: 4 },
		// shadowOpacity: 1,
		// shadowRadius: 2,
		// elevation: 2,
	},
	image: {
		height: null,
		width: null,
		resizeMode: 'cover',
		flex: 1,
	},
	topPadding: {
		marginTop: deviceHeight/5,
	},
	password: {
		fontSize: 16,
	},
	warning_general: {
		color: variables.brandDanger, 
		textAlign: 'center', 
		margin: 15,
		marginTop: 0,
		textShadowColor: variables.brandDark + '33',
		textShadowOffset: { width: 1, height: 1 },
		textShadowRadius: 1,
	},
	warning_specific: {
		color: variables.brandDanger,
		textShadowColor: variables.brandDark + '33',
		textShadowOffset: { width: 1, height: 1 },
		textShadowRadius: 1,
		marginLeft: 15,
		marginTop: 10,
		marginBottom: -10,
	},
	button_submit: {
		margin: 15, 
		marginTop: 50,
		backgroundColor: variables.brandDark,
	},
	nav_container: {
		margin: 15,
		flexDirection: 'row',
		justifyContent: 'center',
	},
	small_text: {
		fontSize: 11,
	},
	nav_text: {
		marginVertical: 8,
		marginRight: -7,
	},
	nav_button_text:{
		color: variables.brandLight,
		textDecorationLine: 'underline',
	},
	link_text: {
		textDecorationLine: 'underline',
	}
}