/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import { isNumeric } from '../lib/helpers';

export const activities = [ 
    {name: 'Other', icon: 'map-marker', value: 0}, 
    {name: 'Primary Home', icon: 'home', value: 1}, 
    {name: 'Work', icon: 'office-building', value: 2}, 
    {name: 'Work Related', icon: 'briefcase', value: 3}, 
    {name: 'Education/School', icon: 'school', value: 4}, 
    {name: 'Religious', icon: 'church', value: 5},//This is a little insensative seeing as how the icon is specifically a christian church
    {name: 'Shopping', icon: 'shopping', value: 6}, 
    {name: 'Personal Errands/Tasks', icon: 'account-convert', value: 7}, 
    {name: 'Dropoff/Pickup/Accompany Someone', icon: 'account-multiple', value: 8}, 
    {name: 'Change Travel Mode/Transfer', icon: 'transit-transfer', value: 9}, 
    {name: 'Health Care', icon: 'home-plus', value: 10}, 
    {name: 'Eat Meals Out', icon: 'silverware', value: 11}, 
    {name: 'Social/Recreational/Entertainment', icon: 'account-group', value: 12},//sofa 
    {name: 'Exercise/Play Sports', icon: 'basketball', value: 13}, 
    {name: 'Secondary Home', icon: 'home', value: 14}, 
];

export const getActivityIcon = (index) => {
    if(isNumeric(index)){
        if(index < activities.length){
            return activities[index].icon;
        }
        else{
            return activities[0].icon;
        }
    }
    else{
        return activities[0].icon;
    }
};

export const getActivityName = (index) => {
    if(isNumeric(index)){
        if(index < activities.length){
            return activities[index].name;
        }
        else{
            return activities[0].name;
        }
    }
    else{
        return activities[0].name;
    }
}

export const modes = [
    {name: 'Other', icon: 'car', value: 0},//find a better other icon
    {name: 'Bicycle', icon: 'bike', value: 1},
    {name: 'Bus', icon: 'bus', value: 2},
    {name: 'Vehicle', icon: 'car', value: 3},
    {name: 'Foot', icon: 'walk', value: 4},
    {name: 'Light Rail', icon: 'train', value: 5},
    {name: 'Taxi/Car Service', icon: 'car', value: 6},
    {name: 'Air', icon: 'airplane', value: 7},
];

export const getModeIcon = (index) => {
    if(isNumeric(index)){
        if(index < modes.length){
            return modes[index].icon;
        }
        else{
            return modes[0].icon;
        }
    }
    else{
        return modes[0].icon;
    }
};

export const getModeName = (index) => {
    if(isNumeric(index)){
        if(index < modes.length){
            return modes[index].name;
        }
        else{
            return modes[0].name;
        }
    }
    else{
        return modes[0].name;
    }
};