/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import auth from './modules/auth';
import diary from './modules/diary';
import details from './modules/details';
import newStop from './modules/newStop';
import initialStore from './initialStore';
import rewards from './modules/rewards';
import notifications from './modules/notifications';

let middleware = applyMiddleware(thunk);

//DEV
middleware = composeWithDevTools(middleware);
//DEV

const reducer = combineReducers({
    auth,
	diary,
	details,
	newStop,
	rewards,
	notifications,
});

const configureStore = () => createStore(reducer, initialStore, middleware);
export default configureStore;