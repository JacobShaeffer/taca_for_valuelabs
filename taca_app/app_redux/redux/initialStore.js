/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import moment from 'moment';

export const auth = {
	user: null, 
	isLoggingIn: false,
};

export const diary = {
	currentDate: moment(),
	mapData: [],
	updateKeys: [],
	currentDayBeginIndex: null,
	currentDayEndIndex: null,
	isLoading: true,
	simpleLoadingIndicator: false,
	selectedIndex: null,
};

export const details = {
	detailsData: null,
	changes: {},
	isDeleteModalVisible: false,
};

export const newStop = {
	travelBefore: {},
	stop: {},
	travelAfter: {},
	screenState: 0,
};

export const rewards = {
	tickets: null,
	raffleData: null,
	ticketsHaveLoaded: false,
	raffleDataLoaded: false,
	selectedRaffle: null,
};

export const notifications = {
	notificationData: [],
	loading: false,
	selectedNotification: null,
}

export default initialStore = {
	auth: {...auth},
	diary: {...diary},
	details: {...details},
	newStop: {...newStop},
	rewards: {...rewards},
	notifications: {...notifications},
}