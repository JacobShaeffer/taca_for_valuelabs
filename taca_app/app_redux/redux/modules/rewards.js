/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import { rewards } from '../initialStore';

// Actions
const SET_TICKETS = 'taca/rewards/SET_TICKETS';
const SET_RAFFLE_DATA = 'taca/rewards/SET_RAFFLE_DATA';
const SET_SELECTED_RAFFLE = 'taca/rewards/SET_SELECTED_RAFFLE';
const INVALIDATE_REWARDS = 'taca/rewards/SET_TICKETS';

// Reducer
export default function reducer(store = {}, action = {}){
    switch(action.type){
        case SET_TICKETS:
			return { ...store, ...action.payload, ticketsHaveLoaded: true }
        case SET_RAFFLE_DATA:
			return { ...store, ...action.payload, raffleDataLoaded: true }
		case SET_SELECTED_RAFFLE: 
			return { ...store, ...action.payload }
		case INVALIDATE_REWARDS:
			return { ...rewards }
        default: 
            return store;
    }
}

// Action Creators
export const setTickets = (total) => ({type: SET_TICKETS, payload: {tickets: total}});
export const setRaffleData = (data) => ({type: SET_RAFFLE_DATA, payload: {raffleData: data}});
export const setSelectedRaffle = (key) => ({type: SET_RAFFLE_DATA, payload: {selectedRaffle: key}});
export const invalidateRewards = () => ({type: INVALIDATE_REWARDS});

// Thunk Creators