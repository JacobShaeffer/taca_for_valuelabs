/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import initialStore from '../initialStore';
import { firestore, https } from '../../lib/server';
import { AsyncStorage } from 'react-native';
import moment from 'moment';
import { valueToPosition } from '@ptomasroos/react-native-multi-slider/converters';

// Actions
const SET_NOTIFICATION_COUNT = 'taca/notifications/SET_NOTIFICATION_COUNT';
const SET_NOTIFICATION_DATA = 'taca/notifications/SET_NOTIFICATION_DATA';
const SET_LOADING = 'taca/notifications/SET_LOADING';
const SET_SELECTED_NOTIFICATION = 'taca/notifications/SET_SELECTED_NOTIFICATION';
const SET_USER_DATA = 'taca/notifications/SET_USER_DATA';

// Reducer
export default function reducer(store = {}, action = {}){
    switch(action.type){
		case SET_NOTIFICATION_COUNT: 
			return {...store, notificationCount: action.payload};
		case SET_NOTIFICATION_DATA:
			return {...store, notificationData: action.payload};
		case SET_LOADING: 
			return {...store, loading: action.payload};
		case SET_SELECTED_NOTIFICATION:
			return {...store, selectedNotification: action.payload};
		case SET_USER_DATA:
			return {...store, userData: action.payload};
        default: 
            return store;
    }
}

// Action Creators
export const setNotificationCount = (count) => ({type: SET_NOTIFICATION_COUNT, payload: count});
export const setNotificationData = (data) => ({type: SET_NOTIFICATION_DATA, payload: data});
export const setLoading = (isLoading) => ({type: SET_LOADING, payload: isLoading});
export const setSelectedNotification = (selection) => ({type: SET_SELECTED_NOTIFICATION, payload: selection});
export const setUserData = (data) => ({type: SET_USER_DATA, payload: data});


// Thunk Creators
/**
 * 
 * @param {String} nid 
 * @param {Array} answers 
 */
export const questionSubmit = (nid) => {
	return function(dispatch, getStore){
		let data = getStore().notifications.notificationData;
		let updated = data.filter(notification => {
			console.log('notifID: ', notification.notifID);
			let keep = notification.notifID != nid
			return keep;
		});
		console.log('updated: ', updated);
		return dispatch(setNotificationData(updated));
	}
}

export const notificationDismissed = (nid) => {
	return function(dispatch){
		let data = getStore().notifications.notificationData;
		let updated = data.filter(notification => notification.notifid !== nid);
		return dispatch(setNotificationData(updated));
	}
}

export const loadNotificationData = (uid) => {
	return async function(dispatch){
		dispatch(setLoading(true));

		try{
			let notificationsSnapshot = await firestore.doGetNotificationsList(uid);
			let notifications = notificationsSnapshot.data().notifications;
			if(!notifications){
				return;
			}
			let data = [];
			for(let i=0; i<notifications.length; i++){
				let next = await firestore.doGetNotification(notifications[i]);
				data.push(next.data());
			}
			dispatch(setNotificationData(data));
			dispatch(setNotificationCount(2));
				
		}
		catch(error){
			console.error('there was an error getting notification data: ', error);
		}
		finally {
			return dispatch(setLoading(false));
		}
	}
}