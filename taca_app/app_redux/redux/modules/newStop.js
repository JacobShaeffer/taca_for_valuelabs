/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import initialStore from '../initialStore';

// Actions
const UPDATE_TRAVEL_AFTER = 'taca/newStop/UPDATE_TRAVEL_AFTER';
const UPDATE_TRAVEL_BEFORE = 'taca/newStop/UPDATE_TRAVEL_BEFORE';
const UPDATE_STOP = 'taca/newStop/UPDATE_STOP';
const INVALIDATE_NEW_STOP_STORE = 'taca/newStop/INVALIDATE_NEW_STOP_STORE';
const NEXT_SCREEN_STATE = 'taca/newStop/NEXT_SCREEN_STATE';
const PREVIOUS_SCREEN_STATE = 'taca/newStop/PREVIOUS_SCREEN_STATE';

// Reducer
export default function reducer(store = {}, action = {}){
    switch(action.type){
		case UPDATE_TRAVEL_AFTER:
			return {...store, travelAfter: {...store.travelAfter, ...action.payload}};
		case UPDATE_TRAVEL_BEFORE:
			return {...store, travelBefore: {...store.travelBefore, ...action.payload}};
		case UPDATE_STOP:
			return {...store, stop: {...store.stop, ...action.payload}};
		case NEXT_SCREEN_STATE: 
			return {...store, screenState: store.screenState + 1};
		case PREVIOUS_SCREEN_STATE:
			return {...store, screenState: store.screenState - 1};
		case INVALIDATE_NEW_STOP_STORE: 
			return {...initialStore.newStop};
        default: 
            return store;
    }
}

// Action Creators
export const updateTravelBefore = (data) => ({type: UPDATE_TRAVEL_BEFORE, payload: data });
export const updateTravelAfter = (data) => ({type: UPDATE_TRAVEL_AFTER, payload: data });
export const updateStop = (data) => ({type: UPDATE_STOP, payload: data });
export const invalidateNewStopStore = () => ({type: INVALIDATE_NEW_STOP_STORE});
export const nextScreenState = () => ({type: NEXT_SCREEN_STATE});
export const previousScreenState = () => ({type: PREVIOUS_SCREEN_STATE});

// Thunk Creators
export const setNewStopCoords = (data) => {
	console.log('is this working?');
	return function(dispatch){
		//calculate the average center of the data and set stop's coords
		const coords = data.coords;
		let latitudeTotal = 0;
		let longitudeTotal = 0;
		let total = 0;
		let counter = 0;
		coords.forEach(element => {
			if(counter !== 0){
				counter ++;
				if(counter === 3){
					counter = 0;
				}
				return;
			}
			latitudeTotal += element.latitude;
			longitudeTotal += element.longitude;
			total++;
			counter++;
		});
		let newCoords = {latitude: latitudeTotal / total, longitude: longitudeTotal / total};
		console.log(newCoords);
		return dispatch(updateStop({coords: newCoords}));
	}
}