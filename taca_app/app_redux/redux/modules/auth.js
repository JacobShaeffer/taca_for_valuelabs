/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import { configureBackgroundGeolocation } from '../../lib/backgroundGeolocation/geolocationUtils';
import initialStore from '../initialStore';

// Actions
const SET_USER = 'taca/auth/SET_USER';
const INVALIDATE_USER = 'tava/auth/INVALIDATE_USER';

// Reducer
export default function reducer(store = {}, action = {}){
    switch(action.type){
        case SET_USER:
            return { ...store, user: action.payload }
		case INVALIDATE_USER: 
			return { ...initialStore.auth };
        default: 
            return store;
    }
}

// Action Creators
export const setUser = (user) => ({type: SET_USER, payload: user});
export const invalidateReduxUser = () => ({type: INVALIDATE_USER})

// Thunk Creators
export const invalidateUser = () => {
    return function (dispatch){
        return configureBackgroundGeolocation({
			url: '',
			autoSync: false,
        }).catch((error) => {
            console.error('there was an error invalidating the geolocation user url: ', error);
        }).finally(() => {
            dispatch(invalidateReduxUser());
        });
    }
}