/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import { firestore, database, https } from '../../lib/server';
import { doGetTripDataForDate } from '../../lib/helpers';
import moment from 'moment';

// Actions
const SET_CURRENT_DATE = 'taca/diary/SET_DATE';
const SET_LOADING = 'taca/diary/SET_LOADING';
const SET_SIMPLE_LOADING_INDICATOR = 'taca/diary/SET_SIMPLE_LOADING_INDICATOR';
const SET_SELECTED_INDEX = 'taca/diary/SET_SELECTED_INDEX';
const SYNC_FROM_SERVER = 'taca/diary/SYNC_FROM_SERVER';
const UPDATE_MAP_DATA_ITEM = 'taca/diary/UPDATE_MAP_DATA_ITEM';
const INSERT_ITEM = 'tava/diary/INSERT_ITEM';
const DELETE_ITEM = 'taca/diary/DELETE_ITEM';
const SET_CURRENT_DAY_INDEX = 'taca/diary/SET_CURRENT_DAY_INDEX';


// Reducer
export default function reducer(store = {}, action = {}){
    switch(action.type){
        case SET_CURRENT_DATE:
			return { ...store, currentDate: action.payload };
		case SYNC_FROM_SERVER:
			return {
				...store,
				currentDate: action.payload.currentDate,
				mapData: action.payload.mapData,
				updateKeys: action.payload.updateKeys,
				currentDayBeginIndex: action.payload.currentDayBeginIndex,
				currentDayEndIndex: action.payload.currentDayEndIndex,
				isLoading: false,
				simpleLoadingIndicator: false,
			}
		case SET_LOADING: 
			return { ...store, isLoading: action.payload }
		case SET_SIMPLE_LOADING_INDICATOR:
			return {...store, simpleLoadingIndicator: action.payload}
		case SET_SELECTED_INDEX: 
			return { ...store, selectedIndex: action.payload }
		case UPDATE_MAP_DATA_ITEM:
			//TODO: FIXME: sorting the array like this can cause update keys to become out of sync with map data
			//updatedMapData = sortArray(updateItem(store.mapData, action.payload));
			updatedMapData = updateItem(store.mapData, action.payload);
			return {
				...store,
				isLoading: false,
				mapData: updatedMapData,
				// ...currentDay,
			}
		case INSERT_ITEM: 
			let updatedMapData = insertItem(store.mapData, action.payload.mapDataIntent);
			let updatedKeys = insertItem(store.updateKeys, action.payload.updateKeyIntent);
			return {
				...store,
				mapData: updatedMapData,
				updateKeys: updatedKeys,
			}	
		case DELETE_ITEM: 
			updatedMapData = removeItem(store.mapData, action.payload);
			return {
				...store, 
				isLoading: false,
				mapData: updatedMapData,
			};
		case SET_CURRENT_DAY_INDEX:
			const { currentDayBeginIndex, currentDayEndIndex } = action.payload;
			return {
				...store,
				currentDayBeginIndex,
				currentDayEndIndex,
			}
        default: 
        	return store;
    }
}

// Reducer functions
/**
 * @param {{index: number, item: any}} intent 
 */
const insertItem = (array, intent) => {
	let newArray = array.slice();
	newArray.splice(intent.index, 0, intent.item);
	return newArray;
}

/**
 * @param {{index: number}} intent 
 */
const removeItem = (array, intent) => {
	let newArray = array.slice();
	newArray.splice(intent.index, 1);
	return newArray;
}

/**
 * @param {{index: number, item: any}} intent 
 */
const updateItem = (array, intent) => {
	return array.map((item, index) => {
		if(index === intent.index){
			// we found the index we were lookin for
			return {
				// this spreads the original item, then spreads the update object ontop of it overwriting only the fields we want
				...item,
				...intent.item
			};
		}
		else{
			return item;
		}
	});
}

const sortArray = (array) => {
	return array.sort((a, b) => {
		ab = moment(a.begin);
		bb = moment(b.begin);
		if(ab.isBefore(bb)){
			return -1;
		}
		else if(ab.isAfter(bb)){
			return 1;
		}
		return 0;
	});
}

// Action Creators
export const setCurrentDate = (date) => ({type: SET_CURRENT_DATE, payload: date});
export const setLoading = (isLoading) => ({type: SET_LOADING, payload: isLoading});
export const setSimpleLoadingIndicator = (isLoading) => ({type: SET_SIMPLE_LOADING_INDICATOR, payload: isLoading});
export const setSelectedIndex = (index) => ({type: SET_SELECTED_INDEX, payload: index});
export const syncServerData = (data) => ({type: SYNC_FROM_SERVER, payload: data});
export const updateMapData = (item, index) => ({type: UPDATE_MAP_DATA_ITEM, payload: {item, index}});
export const insertData = (intent) => ({type: INSERT_ITEM, payload: intent});
export const deleteItem = (index) => ({type: DELETE_ITEM, payload: {index}});
export const setCurrentDayIndex = (newRange) => ({type: SET_CURRENT_DAY_INDEX, payload: newRange});

// Thunk Creators
/**
 * 
 * @param {*} uid user id belonging to the user being updated on server
 * @param {*} tid key to the server trip being updated
 * @param {*} before
 * @param {*} stop
 * @param {*} after
 * @param {*} index
 */
export const addNewStop = ({uid, tid, before, stop, after, index}) => {
	return async function(dispatch){
		dispatch(setLoading(true));

		await firestore.doAddRemoteTrip(uid, stop)
			.then((documentReference) => {
				let payload = {
					mapDataIntent: {
						item: stop,
						index: index+1,
					},
					updateKeyIntent: {
						item: documentReference.id,
						index: index+1,
					}
				}
				dispatch(insertData(payload));
			})
			.catch((error) => {
				//TODO: what to do here?
			});

		await firestore.doAddRemoteTrip(uid, after)
			.then((documentReference) => {
				let payload = {
					mapDataIntent: {
						item: after,
						index: index+2,
					},
					updateKeyIntent: {
						item: documentReference.id,
						index: index+2,
					}
				}
				dispatch(insertData(payload));
			})
			.catch((error) => {
				//TODO: what to do here?
			});

		return firestore.doUpdateRemoteTrip(uid, tid, before)
			.then(() => {
				let intent = {
					item: before,
					index,
				}
				return dispatch(updateMapData(intent));
			})
			.catch((error) => {
				dispatch(setLoading(false));
				//TODO: what to do here?
			});
	}
}

/**
 * 
 * @param {*} uid
 * @param {*} date
 */
export const syncDataFromServer = ({uid, date, loading}) => {
	return function(dispatch){
		if(loading)
			dispatch(setLoading(true));
		return doGetTripDataForDate(uid, date).then((data) => {
			return dispatch(syncServerData(data));
		});
	}	
}

/**
 * updated the server and local data with the data object provided
 * @param {*} uid user id belonging to the user being updated on server
 * @param {*} tid key to the server trip being updated
 * @param {*} selectedIndex index in mapdata to be updated locally
 * @param {*} updates update object to be spread over existing index
 * @param {*} date date object for the data being updated
 */
export const validateExistingTrip = ({uid, tid, selectedIndex, updates, date}, userCreated) => {
	return async function(dispatch, getStore){
		console.log('validateExistingTrip');
		dispatch(updateMapData({verified: true, ...updates}, selectedIndex));
		let timestamp = moment(date).toDate();

		let mapData = getStore().diary.mapData;
		let day = moment(date);
		day.startOf('day'); 
		let dayIsVerified;

		try {
			dayIsVerified = await isDayVerified(mapData, day);
			// if this trip was user created then we do not want to verify it normally
			if(userCreated)
				updates = {...updates, verified: true};

			if(Object.keys(updates).length > 0){
				// if there are updates then send update to the server
				await firestore.doUpdateRemoteTrip(uid, tid, updates, timestamp);
			}

			if(userCreated) return null;
			return await https.onTripVerified({tid, timestamp: timestamp.getTime(), dayIsVerified});
		}
		catch(error){
			console.log('there was an error updateing or verifying: ', error);
		}
	}
}

export const deleteExistingData = ({uid, tid, selectedIndex, date}) => {
	return async function(dispatch){

		// TODO: merge surrounding data
		dispatch(deleteItem(selectedIndex));

		try{
			return await firestore.doDeleteRemoteTrip(uid, tid, moment(date).toDate());
		}
		catch(error){
			console.log('there was an error deletting: ', error);
		}
	}
}


// Thunk Helper Functions
const isDayVerified = async (mapData, date) => {
	let allTripsAreVerified = true;
	let thereIsDataAfter = false;

	for(let i=0; i<mapData.length; i++){
		let mapDataDate = moment(mapData[i].begin);
		mapDataDate.startOf('day');
		if(mapDataDate.isSame(date, 'day')){
			if(!mapData[i].verified) 
				allTripsAreVerified = false;
		}
		else if(mapDataDate.isAfter(date, 'day')){
			thereIsDataAfter = true;
		}
	}

	if(allTripsAreVerified && thereIsDataAfter){
		console.log('verifying Day')
		try{
			let data = await https.onDayVerified({timestamp: date.toDate()})
			console.log('onDayVerified: ', data);
			return true;
		}
		catch(error){
			console.log('there was an error verifying a day: ', error);
		}
	}
	return false;
}