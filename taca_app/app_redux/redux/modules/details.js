/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import initialStore from '../initialStore';

// Actions
const SET_DETAILS_DATA = 'taca/details/SET_DETAILS_DATA';
const UPDATE_DETAILS_DATA = 'taca/details/UPDATE_DETAILS_DATA';
const UPDATE_CHANGES = 'taca/details/UPDATE_CHANGES';
const SHOW_DELETE_MODAL = 'taca/details/SHOW_DELETE_MODAL';
const HIDE_DELETE_MODAL = 'taca/details/HIDE_DELETE_MODAL'
const INVALIDATE_DETAILS_STORE = 'taca/details/INVALIDATE_DETAILS_STORE';

// Reducer
export default function reducer(store = {}, action = {}){
    switch(action.type){
		case SET_DETAILS_DATA:
			return {...store, detailsData: action.payload};
		case UPDATE_DETAILS_DATA:
			return updateNestedStore(store, action.payload);
		case UPDATE_CHANGES:
			return {...store, changes: {...store.changes, ...action.payload}};
		case SHOW_DELETE_MODAL: 
			return {...store, isDeleteModalVisible: true};
		case HIDE_DELETE_MODAL:
			return {...store, isDeleteModalVisible: false};
		case INVALIDATE_DETAILS_STORE: 
			return {...initialStore.details};
        default: 
            return store;
    }
}

// Reducer Functions
/**
 * @param {*} store
 * @param {{path: String, update: *}} action 
 */
const updateNestedStore = (store, action) => {
	console.log('action: ', action);
	return {
		...store,
		[action.path]: {
			...store[action.path],
			...action.detailsData,
		}
	}
}

// Action Creators
export const setDetailsData = (detailsData) => ({type: SET_DETAILS_DATA, payload: detailsData});
export const updateDetailsData = (detailsData) => ({type: UPDATE_DETAILS_DATA, payload: { path: 'detailsData', detailsData }});
export const updateChanges = (changes) => ({type: UPDATE_CHANGES, payload: changes});
export const showDeleteModal = () => ({type: SHOW_DELETE_MODAL});
export const hideDeleteModal = () => ({type: HIDE_DELETE_MODAL});
export const invalidateDetailsStore = () => ({type: INVALIDATE_DETAILS_STORE});

// Thunk Creators