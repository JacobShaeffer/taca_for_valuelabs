/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */


/**
 * calculates the segment of the polyline that is closest to the point, 
 * and returns the index of the first point of the closest line segment
 * @param {{latitude: number, longitude: number}[]} polyline 
 * @param {{latitude: number, longitude: number}} point 
 */ 
export const findNearestSegment = (polyline, point) => {
	// TODO:
	let index = polyline.length / 2;
	return index;
};