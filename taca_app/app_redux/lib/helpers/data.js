/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import moment from 'moment'; 
import { firestore, database } from '../../lib/server';

const createMapRegion = (coords, type) => {
    if(coords.length <= 0){
        return {
            latitude: 33.448210,
            longitude: -112.073832,
            latitudeDelta: 0.1,
            longitudeDelta: 0.1,
        }
    }
    
    if(type === 'polyline'){
        let miX, maX, miY, maY;

		let first = coords[0];
        miX = first.latitude;
        maX = first.latitude;
        miY = first.longitude;
		maY = first.longitude;

        coords.map((point) => {
            miX = Math.min(miX, point.latitude);
            maX = Math.max(maX, point.latitude);
            miY = Math.min(miY, point.longitude);
            maY = Math.max(maY, point.longitude);
        });
        
        let latitude = (miX + maX)/2;
        let longitude = (miY + maY)/2;
        let latitudeDelta = (maY - miY) * 0.5 + 0.004;
        let longitudeDelta = (maX - miX) * 0.5 + 0.004;

        return {
            latitude,
            longitude,
            latitudeDelta,
            longitudeDelta,
        }
    }
    else{
        return {
            ...coords,
            latitudeDelta: 0.003,
            longitudeDelta: 0.0015,
        }
    }
}

const handleTravelData = (data) => {
	if(!Array.isArray(data.coords)){
		let newCoords = [];
		let isRunning =  true;
		let i = 0;

		while(isRunning){
			let key = ('000'+i).slice(-4);
			let value = data.coords[key];
			if(value) newCoords.push(value);
			else isRunning = false;
			i++;
		}

		data.coords = newCoords;
	}
    if(data.coords.length > 0) data.region = createMapRegion(data.coords, 'polyline');
    
    return data;
}

const handleStopData = (data) => {
    let newCoords = [];
    let isRunning =  true;
    let i = 0;

    while(isRunning && !!data.locations){
        let key = ('000'+i).slice(-4);
        let value = data.locations[key];
        if(value) newCoords.push(value);
        else isRunning = false;
        i++;
    }

    data.locations = newCoords;
    data.region = createMapRegion(data.coords, 'point');
    
    return data;
}

/**
 * @param {{currentDate: Date, currentDayBeginIndex: number, currentDayEndIndex: number, mapData: any[], updateKeys: String[]}} data
 */
const travelEnds = (data) => {
	let {
		mapData,
		updateKeys,
		currentDate,
		currentDayBeginIndex,
		currentDayEndIndex,
	} = data;

	// TODO: 
    // let mapData = data.current.mapData;

    // if(mapData && mapData.length > 0){
    //     if(mapData[0].type === 'travel'){
    //         if(data.previous && data.previous.mapData && data.previous.mapData[data.previous.mapData.length-1].type === 'stop'){
    //             mapData[0].gpsStart = data.previous.mapData[data.previous.mapData.length-1].coords;
    //         }
    //     }
    //     for(let i=1; i < mapData.length-1; i++){
    //         if(mapData[i].type === 'travel'){
    //             if(mapData[i-1].type === 'stop'){
    //                 mapData[i].gpsStart = mapData[i-1].coords;
    //             }
    //             if(mapData[i+1].type === 'stop'){
    //                 mapData[i].gpsFinish = mapData[i+1].coords;
    //             }
    //         }
    //     }
    //     if(mapData[mapData.length-1].type === 'travel'){
    //         if(data.post && data.post.mapData && data.post.mapData[0].type === 'stop'){
    //             mapData[mapData.length-1].gpsFinish = data.post.mapData[0].coords;
    //         }
    //     }
    // }

    return data;
}

const processData = (travelSnapshot, editSnapshot, verificationSnapshot, date) => {
    let values = [];
	let keys = [];

	edits = {};
	editSnapshot.forEach((element) => {
		edits[element.id] = element.data();
	});

	verificationSnapshot.forEach((element) => {
		let key = element.key;
		edits[key] = {...edits[key], verified: true};
	});
	console.log('edits: ', edits);

    travelSnapshot.forEach((element) => {
		let data = element.data();
		let myEdits = edits[element.id];
		// TODO: trips that only exist in edits are not added to mapData
		if(myEdits){
			data = { ...data, ...myEdits };
			if(data.deleted){
				// this point has been deleted by the user so we skip it
				console.log(element.id, ' was deleted');
				return;
			}
		}

        if(data.type === 'travel'){
            data = handleTravelData(data);
        }
        else if(data.type === 'stop'){
            data = handleStopData(data);
        }
        else{
            // This should not be able to happen, but if it does we just ignore this element
            // we use return here because this is a foreach loop
            return;
        }

        if(data.region){
            values.push(data);
            keys.push(element.id);
        }
	});
	date = moment(date);
	let currentDay = dateClipMapData(values, date);

    let processedData = {
		currentDate: date,
		...currentDay,
        mapData: [...values],
        updateKeys: [...keys]
    };

    return travelEnds(processedData);
}

const dateClipMapData = (mapData, date) => {
	date = moment(date);
    let todayStart = null;
    let todayEnd = null;
	let counter = 0;
	for(let i=0; i<mapData.length; i++){
		let begin = moment(mapData[i].begin);
		let end = moment(mapData[i].end);
		if(begin.isSame(date, 'day') || end.isSame(date, 'day')){
			if(!todayStart) todayStart = counter-1;
			todayEnd = counter;
		}
		counter++;
	}
	return {currentDayBeginIndex: todayStart, currentDayEndIndex: todayEnd};
}

export const doGetTripDataForDate = (uid, date) => {
    let day = 'day';
    let from = moment(date).startOf(day).subtract(1, day).toDate();
	let to = moment(date).endOf(day).add(1, day).toDate();
	

	let tripPromise = firestore.doGetTripsBetweenDates(uid, from, to);
	let editPromise = firestore.doGetEditsBetweenDates(uid, from, to);
	let verifiedPromise = database.doGetVerifiedTripsBetweenDates(uid, from.getTime(), to.getTime());
	return Promise.all([tripPromise, editPromise, verifiedPromise]).then((values) => {
		let tripDataSnapshot = values[0];
		let editDataSnapshot = values[1];
		let verificationSnapshot = values[2];
		return processData(tripDataSnapshot, editDataSnapshot, verificationSnapshot, moment(date));
	})
	.catch((error) => {
        console.log('There was an error getting TripData: ', error);
        return {
			error: true,
			currentDate: date,
            currentDayBeginIndex: 0,
            currentDayEndIndex: 0,
            mapData: [],
            updateKeys: []
        };
	});
}