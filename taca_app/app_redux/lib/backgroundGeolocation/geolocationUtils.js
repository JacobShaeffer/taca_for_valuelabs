/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import BackgroundGeolocation from 'react-native-background-geolocation';

/**
 * changes the configuration of the background geolocation
 * @param {{options}} config 
 */
export const configureBackgroundGeolocation = (config) => {
    return BackgroundGeolocation.setConfig({
        ...config
    })
    .catch((error) => {console.log('oops! ', error)});
}

/**
 * makes sure the background geolocation library is ready to party
 * also sets up the initial configuration
 */
export const backgroundGeolocationReady = () => BackgroundGeolocation.ready({
    // reset: true,
    desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_HIGH,  
    //notificationSmall(or Large)Icon = https://github.com/transistorsoft/react-native-background-geolocation/blob/master/docs/README.md#config-string-notificationsmallicon-app-icon
    stopOnTerminate: false, 
    startOnBoot: true,       
    httpRootProperty: '.',
    batchSync: false,     
    autoSync: false,        
    maxDaysToPersist: -1,
    // logLevel: BackgroundGeolocation.LOG_LEVEL_VERBOSE,
    // heartbeatInterval: 60,

    //ANDROID Options
    foregroundService: true,
    notificationTitle: 'GPS collection active',
    notificationText: 'Touch to open app',
    
    //IOS Options
    preventSuspend: true,
})
.catch((error) => {
    console.log('error: ', error);
});

/**
 * starts the background geolocation process and returns the current state
 */
export const startGeolocationTracking = async () => {
	try{
		let state = await BackgroundGeolocation.start();
		console.log('geolocation tracking was started successfully');
		return state;
	}
	catch(error){
		if(error === 'Permission denied'){
			console.log('failure: ', failure);
			// TODO: inform the user they need to give location permissions for this app to work
		}
		else{
			console.log('there was an error starting the geolocation tracking: ', error);
		}
		return getGeolocationState();
	}
}

/**
 * stops the background geolocation process and returns the current state
 */
export const stopGeolocationTracking = async () => {
	try{
		let state = await BackgroundGeolocation.stop()
		console.log('geolocation tracking was successfully stopped');
		return state;
	}
	catch(error){
		console.log('an error occured trying to turn off geolocation tracking: ', error);
		return getGeolocationState();
	}
}

export const getGeolocationState = async () => {
	let state;
	try{
		state = await BackgroundGeolocation.getState();
	}
	catch(error){
		console.log('an error occured trying to get the background geolocation state');
		state = {};
	}
	return state;
}