/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

// this files found in this folder are used to abstract the database comunication layer to make it much simpler to implement
import * as auth from './auth';
import * as firebase from './firebase';
import * as database from './database';
import * as firestore from './firestore';
import * as https from './https';

export {
    auth, 
    firebase,
	database,
	firestore,
	https,
}
