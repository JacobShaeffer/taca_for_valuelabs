/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import { auth, authProvider } from './firebase';

export const doSignInAnonymously = () => 
    auth.signInAnonymously();

export const doCreateUserWithEmailAndPassword = (email, password) => 
    auth.createUserWithEmailAndPassword(email, password);

export const doLoginWithEmailAndPassword = (email, password) =>
    auth.signInWithEmailAndPassword(email, password);

export const doSignOut = () => 
    auth.signOut();

export const doResetPassword = (email) =>
    auth.sendPasswordResetEmail(email);

export const doReAuth = (email, password) =>
    AuthUser().reauthenticateAndRetrieveDataWithCredential(authProvider.credential(email, password));

export const doUpdatePassword = (password) =>
    AuthUser().updatePassword(password);

export const AuthUser = () => auth.currentUser;

export const onAuthStateChanged = (func) => 
    auth.onAuthStateChanged(func);