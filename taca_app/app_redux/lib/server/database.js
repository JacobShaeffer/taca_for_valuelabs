/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import {
    database,
} from './firebase';
import { doUpdatePassword } from './auth';

// Incentive data
const INCENTIVES = () => database.ref('incentives');

//TODO: think about making this on instead of once
export const doGetItems = () => 
	INCENTIVES().child('items').once('value').then((snapshot) => snapshot.val());

export const doGetItemDetails = (itemID) => 
	INCENTIVES().child(`details/${itemID}`).once('value').then((snapshot) => {
		let dat = snapshot.val();
		console.log('doGetSprecificItem: ', dat);
		return dat;
	}).catch((error) => {console.log('error getting specific reward item: ', error)});


// User data
const USERS = () => database.ref('users');

export const doGetAnsweredQuestions = (uid) => 
	USERS().child(`${uid}/questions/answered`).once('value').then((snapshot) => snapshot.val());

export const doGetAnsweredSeries = (uid) => 
	USERS().child(`${uid}/questions/series`).once('value').then((snapshot) => snapshot.val());

export const doGetIncentives = (uid) =>
	USERS().child(`${uid}/incentives`).once('value').then((snapshot) => snapshot.val());

export const onGetIncentives = (uid, callback) =>
	USERS().child(`${uid}/incentives`).on('value', (snapshot) => {callback(snapshot.val())});
export const offGetIncentives = (uid) =>
	USERS().child(`${uid}/incentives`).off();

	
// VerifiedTrips
const VERIFIEDTRIPS = (uid) => database.ref(`verifiedTrips/${uid}`);

export const doGetVerifiedTripsBetweenDates = (uid, startAt, endAt) => {
	console.group('doGetVerifiedTripsBetweenDates');
		console.log('uid: ', uid);
		console.log('startAt: ', startAt);
		console.log('endAt: ', endAt);
	console.groupEnd();
	return VERIFIEDTRIPS(uid).orderByValue().startAt(startAt).endAt(endAt).once('value');
}