/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import firebase from 'react-native-firebase';

const auth = firebase.auth();
const firestore = firebase.firestore();
const database = firebase.database();
const functions = firebase.functions();
const authProvider = firebase.auth.EmailAuthProvider;

export {
    auth,
    database,
	firestore,
	functions,
    authProvider,
};