/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

//This file is for creating async api calls to the firebase rest endpoint

import { functions } from './firebase';

const onDayVerified = functions.httpsCallable('onDayVerified');
const onTripVerified = functions.httpsCallable('onTripVerified');
const onTicketsUsed = functions.httpsCallable('onTicketsUsed');
const onQuestionAnswered = functions.httpsCallable('onQuestionAnswered');

export {
	onDayVerified,
	onTripVerified,
	onTicketsUsed,
	onQuestionAnswered,
}