/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

import {
    firestore,
} from './firebase';


const pathEnum = {
	data: 'data',
	trips: 'trips',
	edits: 'edits',
	users: 'users',
	notifications: 'notifications',
	questions: 'questions',
}

// Trip data
const TRIPS = (uid) => firestore.collection(pathEnum.data).doc(String(uid)).collection(pathEnum.trips);
const EDITS = (uid) => firestore.collection(pathEnum.data).doc(String(uid)).collection(pathEnum.edits);
const EDIT = (uid, tid) => firestore.collection(pathEnum.data).doc(String(uid)).collection(pathEnum.edits).doc(tid); 
const NOTIFICATION = (nid) => firestore.collection(pathEnum.notifications).doc(String(nid));
const USER_NOTIFICATIONS = (uid) => firestore.collection(pathEnum.users).doc(String(uid));

export const doUpdateRemoteTrip = (uid, tid, data, timestamp) => 
	EDIT(uid, tid).set({...data, timestamp}, {merge: true});
	//FIXME: any data that is inserted into edits needs to have the begin value with it so it can be properly grabbed by doGetTripDataForDate

export const doDeleteRemoteTrip = (uid, tid, timestamp) => 
	doUpdateRemoteTrip(uid, tid, {deleted: true, timestamp});

export const doGetTripsBetweenDates = (uid, from, to) =>
	TRIPS(uid).orderBy('begin').where('begin', '>=', from).where('begin', '<', to).get();

export const doGetEditsBetweenDates = (uid, from, to) =>
	EDITS(uid).orderBy('timestamp').where('timestamp', '>=', from).where('timestamp', '<', to).get();
	
export const doAddRemoteTrip = (uid, data) => 
	TRIPS(uid).add(data);

export const doGetNotificationsList = (uid) =>
	USER_NOTIFICATIONS(uid).get();

export const doGetNotification = (nid) =>
	NOTIFICATION(nid).get();

export const doCreateAnswerDocument = (uid, nid, answers) =>
	USER_NOTIFICATIONS(uid).collection(pathEnum.questions).doc(nid).set({answered: true, answers});