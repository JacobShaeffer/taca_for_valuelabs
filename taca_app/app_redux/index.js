/*********************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * 
 * Author - Jacob Shaeffer
 */

// Root file of app
// sets up navigation, Style, and registers a Root component to allow modal views
import React, { Component } from 'react';
import { StyleProvider, Root } from 'native-base';
import { Provider } from 'react-redux';
import AppNavigator from './config/routes.js';
import configureStore from './redux/configureStore';
import getTheme from '../native-base-theme/components';


class App extends Component{
	constructor(props){
		super(props);
		this.state = {
			store: configureStore(),
		}
	}

    render(){
        return (
            <Root>
                <Provider store={this.state.store}>
                    <StyleProvider style={getTheme()}>
                        <AppNavigator/>
                    </StyleProvider>
                </Provider>
            </Root>
        );
    }
}

export default App;