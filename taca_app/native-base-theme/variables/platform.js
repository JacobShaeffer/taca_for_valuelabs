// @flow

import color from "color";

import { Platform, Dimensions, PixelRatio } from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = undefined;
export const isIphoneX =
platform === "ios" && (deviceHeight === 812 || deviceWidth === 812 || deviceHeight === 896 || deviceWidth === 896);

const localColors = {
  text: '#001b2a',
  inverseText: '#f0fbfa',
  white: '#fff',
  black: '#000',
  badgeRed: '#ED1727',
  disabledGrey: '#b5b5b5',

}
const brandColors = {
  Primary: '#017d8b',
  PrimaryLight: '#53c2c0',
  Secondary: '#1580c0',
  SecondaryLight: '#139fda',
  Dark: '#043b5a',
  Light: '#cedcdb',
  Inverse: '#d24b37',
  InverseLight: '#ed5d46',
}

export default {
  platformStyle,
  platform,

  //Accordion
  headerStyle: "#edebed",
  iconStyle: localColors.black,
  contentStyle: "#f5f4f5",
  expandedIconStyle: localColors.black,
  accordionBorderColor: "#d3d3d3",

  // Android
  androidRipple: true,
  androidRippleColor: "rgba(256, 256, 256, 0.3)",
  androidRippleColorDark: "rgba(0, 0, 0, 0.15)",
  btnUppercaseAndroidText: true,

  // Badge
  badgeBg: localColors.badgeRed,
  badgeColor: localColors.white,
  badgePadding: platform === "ios" ? 3 : 0,

  // Button
  btnFontFamily: platform === "ios" ? "System" : "Roboto_medium",
  btnDisabledBg: localColors.disabledGrey,
  buttonPadding: 6,
  get btnPrimaryBg() {
    return this.brandPrimary;
  },
  get btnPrimaryColor() {
    return this.inverseTextColor;
  },
  get btnInfoBg() {
    return this.brandInfo;
  },
  get btnInfoColor() {
    return this.inverseTextColor;
  },
  get btnSuccessBg() {
    return this.brandSuccess;
  },
  get btnSuccessColor() {
    return this.inverseTextColor;
  },
  get btnDangerBg() {
    return this.brandDanger;
  },
  get btnDangerColor() {
    return this.inverseTextColor;
  },
  get btnWarningBg() {
    return this.brandWarning;
  },
  get btnWarningColor() {
    return this.inverseTextColor;
  },
  get btnTextSize() {
    return platform === "ios" ? this.fontSizeBase * 1.1 : this.fontSizeBase - 1;
  },
  get btnTextSizeLarge() {
    return this.fontSizeBase * 1.5;
  },
  get btnTextSizeSmall() {
    return this.fontSizeBase * 0.8;
  },
  get borderRadiusLarge() {
    return this.fontSizeBase * 3.8;
  },
  get iconSizeLarge() {
    return this.iconFontSize * 1.5;
  },
  get iconSizeSmall() {
    return this.iconFontSize * 0.6;
  },

  // Card
  cardDefaultBg: localColors.white,
  cardBorderColor: "#ccc",
  cardBorderRadius: 2,
  cardItemPadding: platform === "ios" ? 10 : 12,

  // CheckBox
  CheckboxRadius: platform === "ios" ? 13 : 0,
  CheckboxBorderWidth: platform === "ios" ? 1 : 2,
  CheckboxPaddingLeft: platform === "ios" ? 4 : 2,
  CheckboxPaddingBottom: platform === "ios" ? 0 : 5,
  CheckboxIconSize: platform === "ios" ? 21 : 16,
  CheckboxIconMarginTop: platform === "ios" ? undefined : 1,
  CheckboxFontSize: platform === "ios" ? 23 / 0.9 : 17,
  // checkboxBgColor: "#039BE5",
  checkboxBgColor: brandColors.SecondaryLight,
  checkboxSize: 20,
  checkboxTickColor: localColors.white,

  // Color
  // brandPrimary: platform === "ios" ? "#007aff" : "#3F51B5",
  // brandInfo: "#62B1F6",
  // brandSuccess: "#5cb85c",
  // brandDanger: "#d9534f",
  // brandWarning: "#f0ad4e",
  // brandDark: localColors.black,
  // brandLight: "#f4f4f4",
  brandPrimary: brandColors.Primary,
  brandInfo: brandColors.PrimaryLight,
  brandSuccess: brandColors.SecondaryLight,
  brandDanger: brandColors.Inverse,
  brandWarning: brandColors.InverseLight,
  brandDark: brandColors.Dark,
  brandLight: brandColors.Light,

  //Container
  containerBgColor: localColors.white,

  //Date Picker
  datePickerTextColor: localColors.black,
  datePickerBg: "transparent",

  // Font
  DefaultFontSize: 16,
  fontFamily: platform === "ios" ? "System" : "Roboto",
  fontSizeBase: 15,
  get fontSizeH1() {
    return this.fontSizeBase * 1.8;
  },
  get fontSizeH2() {
    return this.fontSizeBase * 1.6;
  },
  get fontSizeH3() {
    return this.fontSizeBase * 1.4;
  },

  // Footer
  footerHeight: 55,
  // footerDefaultBg: platform === "ios" ? "#F8F8F8" : "#3F51B5",
  footerDefaultBg: brandColors.Primary,
  footerPaddingBottom: 0,

  // FooterTab
  tabBarTextColor: platform === "ios" ? "#6b6b6b" : "#b3c7f9",
  tabBarTextSize: platform === "ios" ? 14 : 11,
  activeTab: platform === "ios" ? "#007aff" : localColors.white,
  sTabBarActiveTextColor: "#007aff",
  tabBarActiveTextColor: platform === "ios" ? "#007aff" : localColors.white,
  // tabActiveBgColor: platform === "ios" ? "#cde1f9" : "#3F51B5",
  tabActiveBgColor: brandColors.Primary,

  // Header
//   toolbarBtnColor: platform === "ios" ? "#007aff" : localColors.white,
  toolbarBtnColor: localColors.white,
  // toolbarDefaultBg: platform === "ios" ? "#F8F8F8" : "#3F51B5",
  toolbarDefaultBg: brandColors.Primary,
  toolbarHeight: platform === "ios" ? 64 : 56,
  toolbarSearchIconSize: platform === "ios" ? 20 : 23,
//   toolbarInputColor: platform === "ios" ? "#CECDD2" : localColors.white,
  toolbarInputColor: localColors.white,
  searchBarHeight: platform === "ios" ? 30 : 40,
  searchBarInputHeight: platform === "ios" ? 30 : 50,
//   toolbarBtnTextColor: platform === "ios" ? "#007aff" : localColors.white,
  toolbarBtnTextColor: localColors.white,
  // toolbarDefaultBorder: platform === "ios" ? "#a7a6ab" : "#3F51B5",
  toolbarDefaultBorder: brandColors.Primary,
  iosStatusbar: platform === "ios" ? "dark-content" : "light-content",
  get statusBarColor() {
    return color(this.toolbarDefaultBg)
      .darken(0.2)
      .hex();
  },
  get darkenHeader() {
    return color(this.tabBgColor)
      .darken(0.03)
      .hex();
  },

  // Icon
  iconFamily: "Ionicons",
  iconFontSize: platform === "ios" ? 30 : 28,
  iconHeaderSize: platform === "ios" ? 33 : 24,

  // InputGroup
  inputFontSize: 17,
  // inputBorderColor: "#D9D5DC",
  inputBorderColor: brandColors.Dark,
  // inputSuccessBorderColor: "#2b8339",
  inputSuccessBorderColor: brandColors.SecondaryLight,
  // inputErrorBorderColor: "#ed2f2f",
  inputErrorBorderColor: brandColors.Inverse,
  inputHeightBase: 50,
  get inputColor() {
    return this.textColor;
  },
  get inputColorPlaceholder() {
    // return "#575757";
    return brandColors.Dark + 'CD';
  },


  // Line Height
  btnLineHeight: 19,
  lineHeightH1: 32,
  lineHeightH2: 27,
  lineHeightH3: 22,
  lineHeight: platform === "ios" ? 20 : 24,
  listItemSelected: platform === "ios" ? "#007aff" : "#3F51B5",

  // List
  listBg: "transparent",
  listBorderColor: "#c9c9c9",
  listDividerBg: "#f4f4f4",
  listBtnUnderlayColor: "#DDD",
  listItemPadding: platform === "ios" ? 10 : 12,
  listNoteColor: "#808080",
  listNoteSize: 13,

  // Progress Bar
  defaultProgressColor: "#E4202D",
  inverseProgressColor: "#1A191B",

  // Radio Button
  radioBtnSize: platform === "ios" ? 25 : 23,
  radioSelectedColorAndroid: "#3F51B5",
  radioBtnLineHeight: platform === "ios" ? 29 : 24,
  get radioColor() {
    return this.brandPrimary;
  },

  // Segment
  // segmentBackgroundColor: platform === "ios" ? "#F8F8F8" : "#3F51B5",
  segmentBackgroundColor: brandColors.Primary,
  // segmentActiveBackgroundColor: platform === "ios" ? "#007aff" : localColors.white,
  segmentActiveBackgroundColor: localColors.white,
  // segmentTextColor: platform === "ios" ? "#007aff" : localColors.white,
  segmentTextColor: localColors.white,
  // segmentActiveTextColor: platform === "ios" ? localColors.white : "#3F51B5",
  segmentActiveTextColor: brandColors.Primary,
  // segmentBorderColor: platform === "ios" ? "#007aff" : localColors.white,
  segmentBorderColor: localColors.white,
  segmentBorderColorMain: platform === "ios" ? "#a7a6ab" : "#3F51B5",

  // Spinner
  defaultSpinnerColor: "#45D56E",
  inverseSpinnerColor: "#1A191B",

  // Tab
  // tabDefaultBg: platform === "ios" ? "#F8F8F8" : "#3F51B5",
  tabDefaultBg: brandColors.Primary,
  topTabBarTextColor: platform === "ios" ? "#6b6b6b" : "#b3c7f9",
  topTabBarActiveTextColor: platform === "ios" ? "#007aff" : localColors.white,
  topTabBarBorderColor: platform === "ios" ? "#a7a6ab" : localColors.white,
  topTabBarActiveBorderColor: platform === "ios" ? "#007aff" : localColors.white,

  // Tabs
  tabBgColor: "#F8F8F8",
  tabFontSize: 15,

  // Text
  // textColor: localColors.black,
  textColor: localColors.text,
  // inverseTextColor: localColors.white,
  inverseTextColor: localColors.inverseText,
  noteFontSize: 14,
  get defaultTextColor() {
    return this.textColor;
  },

  // Title
  titleFontfamily: platform === "ios" ? "System" : "Roboto_medium",
  titleFontSize: platform === "ios" ? 17 : 19,
  subTitleFontSize: platform === "ios" ? 11 : 14,
  subtitleColor: platform === "ios" ? "#8e8e93" : localColors.white,
  titleFontColor: platform === "ios" ? localColors.black : localColors.white,

  // Other
  borderRadiusBase: platform === "ios" ? 5 : 2,
  borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
  contentPadding: 10,
  dropdownLinkColor: "#414142",
  inputLineHeight: 24,
  deviceWidth,
  deviceHeight,
  isIphoneX,
  inputGroupRoundedBorderRadius: 30,

  //iPhoneX SafeArea
  Inset: {
    portrait: {
      topInset: 24,
      leftInset: 0,
      rightInset: 0,
      bottomInset: 34
    },
    landscape: {
      topInset: 0,
      leftInset: 44,
      rightInset: 44,
      bottomInset: 21
    }
  }
};
