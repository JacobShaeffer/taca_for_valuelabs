/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const { database } = require('../serverComm');

const onTicketsUsed = functions.https.onCall(
	/**
	 * @param {{ itemID: string}} data
	 */
	async (data, context) => {
		console.log('onTicketsUsed called');
		let uid = context.auth.uid || null;
		let itemID = data.itemID || null;
		console.log('uid: ', uid);
		console.log('itemID: ', itemID);

		if(uid !== null && itemID !== null){
			let itemCost;
			//attempt to remove tickets from user account in transaction
			//attempt to increment bids in transaction

			try{
				let itemSnapShot = await database.itemCost(itemID).once('value');
				itemCost = itemSnapShot.val();
				console.log('itemCost: ', itemCost);
				let {error, committed, snapshot} = await database.userIncentives(uid).transaction((incentives) => {
					if(incentives){
						// value recieved from server
						let { tickets, bids } = incentives;
						if(tickets >= itemCost){
							//the user has enough tickets to contiue

							if(bids){
								if(bids[itemID]){
									// the user has already bid on this item before
									bids[itemID] = bids[itemID] + itemCost;	
								}
								else{
									// first time the user is bidding on this item
									bids[itemID] = itemCost;
								}
							}
							else{
								bids = {};
								bids[itemID] = itemCost;
							}
							
							// subtract the cost from the users tickets
							tickets -= itemCost;

							return {
								tickets,
								bids,
							}
						}
						else{
							// the user does not have enough tickets
							// abort the transaction
							return;
						}
					}
					else{
						// no value recieved from server, just return 0
						return {
							tickets: 0,
						}
					}
				});

				let ret = {};

				if(error){
					console.error('there was an error trying to spend tickets: ', `uid: ${uid}, itemID: ${itemID}, error: ${error}`);
					ret.status = 'error';
					ret.message = "unknown server error";
				}
				else if(!committed){
					console.error(`Transaction aborted, ${uid} did not have enough tickets for ${itemID} `)
					ret.status = 'error';
					ret.message = "not enough tickets"
				}
				else{
					console.log('tickets spent successfully');
					ret.status = 'success'
					ret.message = "tickets successfully spent";
				}

				// 	// Ensure incentives/bids/itemID/uid is concurrent with users/uid/incentives/bids/itemID
				let newUserBid = snapshot.val();
				await database.itemBids(uid, itemID).set(newUserBid.bids[itemID]);
				return ret;
			}
			catch(error){
				console.error('an unexpected error occured awaiting the userIncentives transaction: ', error);
				return {status: 'error', message: `severe unknown server error`};
			}
		}
		else{
			console.error(`invalid data - uid: ${uid}, itemID: ${itemID}`);
			return {status: 'error', message: `severe unknown server error`};
		}
	}
);

module.exports = onTicketsUsed;