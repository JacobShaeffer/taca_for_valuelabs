/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const moment = require('moment');
const { database } = require('../serverComm');

const onDayVerified = functions.https.onCall(
	/**
	 * @param {{ timestamp: number }} data
	 */
	(data, context) => {
		let uid = context.auth.uid || null;
		let timestamp = data.timestamp || null;

		let date = moment(timestamp);
		date.startOf('day');
		let did = date.toDate().getTime(); 

		let timestampIsValid = (timestamp !== null && (moment(timestamp).diff(moment(), 'days') <= 7));

		if(uid !== null && timestampIsValid){
			console.log('onDayVerified is frunning');
			return database.verifiedDay(uid, did).set(did)
				.then(async () => {
					console.log('onDayVerified Success');
					let rewardEarned;
					try{
						let incentiveCountSnapshot = await database.incentivesGiven(uid, did).once('value');
						let incentiveCount = incentiveCountSnapshot.val();
						if(!incentiveCount){
							console.log('incentiveCount 0 or null: ', incentiveCount);
							//incentive count is either 0 or null/undefined;
							rewardEarned = 4;
						}	
						else{
							console.log('incentive count: ', incentiveCount);
							rewardEarned = 4 - incentiveCount;
						}

						await database.incentivesGiven(uid, did).transaction((incentiveCount) => {
							return (incentiveCount || 0) + rewardEarned;
						}, (error, commited) => {
							if(error) console.log('there was an error commiting the incentiveCount: ', error);
							if(commited) console.log('incentiveCount successfully commited');
						});

						await database.tickets(uid).transaction((tickets) => {
							return (tickets || 0) + rewardEarned;
						}, (error, commited) => {
							if(error) console.log('there was an error commiting a reward: ', error);
							if(commited) console.log('Reward successfully commited');
						});
					}
					catch(error){
						console.log('an error occured during asyncCallback in dayVerified: ', error);
					}
					return {rewardEarned};
				})
				.catch((error) => {
					console.log('there was an error ondayVerification: ', error);
					// This is probably a permission denied because the day was already verified
					return {error}
				});
		}
		else{
			return {error: `invalid data - uid: ${uid}, timestamp: ${timestamp}, timestamp age: ${moment(timestamp).diff(moment(), 'days')}`};
		}
	}
);

module.exports = onDayVerified;