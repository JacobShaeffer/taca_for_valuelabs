/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
// const {  } = require('../serverComm');

const onNotificationDismissed = functions.https.onCall(
	(data, context) => {
		const uid = context.auth.uid || null;
		/**
		 * data params
		 * @param {String} notifID 
		 */
		const { notifID } = data;

		console.log('onNotificationDismissed called: ', data);

		// TODO: update the users list of notifications
		// use a transaction

		return Promise.resolve();
	}
);

module.exports = onNotificationDismissed;