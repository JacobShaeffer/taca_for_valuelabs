/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const moment = require('moment');
const { firestore } = require('../serverComm');

const MINIMUN_LOCATION_ACCURACY = 200;

/**
 * filter out any points that are outside the useful parameter range
 * @param {*} newLocation 
 */
function filterLocation(newLocation) {
	// check if the accuracy of the location is too  high
	if (newLocation.coords.accuracy > MINIMUN_LOCATION_ACCURACY) {
		return {
			viable: false,
			message: 'Location accuracy too high to continue, ignoring location data'
		};
	}

	if (moment(newLocation.timestamp).diff(moment(), 'days') <= 7){
		return {
			viable: false,
			message: `Location too old: ${newLocation.timestamp}`
		};
	}

	return {
		viable: true,
		message: ''
	}
}

const onLocationRestEndpoint = functions.https
	.onRequest((req, res) => {

		var locationObject = req.body;
		var timestamp = moment(locationObject.timestamp).utcOffset("-07:00");
		locationObject.phoenixLocal = timestamp.toISOString(true);

		const uid = req.query.uid;

		// test if the new location is good enough to coninue with
		let filteredLocation = filterLocation(locationObject);
		if (filteredLocation.viable === false) {
			// the new location was a bad egg
			console.log(filteredLocation.message, locationObject);
			res.status(200).end();
			return null;
		}
		else {
			// put the new location into firestore rawData collection
			return firestore.rawData(uid).add(locationObject)
				.then(() => {
					res.status(200).end();
					return null;
				})
				.catch((error) => {
					console.error('an error occurred attempting to insert location data into firestore/rawdata.', error);
					console.log('the last location given was: ', uid, locationObject);
					res.status(500).end();
					return null;
				});

		}
	});

module.exports = onLocationRestEndpoint;