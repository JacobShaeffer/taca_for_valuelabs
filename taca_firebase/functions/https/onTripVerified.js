/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const moment = require('moment');
const { database } = require('../serverComm');

const onTripVerified = functions.https.onCall(
/**
 * @param {{ tid: string, timestamp: number, dayIsVerified: boolean }} data
 */
(data, context) => {
	let dayIsVerified = data.dayIsVerified || null;
	let uid = context.auth.uid || null;
	let tid = data.tid || null;
	let timestamp = data.timestamp || null;

	let date = moment(timestamp);
	date.startOf('day');
	let did = date.toDate().getTime(); 

	if(uid !== null && tid !== null && timestamp !== null){
		return database.verifiedTrip(uid, tid).set(timestamp)
			.then( async () => {
				let rewardEarned = 0;
				if(!dayIsVerified){
					try{
						console.log('inside try');
						let incentiveCountSnapshot = await database.incentivesGiven(uid, did).once('value');
						let incentiveCount = incentiveCountSnapshot.val();
						if(!incentiveCount){
							console.log('incentiveCount 0 or null: ', incentiveCount);
							//incentive count is either 0 or null/undefined;
							rewardEarned = 1;
						}	
						else if(incentiveCount < 3){
							console.log('incentive count: ', incentiveCount);
							// roll the dice
							// 25% chance to earn a reward
							if(Math.random() > 0.75)
								rewardEarned = 1;
						}

						if(rewardEarned){
							console.log('award earned: ', rewardEarned);

							await database.incentivesGiven(uid, did).transaction((incentiveCount) => {
								return (incentiveCount || 0) + rewardEarned;
							}, (error, commited) => {
								if(error) console.log('there was an error commiting the incentiveCount: ', error);
								if(commited) console.log('incentiveCount successfully commited');
							});

							await database.tickets(uid).transaction((tickets) => {
								return (tickets || 0) + rewardEarned;
							}, (error, commited) => {
								if(error) console.log('there was an error commiting a reward: ', error);
								if(commited) console.log('Reward successfully commited');
							});
						}
					}
					catch(error){
						console.log('an error occured during asyncCallback in TripVerified: ', error);
					}
				}
				return {rewardEarned};
			})
			.catch((error) => {
				console.log('there was an error onTripVerification: ', error);
				return {error}
			});
	}
	else{
		return {error: `invalid data - uid: ${uid}, tid: ${tid}, timestamp: ${timestamp}`};
	}
});


module.exports = onTripVerified;