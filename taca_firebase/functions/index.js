/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const admin = require('firebase-admin');

let serviceAccount = require('./service_account.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://taca-dev-2.firebaseio.com/',
    databaseAuthVariableOverride: {
        uid: 'Auth-service-worker'
    }
});

const onLocationRestEndpoint = require('./https/onLocationRestEndpoint');
//TODO: think about renameing this function to match the standard set in the newer functions
// this will mean that the app will need to be updated as the app points to the current name
exports.locationRestEndpoint = onLocationRestEndpoint;

const onUserCreated = require('./auth/onUserCreated');
exports.onUserCreated = onUserCreated;

const onRawData = require('./firestore/onRawData');
exports.onRawData = onRawData;

const onQuestionAnswered = require('./firestore/onQuestionAnswered');
exports.onQuestionAnswered = onQuestionAnswered;

const onDayVerified = require('./https/onDayVerified');
exports.onDayVerified = onDayVerified;

const onTripVerified = require('./https/onTripVerified');
exports.onTripVerified = onTripVerified;

const onTicketsUsed = require('./https/onTicketsUsed');
exports.onTicketsUsed = onTicketsUsed;

const onNotificationDismissed = require('./https/onNotificationDismissed');
exports.onNotificationDismissed = onNotificationDismissed;