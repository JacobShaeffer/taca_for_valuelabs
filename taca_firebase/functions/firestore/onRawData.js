/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const { stopDetection } = require('../utils/stopDetection');

const onRawData = functions.firestore
	.document('data/{uid}/rawData/{rid}')
	.onCreate((snapshot, context) => {
		//now do stop detection

		const uid = context.params.uid;
		const locationObject = snapshot.data();

		return stopDetection(uid, locationObject)
			.catch((error) => {
				console.error('an error occurred during the stopDetection promise chain.', error);
				console.log('the last location given was: ', uid, locationObject);
				return null;
			});
	});

module.exports = onRawData;