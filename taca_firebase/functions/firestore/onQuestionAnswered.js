/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const FieldValue = require('firebase-admin').firestore.FieldValue;
const { firestore } = require('../serverComm');

const onQuestionAnswered = functions.firestore
	.document('users/{uid}/questions/{nid}')
	.onCreate(async (snapshot, context) => {

		const uid = context.params.uid;
		const nid = context.params.nid;

		let userRef = firestore.user(uid);
		return await userRef.update({notifications: FieldValue.arrayRemove(nid)});
	});

module.exports = onQuestionAnswered;