/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const admin = require('firebase-admin');

const database = admin.database();
const firestore = admin.firestore();

module.exports = {
	database,
	firestore,
};