/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const { database } = require('./admin');

const USER = (uid) => database.ref(`users/${uid}`);

const incentivesGiven = (uid, did) => database.ref(`metaData/${uid}/daily/${did}/incentivesGiven/`);
const userIncentives = (uid) => USER(uid).child(`incentives/`);
const verifiedTrip = (uid, tid) => database.ref(`verifiedTrips/${uid}/${tid}/`);
const notification = (nid) => database.ref(`readOnly/notifications/${nid}`);
const verifiedDay = (uid, did) => database.ref(`verifiedDays/${uid}/${did}/`);
const latestTrip = (uid) => USER(uid).child(`latestTrip/`);
const candidate = (uid) => database.ref(`candidates/${uid}/`);
const itemCost = (iid) => database.ref(`incentives/details/${iid}/cost`);
const itemBids = (uid, iid) => database.ref(`incentives/bids/${iid}/${uid}`);
const tickets = (uid) => USER(uid).child(`incentives/tickets/`);

module.exports = {
	incentivesGiven,
	userIncentives,
	verifiedTrip,
	notification,
	verifiedDay,
	latestTrip,
	candidate,
	itemCost,
	itemBids,
	tickets,
}