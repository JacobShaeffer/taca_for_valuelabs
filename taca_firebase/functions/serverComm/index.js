/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const database = require('./database');
const firestore = require('./firestore');

module.exports = {
	database,
	firestore,
};