/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

// NOTE: Anything done with these will be done with administrator privaleges which means they will ignore the Firestore rules (which is really stupid but that's the way Firestore works...) 
const { firestore } = require('./admin');

const batch = () => firestore.batch();
const transaction = (updateFunction) => firestore.runTransaction(updateFunction);

const data = (uid) => firestore.collection('data').doc(uid);
const trips = (uid) => data(uid).collection('trips');
const rawData = (uid) => data(uid).collection('rawData');

const metaData = (uid) => firestore.collection('metaData').doc(uid);
const secure = (uid) => metaData(uid).collection('secure');
const insecure = (uid) => metaData(uid).collection('insecure');

const user = (uid) => firestore.collection('users').doc(uid);
const answers = (uid, nid) => user(uid).collection('questions').doc(nid);

module.exports = {
	batch,
	transaction,
	trips,
	rawData,
	metaData,
	secure,
	insecure,
	user,
	answers,
}