/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const moment = require('moment');
const {
    calculateDeltaDistance,
    calculateNewAverageLocation,
    removeLocationFromAverage,
    calculateDeltaTime
} = require('./private');


// FIXME:
// probabilities appear to not be calcuated
// confience is so very wrong
// internal confidence is always 2
// stop window appears to not be working
// so many things....

class Candidates {
    /**
     * 
     * @param {{location: {}, data: {}, type: String} options 
     */
    constructor(options) {
        let location, data, type;
        if (options) {
            location = options.location;
            data = options.data;
            type = options.type;
        }

        if (location) {
            // create new Candidates from location
            //console.log('cadidates:32) location was not falsey');
            this.first = this.last = new Candidate({
                location: location,
                type: type,
                viaLocation: true
            });
            this.length = 1;
        } else if (data) {
            let i = 0;
            let index = '0000';

            // test for bad data objects
            while(data.list[index]){
                if( !data.list[index].data ){
                    data.list[index] = null;
                }
                i++;
                index = ('000'+ i).slice(-4);
            }

            index = '0000';
            i = 0;
            // move index to the first non-null data object
            while(data.list[index] === null){
                i++;
                index = ('000' + 1).slice(-4);
            }

            // set up this.first
            let current = this.first = new Candidate({
                viaLocation: false,
                data: data.list[index]
            });
            i++;
            index = ('000' + i).slice(-4);

            // create the rest of the candidates
            this.length = 1;
            while (data.list[index]) {
                current.next = new Candidate({
                    viaLocation: false,
                    data: data.list[index]
                });
                current.next.previous = current;
                current = current.next;
                this.length++;
                i++;
                index = ('000' + i).slice(-4);
            }
            this.last = current;
        } else {
            //console.log(`candidates:51) no data given: `, options);
            return undefined;
        }
    }

    log(i) {
        if (i) {
            switch (i) {
                case 1:
                    console.groupCollapsed('Group 1');
                    if (this.first && this.first.stopWindow) {
                        console.log(this.first.stopWindow.time);
                    } else {
                        console.log('this.first.stopWindow is undefined');
                        console.log('this.first: ', this.first);
                    }
                    console.groupEnd();
                    break;
                case 2:
                    console.groupCollapsed('Group 2: popped');
                    console.log('this.first: ', this.first);
                    console.log('this.last: ', this.last);
                    console.log('linkedList length: ' + this.length);
                    console.groupEnd();
                    break;
                default:
                    console.error(`No case for log(${i})`);
            }
        } else {
            console.groupCollapsed('data');
            console.log('length: ', this.length);
            console.log('this.first: ', this.first);
            console.log('this.last: ', this.last);
            console.log(`Using ${Math.round(process.memoryUsage().heapUsed / 1024 * 100)/100} KB`);
            console.groupEnd();
        }
    }

    /** add one to the locationSinceLastChange property of all candidates
     */
    updateLocationsSinceLast() {
        let current = this.first;
        while (current) {
            if (current.locationsSinceLastChange) {
                current.locationsSinceLastChange++;
            } else {
                current.locationsSinceLastChange = 1;
            }
            current = current.next;
        }
    }

    /**
     * walkes down the linked list starting from this.first popping the element if its confidence is high enough
     */
    popIfConfident() {
        this.sanityCheck();
        // DEBUG:
        // return;
        let popped = [];
        let current = this.first;
        while (current) {
            current.calculateConfidence({
                calculatePrevious: false,
                calculateInternalConfidence: false
            });
            let next = current.next;
            if (current.confidence >= CONFIDENCE_POP_THRESHOLD) {
                popped.push(this.pop());
                //console.log('candidates:115) pop');
            } else {
                break;
            }
            current = next;
        }
        if (popped.length <= 0) {
            popped = null;
        }
        return popped;
    }

    /**
     * removes the first element in the linked list
     * @param {{last: boolean}} options if last is true, then it pops the last element rather than the first
     * @returns popped element
     */
    pop(options) {
        if (options && options.last) {
            let ptr = this.last;
            if (this.last && this.last.previous) {
                this.last = this.last.previous;
                this.last.next.previous = null;
                this.last.next = null;
                this.length--;
            } else {
                this.first = null;
                this.last = null;
                this.length = 0;
            }
            return ptr;
        } else {
            let ptr = this.first;
            if (this.first && this.first.next) {
                this.first = this.first.next;
                this.first.previous.next = null;
                this.first.previous = null;
                this.length--;
            } else {
                this.last = null;
                this.first = null;
                this.length = 0;
            }
            return ptr;
        }
    }

    /**
     * adds a new candidate with given location 
     * @param {location} location 
     */
    push(location, type) {
        let newCandidate = new Candidate({
            location,
            type,
            viaLocation: true
        });
        if (this.last) {
            this.last.next = newCandidate;
            newCandidate.previous = this.last;
        } else {
            this.first = newCandidate;
        }
        this.last = newCandidate;
        this.length++;
    }

    /**
     * Checks to see if the data structure makes sense and will attempt to fix it if not so
     */
    sanityCheck() {
        // console.log('sanityChecking');
        //check that each node in the linked list has data
        let allGood = true;
        let current = this.first.next;
        while (current) {
            if (!current.data) {
                allGood = false;
                break;
            }
            current = current.next;
        }
        if(allGood) return;

        while (this.first && !this.first.data) {
            //the first node of the list is empty, remove it
            this.pop();
        }
        while (this.last && !this.last.data) {
            // the last node of the list is empty, remove it
            this.pop({
                last: true
            });
        }

        current = this.first.next;
        while (current) {
            if (!current.data) {
                current.previous.next = current.next;
                current.next.previous = current.previous;
            }
            current = current.next;
        }
    }

    /**
     * returns an object that is efficient for the database to hold
     */
    databaseify() {
        // the first element of the linked list is done outside of the loop to prevent the need for checking
        // if previous exists in every loop
        let db = {
            list: {
                '0000': this.first
            },
        }

        let index = 1;
        let current = this.first;
        while (current && current.next) {
            current = current.next;
            current.previous.next = null;
            current.previous = null;
            db.list[('000' + index).slice(-4)] = current;
            index++;
        }

        return db;
    }
}

module.exports = Candidates;

const STOP_CENTER_DISTANCE_THRESHOLD = 250;
// const STOP_CENTER_DISTANCE_THRESHOLD = 220;                 // the maximum distance in meters a location can be from the center of this to be added to this if this is a stop
// const WALKING_CENTER_DISTANCE_THRESHOLD = 100;
// TODO: figure out some way of collecting on_foot trips
// TODO: maybe look for groups of points that would have been a stop that all (or a significant proportion) have on_foot type
const STOP_TIME_THRESHOLD = 300; //5min * 60sec;            // the minumum amount of time a stop window needs to be in order for it to become it's own candidate
const INTERNAL_CONFIDENCE_THRESHOLD = 0.70;                 // when this is brand new and previous' internal confidence is at least this high then we assign this type opposite to previous type
const DEFAULT_INTERNAL_CONFIDENCE = 0.2;                    // default internal confidence assigned to new candidates
const DEFAULT_INTERNAL_CONFIDENCE_HIGH = 0.4;               // internal confidence assigned when there is enough information about a new candidate to justify it
const MINIMUM_VIABLE_PROBABILITY_THRESHOLD = 0.4            // if previous or next probability averages are less than this then we just stop looking because there is no hope
const PROBABILITY_CUTOFF_THRESHOLD = 0.65;                   // minimum average probability a set of locations must have to be consumed by previous or next 
const RETURN_TO_SENDER_PROBABILITY_CUTOFF_THRESHOLD = 0.5;  // minimum probability a location must have to be the last location to be consumed
const CONFIDENCE_POP_THRESHOLD = 0.90;                      // confidence level this must have to be popped into a Trip
const CONSTANT_CONFIDENCE_MULTIPLIER = 1.2;                 // 
const MINIMUM_LOCATIONS_WITHOUT_CHANGES_THRESHOLD = 4;      // The minimum number of location added without any changes to this for this to have a non-zero confidence
const COUNT_AFTER_COEFICIENT = 2.0                          // The higher this is the less impact having candidates after this will have
const ACTIVITY_CONFIDENCE_THRESHOLD = 60;                   // The minimum confidence for a 'still' activity on a location to be counted as accurate
const SAME_TRIP_DISTANCE_THRESHOLD = 3218;                  // The minimum distance a point must be to be classified as a break in data (2 miles)
const SAME_TRIP_TIME_THRESHOLD = 7200;                  // The minimum distance a point must be to be classified as a break in data (2 hours)

// next and previous probability constants
// NOTE: the maximum value is associated with a better score in a stop scenario
// this is why CENTER_DISTANCE_MAXIMUM is lower than CENTER_DISTANCE_MAXIMUM
const CENTER_DISTANCE_MINIMUM = 5;
const CENTER_DISTANCE_MAXIMUM = 200;
const NEIGHTBOR_PROXIMITY_CLOSE_MINIMUM = 0;
const NEIGHTBOR_PROXIMITY_CLOSE_MAXIMUM = 75;
const NEIGHTBOR_PROXIMITY_FAR_MINIMUM = 10;
const NEIGHTBOR_PROXIMITY_FAR_MAXIMUM = 150;
const RELATIVE_HEADING_MINIMUM = 0;
const RELATIVE_HEADING_MAXIMUM = Math.PI;
// weight constants
// !Important NOTE: the following four constants need to add to 1
const CENTER_DISTANCE_WEIGHT = 0.38;
const NEIGHBOR_PROXIMITY_CLOSE_WEIGHT = 0.25;
const NEIGHBOR_PROXIMITY_FAR_WEIGHT = 0.27;
const RELATIVE_HEADING_WEIGHT = 0.10;

class Candidate {
    constructor({
        location,
        type = 'default',
        viaLocation,
        data
    }) {
        if (viaLocation) {
            this.next = null;
            this.previous = null;
            location.calculated = {
                neighborProximity: {
                    close: -1,
                    far: -1
                },
                relativeHeading: -1
            }; // FIXME: these are removed by firebase, maybe change the default value?
            this.data = [location];
            this.stopWindow = {
                first: 0,
                center: {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude
                },
                count: 1,
                time: {
                    start: moment(location.timestamp).toISOString(),
                    end: moment(location.timestamp).toISOString(),
                }
            };
            this.probabilities = {
                // -1 means probability not yet calculated
                // 0 means 0% probability
                // 1 means 100% probability
                previous: [-1], // the probability that the associated data element actually belongs to this.previous
                next: [-1], // ditto but belongs to this.next
            };
            this.center = {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
            };
            this.begin = location.timestamp;
            this.end = location.timestamp;
            this.type = type;
            this.locationsSinceLastChange = 0;
            this.confidence = 0.0; //how confident we are that this candidate is a legit trip
            this.internalConfidence = 0.0; //how confident we are about the location data that makes up this candidate
        } else {
            //console.log(`assigning data to Candidate via data`);
            Object.assign(this, data);
        }
    }

    /**
     * this function assumes that the values declared in the constructor have had values assigned already
     * pushes a new location to the end of the tail of this candidate
     * @param {*} location 
     */
    addLocation(location) {
        this.center = calculateNewAverageLocation(this.center, this.data.length, location.coords);
        location.calculated = this.calculateNewLocationProperties(location);
        this.data.push(location);
        this.probabilities.next.push(-1);
        this.probabilities.previous.push(-1);
        this.end = location.timestamp;
        this.locationsSinceLastChange = 0;
    }

    /**
     * adds a location to the front of the list 
     * @param {*} location 
     */
    prependLocation(location) {
        this.center = calculateNewAverageLocation(this.center, this.data.length, location.coords);
        location.calculated = this.calculateNewLocationProperties(location);
        this.data.unshift(location);
        this.probabilities.next.unshift(-1);
        this.probabilities.previous.unshift(-1);
        this.begin = location.timestamp;
        this.locationsSinceLastChange = 0;
    }

    /**
     * calculates the neighborProximity and relativeHeading of given location to previous location in this candidate
     * @param {*} location 
     */
    calculateNewLocationProperties(location) {
        let neighborProximity = {
            close: null,
            far: null
        };
        let relativeHeading = null;

        let len = this.data.length;
        if (len === 0) {
            console.error('a location is being added to a candidate with no data');
        } else {
            if (len === 1) {
                neighborProximity = {
                    close: calculateDeltaDistance(location.coords, this.data[len - 1].coords),
                    far: -1,
                }
            } else {
                neighborProximity = {
                    close: calculateDeltaDistance(location.coords, this.data[len - 1].coords),
                    far: calculateDeltaDistance(location.coords, this.data[len - 2].coords),
                }
            }
            let deltaLat = this.data[len - 1].coords.latitude - location.coords.latitude;
            let deltaLng = this.data[len - 1].coords.longitude - location.coords.longitude;
            relativeHeading = Math.atan2(deltaLat, deltaLng);
        }

        return {
            neighborProximity,
            relativeHeading
        };
    }

    /**
     * returns true if the location supplied would fit the current trip model of this candidate 
     * or it may return 'stop' indecating that the stopWindow has grown to a point where a stop is reasonable
     * this function expects the location given to it to be added to the candidate if true is returned
     * @param {*} location 
     * @returns boolean, or string with type the given point does fit
     */
    fits(location) {
        // DEBUG:
        console.assert(this.data, 'There was no data in the candidate trying to be fitted');

        //console.log(`candidates:383) attempting to fit into a ${this.type}`);
        if (this.type === 'stop') {
            // distance from center
            let distance = calculateDeltaDistance(this.center, location.coords);
            // TODO: look for non-vehicle trips here, a vehicle trip can become a stop if the user parks and starts walking, this is classified as a mode change
            // but then the wlking trip will be consumed as part of the stop.
            // if (distance < STOP_CENTER_DISTANCE_THRESHOLD + location.coords.accuracy) {
            if (distance < STOP_CENTER_DISTANCE_THRESHOLD) {
                return true;
            } else {
                // console.log('viable travel candidate detected');
                return 'travel';
            }
        } else {
            return this.tryFitTravel(location);
        }
    }

    /**
     * attempts to fit the given location into the latest candidate if the latest is a travel candidate
     * this is separated from fits to allow for a recursive decent
     * @param {*} location 
     */
    tryFitTravel(location) {
        let distance = calculateDeltaDistance(this.stopWindow.center, location.coords);
        if(distance > SAME_TRIP_DISTANCE_THRESHOLD){
            console.error('oops!');
        }
        //console.log('candidates:405) distance: ' + distance);
        // TODO: we need to keep track of if this is a vehicle or on_foot/other travel so we can use the proper thesholds
        // TODO: if there is an obvious change from walking to vehicle for example then we break that into two trips with a mode change
        // const DISTANCE_THRESHOLD = this.walkingActivity ? WALKING_CENTER_DISTANCE_THRESHOLD : STOP_CENTER_DISTANCE_THRESHOLD;
        // if (distance < DISTANCE_THRESHOLD + location.coords.accuracy) {
        if (distance < STOP_CENTER_DISTANCE_THRESHOLD) {
            this.stopWindow.center = calculateNewAverageLocation(this.stopWindow.center, this.stopWindow.count, location.coords);
            this.stopWindow.count++;
            this.stopWindow.time.end = moment(location.timestamp).toISOString();
            let time = calculateDeltaTime(location.timestamp, this.stopWindow.time.start);
            //console.log(`candidates:412) checking on timestamp values - location.timestamp: ${location.timestamp}, stopWindow.time.start: ${this.stopWindow.time.start}`);
            //console.log(`candidates:413) time: ${time}`);
            if (time > STOP_TIME_THRESHOLD) {
                // console.log('viable stop candidate detected', this.stopWindow);
                return 'stop';
            } else {
                return true;
            }
        } else {
            let time = calculateDeltaTime(location.timestamp, this.stopWindow.time.start);
            //console.log(`candidates:422) checking on timestamp values - location.timestamp: ${location.timestamp}, stopWindow.time.start: ${this.stopWindow.time.start}`);
            //console.log('candidates:423) time: ' + time);
            if (time > STOP_TIME_THRESHOLD) {

                //check for still location as last location of this (maybe a still in the last few?)
                let lastLocation = this.data[this.data.length - 1];
                let lastIsStill = lastLocation.activity.type === 'still';
                let lastConfidence = lastLocation.activity.confidence;

                if (lastIsStill && lastConfidence > ACTIVITY_CONFIDENCE_THRESHOLD) {
                    //console.log('candidates:432) gapStop');
                    // the last location currently in this is a still location
                    // this implies that that last location is part of a stop and that this new location is part of a new travel

                    // console.log('viable gapStop candidate detected');
                    return 'gapStop'; // a gapStop is a stop defined by the gap left when the app stops collecting data because the phone is still
                } else {
                    //compare against new threshold to see if point is too far to be still the same trip
                    let distanceFromLast = calculateDeltaDistance(lastLocation.coords, location.coords);
                    // let timeFromLast = calculateDeltaTime(location.timestamp, lastLocation.timestamp);
                    if (distanceFromLast > SAME_TRIP_DISTANCE_THRESHOLD || time > SAME_TRIP_TIME_THRESHOLD) {
                        //console.log('candidates:443) not same trip');
                        // the new location is too far away from existing locations to be part of the same trip
                        // we must be missing data somewhere
                        // console.log('no viable candidate detected');
                        return false;
                    } else {
                        //recursively remove stopWindow points from the beginning until the new location fits
                        if (this.stopWindow.count > 0) {
                            distance = this.popLocationFromStopWindow(location);
                        }
                        if (this.stopWindow.count === 0) {
                            this.stopWindow.first = this.data.length;
                            this.stopWindow.time.start = moment(location.timestamp).toISOString();
                            this.stopWindow.center = calculateNewAverageLocation(this.stopWindow.center, this.stopWindow.count, location.coords);
                            this.stopWindow.count++;
                            this.stopWindow.time.end = moment(location.timestamp).toISOString();
                            return true;
                        } else {
                            this.stopWindow.time.start = moment(this.data[this.stopWindow.first].timestamp).toISOString();
                            return this.tryFitTravel(location);
                        }
                    }
                }
            } else {
                // keep removeing points from stopWindow starting from the beginning until location fits
                // if all points are removed then just make stopWindow contain only location
                do {
                    distance = this.popLocationFromStopWindow(location);
                // } while (distance >= DISTANCE_THRESHOLD + location.coords.accuracy && this.stopWindow.count > 0);
                } while (distance >= STOP_CENTER_DISTANCE_THRESHOLD && this.stopWindow.count > 0);
                if (this.stopWindow.count === 0) {
                    this.stopWindow.first = this.data.length;
                    this.stopWindow.time.start = moment(location.timestamp).toISOString();
                } else {
                    this.stopWindow.time.start = moment(this.data[this.stopWindow.first].timestamp).toISOString();
                }
                this.stopWindow.center = calculateNewAverageLocation(this.stopWindow.center, this.stopWindow.count, location.coords);
                this.stopWindow.count++;
                this.stopWindow.time.end = moment(location.timestamp).toISOString();

                return true;
            }
        }
    }

    /**
     * removes the first element of stop window and recalculates the center of stopwindow
     * @returns distance from location to new stopwindow center
     * @param {*} location 
     */
    popLocationFromStopWindow(location) {
        let first = this.data[this.stopWindow.first];
        this.stopWindow.center = removeLocationFromAverage(this.stopWindow.center, this.stopWindow.count, first.coords);
        this.stopWindow.count--;
        this.stopWindow.first++;
        let distance = calculateDeltaDistance(this.stopWindow.center, location.coords);
        return distance;
    }

    /**
     * removes locations from this.data and returns them as an array 
     * @param {number} count the number of locations to remove
     * @param {boolean} reverse if true the locations are taken from the back of the array
     */
    give(count, reverse = false) {
        if (count >= this.data.length) {
            console.log('in give count >= this.data.length');
            let dataToGive = this.data.splice(0);
            this.data = null;
            this.center = null;
            this.begin = null;
            this.end = null;
            this.probabilities = {
                previous: null,
                next: null
            };
            this.stopWindow = {
                center: {
                    latitude: null,
                    longitude: null
                },
                count: null,
                first: null,
                time: {
                    end: null,
                    start: null
                }
            };
            this.locationsSinceLastChange = null;
            return dataToGive;
        }
        let stopWindowOption;
        let index = 0;
        if(reverse){
            // if reverse is true then we want to take points from the back of the array
            index = this.data.length - (count);
        }
        //1: the stop window is completely destroyed, reset to the last point in the data
        //2: the stop window is untouched
        //3: the stop window is damaged and needs to be recalculated
        //4: the stop window is damaged and needs to be recalculated (different data is left intact compared to 3)
        if(this.type === 'stop'){
            stopWindowOption = 5;
        }
        else{
            if (reverse) {
                if (index <= this.stopWindow.first) {
                    stopWindowOption = 1;
                } else {
                    stopWindowOption = 3;
                }
            } else {
                if (count >= this.stopWindow.first) {
                    stopWindowOption = 4;
                } else {
                    stopWindowOption = 2;
                }
            }
        }
        let totalCount = this.data.length;
        let dataToGive = this.data.splice(index, count);

        this.probabilities.previous.splice(index, count);
        this.probabilities.next.splice(index, count);

        let rv = this.center.latitude * totalCount;
        let lv = this.center.longitude * totalCount;

        for (let i = 0; i < dataToGive.length; i++) {
            rv -= dataToGive[i].coords.latitude;
            lv -= dataToGive[i].coords.longitude;
        }

        let last = this.data[this.data.length - 1];
        this.center = {
            latitude: rv / this.data.length,
            longitude: lv / this.data.length
        };
        this.begin = this.data[0].timestamp;
        this.end = last.timestamp;
        this.locationsSinceLastChange = 0;

        switch (stopWindowOption) {
            case 1:
                // console.log(`case 1`);
                this.stopWindow = {
                    center: {
                        latitude: last.coords.latitude,
                        longitude: last.coords.longitude,
                    },
                    count: 1,
                    first: this.data.length - 1,
                    time: {
                        end: last.timestamp,
                        start: last.timestamp,
                    }
                };
                break;
            case 2:
                // console.log(`case 2`);
                //nothing to do here
                break;
            case 3:
                // console.log(`case 3`);
                this.stopWindow = {
                    center: this.recalculateAverageLocation(this.data.slice(this.stopWindow.first, this.data.length)),
                    count: this.stopWindow.count - count,
                    first: this.stopWindow.first,
                    time: {
                        start: this.stopWindow.time.start,
                        end: last.timestamp,
                    }
                }
                break;
            case 4:
                // console.log(`case 4`);
                this.stopWindow = {
                    center: this.center,
                    count: this.data.length,
                    first: 0,
                    time: {
                        start: this.data[0].timestamp,
                        end: last.timestamp,
                    }
                }
                break;
            case 5: 
                // the candidate giving is a stop and therefore we need not care about the stopWindow
                break;
            default:
                console.error(`unknown stop window recalculation case: ${stopWindowOption}`);
        }

        return dataToGive;
    }

    recalculateAverageLocation(dataSet) {
        if (!dataSet || dataSet.length <= 0) {
            return {
                latitude: null,
                longitude: null,
            };
        }
        let average = {
            latitude: dataSet[0].coords.latitude,
            longitude: dataSet[0].coords.longitude
        };
        average = dataSet.reduce((acc, cur, idx) => {
            return calculateNewAverageLocation(acc, idx, cur.coords);
        }, average);
        return average;
    }

    /**
     * takes a list of raw gps data and adds it to the existing data  
     * @param {rawGPS[]} locations list of raw gps data
     * @param {boolean} reverse identifies whether to add the data to the front of back (true for front) 
     */
    take(locations, reverse = false) {
        //reverse being true means we are adding the new locations to the begining of the list
        if (reverse) {
            for (let i = locations.length - 1; i >= 0; i--) {
                this.prependLocation(locations[i]);
            }
        } else {
            for (let i = 0; i < locations.length; i++) {
                this.addLocation(locations[i]);
            }
        }
    }

    /**
     * calculates the type and confidences of the current candidate and optionaly the previous candidate 
     * @param {{calculatePrevious: boolean}} options 
     */
    calculateConfidence(options) {
        // NOTE: this should be fast
        // NOTE: ensures the confidences of the trips before are atleast the same as the current after calculation
        if (this.previous && options && options.calculatePrevious) {
            this.previous.calculateConfidence({
                calculateInternalConfidence: true
            });
        }

        // It's possible for nodes to have no data in this function
        if (!this.data || this.data.length === 0) {
            console.warn('possible error in calculateConfidence. There was no data in this: ', this.data);
            return;
        }
        //console.log('candidates:582) there was data');

        // internalConfidence:
        if (options.calculateInternalConfidence === true) {
            this.calculateInternalConfidence();
        }

        // confidence: 
        if (this.locationsSinceLastChange < MINIMUM_LOCATIONS_WITHOUT_CHANGES_THRESHOLD) {
            this.confidence = 0.0;
            this.confidence_is_constant = false;
        } else {
            let countAfterCurrent = 0;
            let current = this.next;
            while (current) {
                countAfterCurrent++;
                current = current.next;
            }

            let sigmoid = 1.0 / (COUNT_AFTER_COEFICIENT * (1.0 + Math.exp(-(countAfterCurrent * 3.0) + 5.0)));
            let calculated_confidence = this.internalConfidence * (1.0 + sigmoid);
            if (this.confidence === calculated_confidence || this.confidence_is_constant) {
                this.confidence = this.confidence * CONSTANT_CONFIDENCE_MULTIPLIER;
                this.confidence_is_constant = true;
            } else {
                this.confidence = calculated_confidence;
                this.confidence_is_constant = false;
            }
        }
    }

    /**
     * calculated the internal confidence of a candidate
     */
    calculateInternalConfidence() {
        let sum = 0.0;
        this.calculateNextProbabilities();
        this.calculatePreviousProbabilities();
        // //console.log('this.probabilities: ', this.probabilities);
        for (let i = 0; i < this.probabilities.next.length; i++) {
            if(this.next){
                sum += this.probabilities.next[i];
            }
            if(this.previous){
                sum += this.probabilities.previous[i];
            }
        }
        if (sum !== 0) {
            let length = this.probabilities.next.length;
            length = this.next && this.previous ? length * 2.0 : length;
            let averageInternalProbability = sum / length;
            let averageInternalConfidence = 1.0 - averageInternalProbability;
            this.internalConfidence = averageInternalConfidence;
        } else {
            this.internalConfidence = 0;
        }
    }

    /**
     * 
     * @param {number} centerDistance 
     * @param {{close: number, far: number}} neighborProximity 
     * @param {number} relativeHeading 
     * @param {string} type
     */
    // calculateProbability(centerDistance, neighborProximity, relativeHeading, otherType, otherWalkingActivity = false, thisType, thisWalkingActivity = false) {
    calculateProbability(centerDistance, neighborProximity, relativeHeading, type){
        // the following 4 constants need to add to 1;
        if (CENTER_DISTANCE_WEIGHT + NEIGHBOR_PROXIMITY_CLOSE_WEIGHT + NEIGHBOR_PROXIMITY_FAR_WEIGHT + RELATIVE_HEADING_WEIGHT !== 1) {
            throw new Error('constants do not add to 1: Candidates.js -> Candidate -> calculateProbability - line: 624');
        }

        //setup intermediate variables
        let centerDistanceValue, centerDistanceValueAdjusted;
        let neighborProximityCloseValue, neighborProximityCloseValueAdjusted;
        let neighborProximityFarValue, neighborProximityFarValueAdjusted;
        let relativeHeadingValue, relativeHeadingValueAdjusted;

        //calculate individual probabilities for reach variable
        centerDistanceValue = this.normalizeValue(centerDistance, CENTER_DISTANCE_MINIMUM, CENTER_DISTANCE_MAXIMUM);
        neighborProximityCloseValue = this.normalizeValue(neighborProximity.close, NEIGHTBOR_PROXIMITY_CLOSE_MINIMUM, NEIGHTBOR_PROXIMITY_CLOSE_MAXIMUM);
        neighborProximityFarValue = this.normalizeValue(neighborProximity.far, NEIGHTBOR_PROXIMITY_FAR_MINIMUM, NEIGHTBOR_PROXIMITY_FAR_MAXIMUM);
        relativeHeadingValue = this.normalizeValue(Math.abs(relativeHeading), RELATIVE_HEADING_MINIMUM, RELATIVE_HEADING_MAXIMUM);

        // //console.log(`
        //     centerDistanceValue: ${centerDistanceValue}
        //     neighborProximityCloseValue: ${neighborProximityCloseValue}
        //     neighborProximityFarValue: ${neighborProximityFarValue}
        //     relativeHeadingValue: ${relativeHeadingValue}
        // `);

        //inverse the values for travel candidates
        // if (otherType === 'stop') {
        if (type === 'stop') {
            //NOTE: this can be improved when this and other are stops be looking to see which it is closer too 
            // also maybe trying to combine the two stops if they are close enough?
            centerDistanceValue = 1 - centerDistanceValue;
            neighborProximityCloseValue = 1 - neighborProximityCloseValue;
            neighborProximityFarValue = 1 - neighborProximityFarValue;
            relativeHeadingValue = 1 - relativeHeadingValue;
        }
        // else if(thisType === 'travel' && otherType === 'travel'){
        //     if(otherWalkingActivity === thisWalkingActivity){
        //         // both candidates are either walking travel or vehicle travel
        //         // this means leave the values alone?
        //     }
        //     else{
        //         // the two candidates have differing walkActivity values
        //         // they should not eat one another
        //         centerDistanceValue = 0;
        //         neighborProximityCloseValue = 0;
        //         neighborProximityFarValue = 0;
        //     }
        // }

        //adjust each value by given constant
        centerDistanceValueAdjusted = CENTER_DISTANCE_WEIGHT * centerDistanceValue;
        neighborProximityCloseValueAdjusted = NEIGHBOR_PROXIMITY_CLOSE_WEIGHT * neighborProximityCloseValue;
        neighborProximityFarValueAdjusted = NEIGHBOR_PROXIMITY_FAR_WEIGHT * neighborProximityFarValue;
        relativeHeadingValueAdjusted = RELATIVE_HEADING_WEIGHT * relativeHeadingValue;

        //calculate total probability
        let probability =
            centerDistanceValueAdjusted +
            neighborProximityCloseValueAdjusted +
            neighborProximityFarValueAdjusted +
            relativeHeadingValueAdjusted;

        //ensure there is a value in probability
        if (!probability) probability = 0;

        console.assert(probability <= 1 && probability >= 0, `Probability was calcuated to be ${probability} when it should be constrained in [0, 1]`);

        return probability;
    }

    /**
     * returns a normalized value with values over max or under min being clamped to their respective extreme
     * @param {number} val value to be normalized
     * @param {number} min value assigned to 0
     * @param {number} max value assigned to 1
     */
    normalizeValue(val, min, max) {
        // clamp val between min and max
        val = Math.min(Math.max(val, min), max);
        // calculate normalized value
        let normalized = (val - min) / (max - min);
        return normalized;
    }

    /**
     * calculates the probabilities that locations in this actually belong in previous
     */
    calculatePreviousProbabilities() {
        if (!this.previous || !this.previous.data) {
            this.probabilities.previous.fill(0);
            return;
        }
        //console.log(`817) calculating previous probabilities`);

        let continueChecking = true;
        const noChangesLimit = 3;
        let noChangeCounter = 0;

        for (let i = 0; i < this.probabilities.previous.length; i++) {
            if (!continueChecking) {
                if (this.probabilities.previous[i] !== -1) {
                    continue;
                }
            }
            let current = this.data[i];
            let centerDistance;
            if (this.previous.type === 'stop') {
                centerDistance = calculateDeltaDistance(this.previous.center, current.coords);
            } else {
                centerDistance = calculateDeltaDistance(this.center, current.coords);
            }

            // let calcProb = this.calculateProbability(centerDistance, this.data[i].calculated.neighborProximity, this.data[i].calculated.relativeHeading, this.previous.type, this.previous.walkingActivity, this.type, this.walkingActivity);
            let calcProb = this.calculateProbability(centerDistance, this.data[i].calculated.neighborProximity, this.data[i].calculated.relativeHeading, this.previous.type);
            if (this.probabilities.previous[i] === calcProb) {
                noChangeCounter++;
                if (noChangesLimit <= noChangeCounter) {
                    continueChecking = false;
                }
            } else {
                this.probabilities.previous[i] = calcProb;
            }
        }
    }

    /**
     * calculates the probabilities that locations in this actually belong in next
     */
    calculateNextProbabilities() {
        if (!this.next || !this.next.data) {
            this.probabilities.next.fill(0);
            return;
        }

        let continueChecking = true;
        const noChangesLimit = 3;
        let noChangeCounter = 0;

        for (let i = this.probabilities.next.length - 1; i >= 0; i--) {
            if (!continueChecking) {
                if (this.probabilities.next[i] !== -1) {
                    continue;
                }
            }
            let current = this.data[i];
            let centerDistance;
            if(
                !(
                    (current.coords.hasOwnProperty('latitude') && current.coords.hasOwnProperty('longitude'))
                    &&
                    (typeof current.coords.latitude === 'number' && typeof current.coords.longitude === 'number')
                )
            ){
                this.probabilities.next[i] = -1;
                continue;
            }
            if (this.next.type === 'stop') {
                centerDistance = calculateDeltaDistance(this.next.center, current.coords);
            } else {
                centerDistance = calculateDeltaDistance(this.center, current.coords);
            }

            // let calcProb = this.calculateProbability(centerDistance, this.data[i].calculated.neighborProximity, this.data[i].calculated.relativeHeading, this.next.type, this.next.walkingActivity, this.type, this.walkingActivity);
            let calcProb = this.calculateProbability(centerDistance, this.data[i].calculated.neighborProximity, this.data[i].calculated.relativeHeading, this.next.type);
            if (this.probabilities.next[i] === calcProb) {
                noChangeCounter++;
                if (noChangesLimit <= noChangeCounter) {
                    continueChecking = false;
                }
            } else {
                this.probabilities.next[i] = calcProb;
            }
        }
    }

    //const PROBABILITY_MOVING_WINDOW_SIZE = 5;

    /**
     * attempt to assimilate the data from the next candidate into this candidate
     */
    tryConsumeNext() {
        if (this.next) { //&& this.next.internalConfidence < CONFIDENCE_CONSUME_THRESHOLD){
            this.next.calculatePreviousProbabilities(this.type);

            let maxIndex = -1;
            let maxAvgProb = -1;
            let currentAvgProb = 0;
            for (let i = 0; i < this.next.probabilities.previous.length; i++) {
                // TODO: upgrade to a moving window average
                currentAvgProb = (currentAvgProb * i + this.next.probabilities.previous[i]) / (i + 1);

                // if the maxProb is low enough, just give up
                // this will move the threshold from (almost) 0 to 1 times the constant as i increases 
                let sigmoid = (1 / (1 + Math.exp(-1 * (i - 5))) * MINIMUM_VIABLE_PROBABILITY_THRESHOLD);
                if (currentAvgProb < sigmoid) {
                    break
                }

                if (currentAvgProb >= PROBABILITY_CUTOFF_THRESHOLD) { // && maxAvgProb <= currentAvgProb){
                    maxAvgProb = currentAvgProb;
                    maxIndex = i;
                }
            }

            // this loop removes any locations that do not have a high enough probability from the end of the list of those being taken
            // example: the list of locations that maxIndex will take might look like this
            // [0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.9, 0.4, 0.1, 0.2, 0.3]
            // as you can see the last few points should not actually be taken but because of the first set of very high
            // probability points the average was very strong and so gatherd points it should not have so this loop will
            // cut off those last points with an individual cutoff
            for (let i = maxIndex; i >= 0; i--) {
                if (this.next.probabilities.previous[i] < RETURN_TO_SENDER_PROBABILITY_CUTOFF_THRESHOLD) {
                    maxIndex--;
                } else {
                    break;
                }
            }

            if (maxIndex > -1) {
                // there is a set of points that have a high enough  probability to be taken, do that
                this.take(this.next.give(maxIndex + 1));
                this.calculateConfidence({
                    calculatePrevious: true,
                    calculateInternalConfidence: true
                });
            }
            // else return;
        }
        // else return;
        // TODO: else look to see if it is same type, and maybe consume to become one
    }

    /**
     * steal as many locations from the previous candidate as possible 
     * this function is written with the intent that there is only one location in this.data but will work if that is not the case
     */
    tryConsumePrevious(forceProbabilityCalculations) {
        if (!this.previous) {
            return;
        }

        if (this.type === 'stop' && !forceProbabilityCalculations) {
            // TODO: look for on_foot trips here
            // loop backwards throught stopWindow looking for on_foot
            // need like a 50%? on_foot to count
            // then when the points stop being on_foot for long enough go back to the last on_foot

            // let data = this.previous.data;
            // if(data.length >= 6){
            //     let length = data.length-1;
            //     let index = 0;

            //     let lastWalking = false;
            //     let walkingWindow = {
            //         count: 0,
            //         walkingData: [-1,-1,-1,-1,-1,-1],
            //         isWalking: function(){
            //             if(this.count >= 6){
            //                 let val = this.walkingData.reduce((acc, curr) => acc + curr);
            //                 return val > 3;
            //             }
            //             else{
            //                 return -1;
            //             }
            //         },
            //         set: function(isWalking){
            //             let val = isWalking ? 1 : 0;
            //             this.walkingData.push(val);
            //             this.data = data.slice(1);
            //             this.count++;
            //         }
            //     }
            //     do{
            //         if(data[length - index].activity.type === 'on_foot'){
            //             walkingWindow.set(true);
            //             lastWalking = index;
            //         }
            //         walkingWindow.set(false);

            //         index++;
            //     } while(index <= length && walkingWindow.isWalking() !== false);

            //     if(lastWalking){
            //         // a walking trip was detected in the stopwindow of the previous candidate
            //         // change this to a travel, and make it a walkingActivity
            //         this.type = 'travel';
            //         this.walkingActivity = true;
            //         this.take(this.previous.give(lastWalking+1, true), true);
            //         this.internalConfidence = DEFAULT_INTERNAL_CONFIDENCE_HIGH;
            //         return;
            //     }
            // }

            // if previous.stopWindow is full of on_foot then
            if(this.previous.stopWindow.count > 0){
                this.take(this.previous.give(this.previous.stopWindow.count, true), true);
            }
            this.internalConfidence = DEFAULT_INTERNAL_CONFIDENCE_HIGH;
        } else {
            // FIXME:
            if (this.type === 'default') {
                if (this.previous.internalConfidence >= INTERNAL_CONFIDENCE_THRESHOLD) {
                    if (this.previous.type === 'stop') {
                        this.type = 'travel';
                    } else {
                        // this.previous.type is guarenteed to be travel here if the internalConfidence is above the threshold and it is not stop
                        this.type = 'stop';
                    }
                    this.internalConfidence = DEFAULT_INTERNAL_CONFIDENCE;
                } else {
                    // previous has no confidence in it's type and there is only one point in this candidate
                    // so we leave it as a travel
                    this.type = 'travel';
                }
            }

            this.previous.calculateNextProbabilities();

            let maxCount = -1;
            let currentCount = 0;
            // let maxAvgProb = -1;
            let currentAvgProb = 0;
            for (let i = this.previous.probabilities.next.length - 1; i >= 0; i--) {
                // TODO: upgrade to moving window average
                currentAvgProb = (currentAvgProb * currentCount + this.previous.probabilities.next[i]) / (currentCount + 1);

                // if the maxProb is low enough, just give up
                // this will move the threshold from (almost) 0 to 1 times the constant as i increases 
                let sigmoid = (1 / (1 + Math.exp(-1 * (currentCount - 5))) * MINIMUM_VIABLE_PROBABILITY_THRESHOLD);
                if (currentAvgProb < sigmoid) {
                    // console.log(`964) current average probability of next.probabilities is too low, breaking.`);
                    // console.log(`965) currentAvgProb: ${currentAvgProb}, i: ${i}, sigmoid: ${sigmoid}`);
                    break
                }

                currentCount++;
                // console.log(`this probability: ${this.previous.probabilities.next[i]}`);
                // console.log(`currentAvgProb: ${currentAvgProb}`);
                if (currentAvgProb >= PROBABILITY_CUTOFF_THRESHOLD) {// && maxAvgProb <= currentAvgProb) {
                    // maxAvgProb = currentAvgProb;
                    maxCount = currentCount;
                }
            }

            for (let i = maxCount; i < this.previous.probabilities.next.length; i++) {
                if (this.previous.probabilities.next[i] < RETURN_TO_SENDER_PROBABILITY_CUTOFF_THRESHOLD) {
                    maxCount--;
                } else {
                    break;
                }
            }

            // console.log(`maxCount: ${maxCount}`);
            if (maxCount > 0) {
                // console.log('this.data: ', this.data);
                this.take(this.previous.give(maxCount, true), true);
            }
            // else return;
        }
    }

    log() {
        console.log(`this.stopWindow: `, this.stopWindow);
        console.log(`this.begin: ${this.begin}`);
        console.log(`this.end: ${this.end}`);
        console.log(`this.data.length: ${this.data ? this.data.length : null}`);
    }

}