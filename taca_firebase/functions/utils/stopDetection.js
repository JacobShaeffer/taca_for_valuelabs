/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const {
	getCandidateData,
	createTripsFromCandidates
} = require('./private');
const {
	database,
	firestore,
} = require('../serverComm');
const Candidates = require('./Candidates');

/**
 * 
 * @param {string} uid 
 * @param {{}} newLocation 
 * @param {} res 
 */
function stopDetection(uid, newLocation) {
	return getCandidateData(uid)
		.then((data) => {
			let candidates = new Candidates({
				data: data
			});
			// console.log(candidates.log(2));
			let candidatesWasEmpty = false;

			if (candidates && candidates.last) {
				let fits = candidates.last.fits(newLocation);
				if (fits === true) {
					console.log('fits into existing');
					candidates.last.addLocation(newLocation);
					// NOTE: possibly when a confidence is calculated the previous of the candidate will be given at least the same confidence all the way up
					candidates.last.calculateConfidence({
						calculatePrevious: true,
						calculateInternalConfidence: true
					});
					// The candidate before last will try to absorb last if last if not confident enough
					if (candidates.last.previous) {
						candidates.last.previous.tryConsumeNext();
						candidates.sanityCheck();
						candidates.last.tryConsumePrevious( /*forceProbabilityCalculations:*/ true);
					}
				} else {
					console.log('fits: ', fits);
					// console.log(`index:133) new candidate being added - fits: ${fits}`);
					if (fits === 'stop' || fits === 'travel') {
						candidates.push(newLocation, fits);
						// last will try to appropriate points from it's previous
						candidates.last.tryConsumePrevious();
						candidates.last.calculateConfidence({
							calculatePrevious: true,
							calculateInternalConfidence: true
						});
					} else if (fits === 'gapStop') {
						// the location given is a travel, but the stop window of the current last candidate should be made a stop
						let lastLocation = candidates.last.give(1, true)[0];
						candidates.sanityCheck();
						candidates.push(lastLocation, 'stop');
						candidates.last.tryConsumePrevious();
						candidates.last.calculateConfidence({
							calculatePrevious: true,
							calculateInternalConfidence: true
						});
						candidates.last.previous.tryConsumeNext();
						candidates.sanityCheck();
						candidates.push(newLocation, 'travel');
						candidates.last.tryConsumePrevious();
						candidates.last.calculateConfidence({
							calculatePrevious: true,
							calculateInternalConfidence: true
						});
					} else if (fits === false) {
						// the only way we get here is if data is missing and there is a large gap between locations
						// so just start a new candidate
						candidates.push(newLocation);
					} else {
						console.error(`fits was an unrecognized value: ${fits}`);
					}
				}
				candidates.sanityCheck();
			} else {
				candidatesWasEmpty = true;
			}

			return {
				candidatesWasEmpty,
				candidates
			};
		})
		.then(({
			candidatesWasEmpty,
			candidates
		}) => {
			let promises = [];
			let promiseToSetCandidates;
			// or look into timestamp checking to ensure consistancy (maybe just add locations that are before end of candidate?)
			if (candidatesWasEmpty) {
				promiseToSetCandidates = database.latestTrip(uid).once('value')
					// TODO: nested promise execution: this is an anti-pattern
					.then((snapshot) => {
						let latest = snapshot.val();
						if (!latest) {
							// if no data exists then assume the latest was a stop to make the new candidate a trip since trips can become stops more easily
							latest = {
								type: 'stop'
							};
						}
						let type = latest.type === 'travel' ? 'stop' : 'travel';
						candidates = new Candidates({
							location: newLocation,
							type: type
						});
						candidates.updateLocationsSinceLast();
						return database.candidates(uid).set(candidates.databaseify());
					});
			} else {
				candidates.sanityCheck();
				candidates.updateLocationsSinceLast();
				let viableCandidates = candidates.popIfConfident();
				let newTrips = false;
				if (viableCandidates) {
					// NOTE: we can do more in depth testing to make sure everything is good here
					// NOTE: we can also do some other taks here since this won't happen all of the time
					newTrips = createTripsFromCandidates(viableCandidates);
				}
				if (newTrips) {
					let indexOfLast = newTrips.length - 1;
					const promiseToStoreLatest = database.latestTrip(uid).set({
						type: newTrips[indexOfLast].trip.type
					});
					promises.push(promiseToStoreLatest);
					let batch = firestore.batch();
					for (let i = 0; i < newTrips.length; i++) {
						let newTripRef = firestore.trips(uid).doc();
						batch.set(newTripRef, newTrips[i].trip);
						// let newDataArray = firestore.rawData(uid).doc();
						// batch.set(newDataArray, {timestamp: newTrips[i].timestamp, data: newTrips[i].data});
					}
					promises.push(batch.commit());
				}
				promiseToSetCandidates = database.candidates(uid).set(candidates.databaseify());
			}
			promises.push(promiseToSetCandidates);
			return Promise.all(promises);
		});
}


module.exports = {
	stopDetection,
}