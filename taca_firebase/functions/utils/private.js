/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const moment = require('moment');
const {
	database,
} = require('../serverComm');

/**
 * retrieves the candidate data from rtdb
 * @returns {candidates} returns null if no data exists
 */
function getCandidateData(uid){
	return database.candidates(uid).once('value').then((snapshot) => {
		let data = snapshot.val();
		// let candidates = new Candidates({data: data});
		return data;
	})
}

/**
 * accepts an array of candidates and returns an array of trips
 */
function createTripsFromCandidates(candidates){
	let trips = [];
	for(let i=0; i<candidates.length; i++){
		trips.push(createTripFromCandidate(candidates[i]));
	}
	return trips;
}

/**
 * creates a trip from candidate data
 * @param {{data: {rawgps}[], confidence: number, center: latlng, begin: Date, end: Date, type: string, interalConfidence: number}} candidate 
 * @returns {trips, data} 
 */
function createTripFromCandidate(candidate){
	let tempCoords = {};
	for(let i=0; i<candidate.data.length; i++){
		tempCoords[('000' + i).slice(-4)] = {
			latitude: candidate.data[i].coords.latitude,
			longitude: candidate.data[i].coords.longitude,
		}
	}

	let trip = {};
	if(candidate.type === 'stop'){
		trip.activity = 0;// TODO: implement activity recognition
		trip.coords = candidate.center;
		trip.locations = Object.assign({}, tempCoords);
	}
	else if(candidate.type === 'travel'){
		trip.coords = Object.assign({}, tempCoords);
		trip.count = candidate.data.length;
		trip.mode = 0;// TODO: implement mode recognition
	}
	else{
		return null;
	}
	trip.verified = false;
	trip.type = candidate.type;
	trip.begin = moment(candidate.begin).toDate();
	trip.end = moment(candidate.end).toDate();
	return {trip, timestamp: moment(candidate.data[0].phoenixLocal).toDate(), data: candidate.data};
}

const activities = [ 
    {name: 'Other', value: 0}, 
    {name: 'Primary Home', value: 1}, 
    {name: 'Work', value: 2}, 
    {name: 'Work Related', value: 3}, 
    {name: 'Education/School', value: 4}, 
    {name: 'Religious', value: 5},//This is a little insensative seeing as how the icon is specifically a christian church
    {name: 'Shopping', value: 6}, 
    {name: 'Personal Errands/Tasks', value: 7}, 
    {name: 'Dropoff/Pickup/Accompany Someone', value: 8}, 
    {name: 'Change Travel Mode/Transfer', value: 9}, 
    {name: 'Health Care', value: 10}, 
    {name: 'Eat Meals Out', value: 11}, 
    {name: 'Social/Recreational/Entertainment', value: 12},//sofa 
    {name: 'Exercise/Play Sports', value: 13}, 
    {name: 'Secondary Home', value: 14}, 
];

const modes = [
    {name: 'Other', value: 0},//find a better other icon
    {name: 'Bicycle', value: 1},
    {name: 'Bus', value: 2},
    {name: 'Vehicle', value: 3},
    {name: 'Foot', value: 4},
    {name: 'Light Rail', value: 5},
    {name: 'Taxi/Car Service', value: 6},
    {name: 'Air', value: 7},
];

/**
 * constant time function for adding a value to an existing average
 * O(1)
 * @param {latitude: number, longitude: number} averageLocation existing average to be added to
 * @param {number} count the number of points that make up averageLocation
 * @param {latitude: number, longitude: number} newLocation the new location to add to the average
 * @return {{latitude: number, longitude: number}}
 */
function calculateNewAverageLocation(averageLocation, count, newLocation){
	var newAverage = {
		latitude: (count * averageLocation.latitude + newLocation.latitude) / (count + 1),
		longitude: (count * averageLocation.longitude + newLocation.longitude) / (count + 1)
	}
	return newAverage;
}

/**
 * removes the given location from the given average location
 * @param {*} averageLocation the current average location
 * @param {*} count the number of points making up averageLocation
 * @param {*} locationToRemove the gps coordinates to be removed
 * @returns {{latitude: number, longitude: number}}
 */
function removeLocationFromAverage(averageLocation, count, locationToRemove){
	if(count === 0){
		return averageLocation;
	}
	else if(count === 1){
		return {latitude: 0, longitude: 0};
	}
	else{
		return {
			latitude: ((count * averageLocation.latitude) - locationToRemove.latitude) / (count - 1),
			longitude: ((count * averageLocation.longitude) - locationToRemove.longitude) / (count - 1),
		}
	}
}

/**
 * returns the number of seconds between two timestamps
 * O(1)
 * @param {string} timestamp1
 * @param {string} timestamp2
 * @return {number}
 */
function calculateDeltaTime(timestamp1, timestamp2){
	var moment1 = moment(timestamp1);
	var moment2 = moment(timestamp2);
	return Math.abs(moment2.diff(moment1, 'seconds'));
}

/**
 * converts radians to degrees
 * O(1)
 * @param {number} degrees degree value to be converted to radians
 * @returns {number}
 */
function toRadians(degrees){
	return degrees * (Math.PI/180);
}

/**
 * calculates the distance between two points on the earth
 * the algorithm used loses accuracy the farther apart the points are, but is more than accurate enough for Arizona sized distances
 * it is also calabrated for Phoenix, Arizona as is uses the earths radius at 33.5 degrees and at Phoenix altitude 
 * @param {{latitude: number, longitude: number}} coordinate1
 * @param {{latitude: number, longitude: number}} coordinate2
 * @returns {number}
 */
function calculateDeltaDistance(coordinate1, coordinate2){
	var R = 6372e3; // aproximate radius of the earth in meters (at 33.5 degrees north)
	var lat1 = coordinate1.latitude;
	var long1 = coordinate1.longitude;
	var lat2 = coordinate2.latitude;
	var long2 = coordinate2.longitude;

	var φ1 = toRadians(lat1);
	var φ2 = toRadians(lat2);
	var Δφ = toRadians((lat2-lat1));
	var Δλ = toRadians((long2-long1));

	var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
			Math.cos(φ1) * Math.cos(φ2) *
			Math.sin(Δλ/2) * Math.sin(Δλ/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

	return(R * c);
}

module.exports = {
	getCandidateData,
	createTripFromCandidate,
	createTripsFromCandidates,
	calculateNewAverageLocation,
	removeLocationFromAverage,
    calculateDeltaDistance,
    calculateDeltaTime,
}