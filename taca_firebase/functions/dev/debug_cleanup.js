/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const admin = require('firebase-admin');

const debug_cleanup = functions.database.ref('userData/rawData/{uid}/cleanup')
	.onCreate((snapshot, context) => {
		const uid = context.params.uid;
		const tripsRef = admin.firestore().collection('users').doc(String(uid)).collection('trips');
		return tripsRef.get()
			.then((snapshot) => {
				var batch = admin.firestore().batch();
				snapshot.docs.forEach((doc) => {
					batch.delete(doc.ref);
				});
				return batch.commit();
			});
	});


module.exports = debug_cleanup;