/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

// const moment = require('moment');
// const functions = require('firebase-functions');
// const admin = require('firebase-admin');
// const { stopDetection } = require('../lib/index');
// // const data = require('../../firestore-export-as-array.json');
// const locData = require('./testLocation.json');

// const debug_test = functions.https
// 	.onRequest(async (req, res) => {
// 		const body = req.body;
// 		const unloadedData = body.data;
// 		const uid = body.uid;
// 		let index = body.index || 0;
// 		const selectBoundry = body.selectBoundry || false;
// 		const dateBoundry = body.dateBoundry || false;
// 		const selectByID = body.selectByID || false;
// 		var range = body.range || false;
// 		const location = body.location || false;
// 		const test = body.test || false;

// 		let data;
// 		if(unloadedData){
// 			data = require(unloadedData);
// 		}
// 		if(data){
// 			if(test){
// 				console.log(data.length);				
// 				res.status(200).end();
// 				return null;
// 			}
// 			if(selectBoundry){
// 				let i = 0;
// 				let count = 0;
// 				let countTo = selectBoundry.count || 0;
// 				let type = selectBoundry.type;
// 				while(i < data.length){
// 					if(data[i].trip_begin === type){
// 						count++;
// 						if(count > countTo){
// 							break;
// 						}
// 					}
// 					i++;
// 				}
// 				index = i;
// 				console.log('Index selected was ' + index);
// 				res.status(200).end();
// 				return null;
// 			}
// 			else if(dateBoundry){
// 				let date = dateBoundry;

// 				let i = 0;
// 				let begin = -1;
// 				let end = -1;
// 				let count = 0;
// 				while(i < data.length){
// 					let str = moment(data[i].phoenixLocal).toISOString(true);
// 					str = str.substr(0, 10);
// 					if(date === str){
// 						count++;
// 						if(begin === -1){
// 							begin = i;
// 						}
// 						end = i;
// 					}
// 					i++;
// 				}

// 				console.log(`begin: ${begin}, end: ${end}, count: ${count}`);

// 				res.status(200).end();
// 				return null;
// 			}
// 			else if(selectByID){
// 				let i = 0;
// 				while(i < data.length){
// 					if(data[i].uuid === selectByID){
// 						console.log(`ID found at index ${i}`);
// 						break;
// 					}
// 					i++;
// 				}
// 				res.status(200).end();
// 				return null;
// 			}
// 			else if(range){
// 				range = {min: parseInt(range.min), max: parseInt(range.max)};
// 				console.log(`range(${range.min}, ${range.max == -1})`);
// 				if(range.max == -1){
// 					range.max = data.length-1;
// 				}
// 				console.log(`range(${range.min}, ${range.max}), data.length: ${data.length}`);
// 				for(let i=range.min; i<=range.max; i++){
// 					console.log(`calling ${i}`)
// 					await stopDetection(uid, data[i]).then(() => {console.log(`${i} finished \n`)});
// 				}
// 				res.status(200).end();
// 				return null;
// 			}
// 			else if(index){
// 				return stopDetection(uid, data[index]).then(() => {
// 					res.status(200).end();
// 					return null;
// 				})
// 				.catch((error) => {
// 					console.error('an error occurred during the stopDetection promise chain.', error);
// 					res.status(500).end();
// 					return null;
// 				});
// 			}
// 			else{
// 				let max = data.length-1;
// 				for(let i=0; i<=max; i++){
// 					console.log(`calling ${i}`)
// 					await stopDetection(uid, data[i]).then(() => {console.log(`${i} finished \n`)});
// 				}
// 				res.status(200).end();
// 				return null;
// 			}
// 		}
// 		else if(location){
// 			console.log('location given: ', locData);
// 			return stopDetection(uid, locData).then(() => {
// 				res.status(200).end();
// 				return null;	
// 			})
// 			.catch((error) => {
// 				console.error(error);
// 			})
// 		}
// 		else{
// 			console.error('data or location not given: aborting');
// 			res.status(500).end();
// 			return null;
// 		}
// 	});

// module.exports = debug_test;