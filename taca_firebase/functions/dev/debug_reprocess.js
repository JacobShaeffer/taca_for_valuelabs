/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

// const functions = require('firebase-functions');
// const admin = require('firebase-admin');
// const fs = require('fs');
// const moment = require('moment');
// const stringify = require('json-stable-stringify');

// /**
//  * This module is used for interacting with the 'rawData' stored in Firestore
//  * calling this module: debug_reprocess.post().form({uid: $STRING [, setTime: $BOOLEAN, count: $NUMBER, begin: $NUMBER, startAt: $DATE]})
//  * time: used to add timestamp variables to rawData to allow retrieving date ranges of data
//  * count: the absolute number of documents retrieved from the server
//  * begin: this option overwrites setTime if not 0, default value is 0 if not given, otherwise is the absolute number of documents to skip
//  * startAt: the earliest a point can have its timestamp to be retrieved from server
//  * 
//  * the server is formated as such
//  * collection: rawData {
//  * 		document: $documentID {
//  * 			data: raw data array,
//  * 			timestamp: *OPTIONAL* timestamp of earliest point in data array
//  * 		}
//  * }
//  */

// // example function call: debug_reprocess.post().form({uid: 'JJQA8YlULaXIP3dfWZwOm0rAFJA2', count: 15, begin: 7})
// // begin at zero-based index 7, and do 15 locations

// const debug_reprocess = functions.https
// 	.onRequest(async (req, res) => {
// 		const body = req.body;
// 		const setTime = body.time;
// 		const uid = body.uid;
// 		const count = parseInt(body.count) || -1;
// 		let begin = parseInt(body.begin) || 0;
// 		const startAt = body.startAt ? moment(body.startAt) : null;

// 		if(!uid){
// 			console.error('Error! Wrong number of arguments given. aborting reprocess run');
// 			console.log(`uid: ${uid}`);
// 			res.status(500).end();
// 			return null;
// 		}

// 		if(setTime){
// 			console.log('setTime evaluated true');
// 			return admin.firestore().collection('users').doc(uid).collection('rawData').get()
// 			.then((collectionSnapshot) => {
// 				let promises = [];
// 				collectionSnapshot.forEach((documentSnapshot) => {
// 					let docData = documentSnapshot.data().data;
// 					console.log('on document: ', documentSnapshot.id);
// 					let time = documentSnapshot.data().timestamp;
// 					if(!time){
// 						console.log('document does not have timestamp');
// 						//need to create a timestamp
// 						promises.push(admin.firestore().collection('users').doc(uid).collection('rawData').doc(documentSnapshot.id).set({timestamp: moment(docData[0].phoenixLocal).toDate()}, {merge: true}));
// 					}
// 				});
// 				console.log('finished');
// 				res.status(200).end();
// 				return Promise.all(promises);
// 			})
// 			.catch((error) => {
// 				console.error('oops!:', error);
// 			})
// 		}

// 		admin.firestore().collection('users').doc(uid).collection('rawData').orderBy('timestamp').get()
// 		.then((collectionSnapshot) => {
// 			let data = [];
// 			let date = moment();
// 			date = date.format('HH_mm_ss');

// 			if(begin === 0 && startAt){
// 				collectionSnapshot.forEach((doc) => {
// 					if(moment(doc.data().timestamp).isBefore(startAt)){
// 						begin++;
// 					}
// 				})
// 			}

// 			let ws = fs.createWriteStream(`/temp/${uid.slice(0, 5)}-${date}.json`, {flags: 'w'});

// 			let currentCount = 0;
// 			let keepGoing = count;
// 			collectionSnapshot.forEach((documentSnapshot) => {
// 				if(currentCount >= begin && keepGoing){
// 					let docData = documentSnapshot.data().data;
// 					for(let i=0; i<docData.length; i++){
// 						data.push(docData[i]);
// 					}
// 					keepGoing--;
// 				}
// 				currentCount++;
// 			});

// 			if(data.length === 0){
// 				console.error('data has length 0. It is possible the raw data does not have timestamps, check that first. Otherwise check your input parameters then try again.');
// 			}

// 			data = data.sort((a, b) => {
// 				let da = moment(a.phoenixLocal);
// 				let db = moment(b.phoenixLocal);

// 				if(da.isSameOrBefore(db)){
// 					return -1;
// 				}
// 				else{
// 					return 1;
// 				}
// 			});

// 			ws.write('[\n');
// 			for(let i=0; i<data.length; i++){
// 				ws.write(stringify(data[i]));
// 				if(i+1 < data.length){
// 					ws.write(',\n');
// 				}
// 			}
// 			ws.write(']');
// 			ws.end();

// 			ws.on('finish', () => {
// 				console.log('write successful');
// 				res.status(200).end();
// 				return null;
// 			})
// 			ws.on('error', (error) => {
// 				console.error('oops!', error);
// 				res.status(500).end();
// 				return null;
// 			})
// 		})
// 		.catch((error) => {
// 			console.error('there was an error', error);
// 			res.status(500).end();
// 			return null;
// 		});
// 	});

// module.exports = debug_reprocess;