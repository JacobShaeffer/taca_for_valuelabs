/******************************************************************************
 * Maricopa Association of Governments (MAG)
 * -----------------------------------
 * [2019] - [2020] MAG
 * All Rights Reserved
 * 
 * Author - Jacob Shaeffer
 */

const functions = require('firebase-functions');
const moment = require('moment');

const onUserCreated = functions.auth.user().onCreate((user) => {
	console.log('A new user has been created: ', user.uid);
	return null;
});

module.exports = onUserCreated;